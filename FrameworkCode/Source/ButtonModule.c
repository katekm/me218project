/*----------------------------- Include Files -----------------------------*/
/* include header files for this service
*/
#include "ButtonModule.h"
#include "ProjectService.h"

/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "NavService.h"

/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000
#define HALF_SEC (ONE_SEC/2)
#define TWO_SEC (ONE_SEC*2)
#define FIVE_SEC (ONE_SEC*5)

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

//bool CheckButtonEvents( void );

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
// Data private to the module: LastButtonState
static uint8_t LastButtonState;
static ButtonState_t CurrentState;
static uint8_t MyPriority;
// add a deferral queue for up to 3 pending deferrals +1 to allow for overhead
static ES_Event_t DeferralQueue[3+1];

/*************************************************************************************
Pseudo-code for the Button module (a service that implements a state machine)
Data private to the module: LastButtonState
*/

/*
InitializeButtonDebounce
Takes a priority number, returns True. 
*/

bool InitButtonDebounce ( uint8_t Priority )
{
  printf("In Button Debounce \n\r");
  
  ES_Event_t ThisEvent;
  MyPriority = Priority;

  //Set CurrentState to be DEBOUNCING
  CurrentState = Debouncing;

  //Start debounce timer (timer posts to ButtonDebounceSM) ********************************
  ES_Timer_InitTimer(DEBOUNCE_TIMER,100); 
  ThisEvent.EventType = ES_INIT;

  printf("Finished Button Debounce Init\n\r");


  // initialize the deferal que
  ES_InitDeferralQueueWith(DeferralQueue, ARRAY_SIZE( DeferralQueue));
  if (ES_PostToService( MyPriority, ThisEvent) == true)
  {  
    return true;
  }else
  {
    return false;
  }
}


/*
RunButtonDebounceSM (implements a 2-state state machine for debouncing timing)
The EventType field of ThisEvent will be one of: ButtonUp, ButtonDown, or ES_TIMEOUT

*/

ES_Event_t RunButtonDebounceSM ( ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState){
    case Debouncing :
      //If EventType is ES_TIMEOUT & parameter is debounce timer number
      if (( ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == DEBOUNCE_TIMER)){
        CurrentState = Ready2Sample;
      }

      break;
      
    case Ready2Sample :
      if( ThisEvent.EventType == ES_BUTTON_UP){ 
        // Start debounce timer
        ES_Timer_InitTimer(DEBOUNCE_TIMER,300);
		    //Set CurrentState to DEBOUNCING
	    	CurrentState = Debouncing;
		    //Post DBButtonUp to MorseElements & DecodeMorse queues
		    ThisEvent.EventType = ES_DBBUTTON_UP;
	    //	PostProjectService(ThisEvent);
      }
      else if(ThisEvent.EventType == ES_LIMIT_FR_DN){ 
        // Start debounce timer
        ES_Timer_InitTimer(DEBOUNCE_TIMER,300);
		    //Set CurrentState to DEBOUNCING
		    CurrentState = Debouncing;
		    //Post DBButtonUp to MorseElements & DecodeMorse queues
		    ThisEvent.EventType = ES_DBLIMIT_FR_DN;
		    PostNavService(ThisEvent);
        printf("\n\r posted to front right down \n\r");
      }
      else if(ThisEvent.EventType == ES_LIMIT_FL_DN) { 
        // Start debounce timer
        ES_Timer_InitTimer(DEBOUNCE_TIMER,300);
		    //Set CurrentState to DEBOUNCING
		    CurrentState = Debouncing;
		    //Post DBButtonUp to MorseElements & DecodeMorse queues
		    ThisEvent.EventType = ES_DBLIMIT_FL_DN;
		    PostNavService(ThisEvent);
        printf("\n\r posted to front left down \n\r");
      }
      else if (ThisEvent.EventType == ES_LIMIT_BR_DN) {
        ES_Timer_InitTimer(DEBOUNCE_TIMER,300);
        //Set CurrentState to DEBOUNCING
        CurrentState = Debouncing;
        //Post DBButtonUp to MorseElements & DecodeMorse queues
        ThisEvent.EventType = ES_DBLIMIT_BR_DN;
        PostNavService(ThisEvent);
        printf("\n\r posted to back right down \n\r");
      }
      else if (ThisEvent.EventType == ES_LIMIT_BL_DN) {
        ES_Timer_InitTimer(DEBOUNCE_TIMER,300);
        //Set CurrentState to DEBOUNCING
        CurrentState = Debouncing;
        //Post DBButtonUp to MorseElements & DecodeMorse queues
        ThisEvent.EventType = ES_DBLIMIT_BL_DN;
        PostNavService(ThisEvent);
        printf("\n\r posted to back left down \n\r");
      }
      break;
    }
  return ReturnEvent;
  
}

/*****************************************************************************/

bool PostButtonDebounce( ES_Event_t ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}
