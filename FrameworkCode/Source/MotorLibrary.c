/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "MotorLibrary.h"

/* include header files for hardware access
*/
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"

/*----------------------------- Module Defines ----------------------------*/

#define GenA_Normal ( PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO )
#define GenB_Normal ( PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO )
#define BitsPerNibble 4

#define PWM_FREQ 3500 //changed from 4000 to 3500 for project
#define PWM_TICKS_PER_MS 1250
#define PWM_TICKS_PER_SEC 1250000

// Arrays containing the constants needed for PWM configuration
const uint32_t PWM_GEN_SEL[4] = {PWM_O_0_GENA, PWM_O_0_GENB, PWM_O_1_GENA, PWM_O_1_GENB};
const uint32_t PWM_CMP_SEL[4] = {PWM_O_0_CMPA, PWM_O_0_CMPB, PWM_O_1_CMPA, PWM_O_1_CMPB};
const uint32_t MTR_DIR_SEL[4] = {BIT6HI, BIT7HI, 0, 0};
const uint32_t PWM_INV_SEL[4] = {PWM_INVERT_PWM0INV, PWM_INVERT_PWM1INV, 
                                 PWM_INVERT_PWM2INV, PWM_INVERT_PWM3INV};
const uint32_t PWM_NOR_SEL[4] = {GenA_Normal, GenB_Normal, GenA_Normal, GenB_Normal};
const uint32_t PWM_100_SEL[4] = {PWM_0_GENA_ACTZERO_ONE, PWM_0_GENB_ACTZERO_ONE,
                                 PWM_1_GENA_ACTZERO_ONE, PWM_1_GENB_ACTZERO_ONE};
const uint32_t PWM_0_SEL[4] = {PWM_0_GENA_ACTZERO_ZERO, PWM_0_GENB_ACTZERO_ZERO,
                               PWM_1_GENA_ACTZERO_ZERO, PWM_1_GENB_ACTZERO_ZERO};

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

bool InitMotorHW(void);
void SetDutyCycle(int8_t DutyCycle, uint8_t MotorNum);

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file

/***************************************************************************
 private functions
 ***************************************************************************/

bool InitMotorHW(void) {
  // Initialize PWM Hardware for the TIVA
  // Motor Pins: Port B, Pin 4-7
  // PWM Generator 0A/0B/1A/1B
  // PWM Output: Module 0, PWM 0-3
  
  // start by enabling the clock to the PWM Module (PWM0)
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;
    
  // enable the clock to Port B
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
    
  // Select the PWM clock as System Clock/16
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) |
  (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_16);
    
  // make sure that the PWM module clock has gotten going
  while ((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0);
    
  // disable the PWM while initializing
  HWREG(PWM0_BASE + PWM_O_0_CTL ) = 0;
  HWREG(PWM0_BASE + PWM_O_1_CTL ) = 0;
    
  // Program generator to go to 1 at rising compare A/B, 0 on falling compare A/B
  
  // GenA_Normal = (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO )
  HWREG(PWM0_BASE + PWM_O_0_GENA) = GenA_Normal;
  HWREG(PWM0_BASE + PWM_O_1_GENA) = GenA_Normal;
  // GenB_Normal = (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO )
  HWREG(PWM0_BASE + PWM_O_0_GENB) = GenB_Normal;
  HWREG(PWM0_BASE + PWM_O_1_GENB) = GenB_Normal;

  // Set the PWM period. Since we are counting both up & down, we initialize
  // the load register to 1/2 the desired total period. We will also program
  // the match compare registers to 1/2 the desired high time
  HWREG(PWM0_BASE + PWM_O_0_LOAD) = (PWM_TICKS_PER_SEC/PWM_FREQ) >> 1;
  HWREG(PWM0_BASE + PWM_O_1_LOAD) = (PWM_TICKS_PER_SEC/PWM_FREQ) >> 1;
  
  // enable the PWM outputs
  HWREG(PWM0_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM0EN | PWM_ENABLE_PWM1EN |
                                      PWM_ENABLE_PWM2EN | PWM_ENABLE_PWM3EN);
  
  // Configure Port B, Pin 4-7 for its alternate function
  HWREG(GPIO_PORTB_BASE + GPIO_O_AFSEL) |= 0xF0; // 0xF0 = 11110000
  
  // Map PWM function to this pin; M0PWMx corresponds to a value of 4
  HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) =
  (HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) & 0x0000ffff) + 
  (4 << (4*BitsPerNibble)) + (4 << (5*BitsPerNibble)) +
  (4 << (6*BitsPerNibble)) + (4 << (7*BitsPerNibble));
  
  // Enable Port B, pins 2-7 for digital I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= 0xFC; // 0xF0 = 11111100
  
  // Turn Pins 2-7 into outputs
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= 0xFC; // 0xF0 = 11111100
  
  // set the up/down count mode, enable the PWM generator and make
  // both generator updates locally synchronized to zero count
  HWREG(PWM0_BASE+ PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
  PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
  
  HWREG(PWM0_BASE+ PWM_O_1_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
  PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
  
  return true;
}

void SetDutyCycle(int8_t DutyCycle, uint8_t MotorNum) {
  // Updates the duty cycle on the PWM pins for the motors
  
  // Inputs: 
  // DutyCycle: Desired duty cycle {-100 to 100}, negative values for reverse direction
  // MotorNum: Picks which motor to update (0 or 1)
  
  uint32_t SetComp;
  
  if( DutyCycle == 0 ) {
    // Un-invert PWM output
    HWREG(PWM0_BASE + PWM_O_INVERT) &= ~PWM_INV_SEL[MotorNum];
    
    // Set action on Zero to set output low
    HWREG(PWM0_BASE + PWM_GEN_SEL[MotorNum]) = PWM_0_SEL[MotorNum];
    
    // Lower direction control pin on motor driver
    HWREG(GPIO_PORTA_BASE+(GPIO_O_DATA + ALL_BITS)) &= ~MTR_DIR_SEL[MotorNum];
  }
  
  else if( DutyCycle < 99 && DutyCycle > 0 ) {
    // Restore normal behavior on Zero
    HWREG(PWM0_BASE + PWM_GEN_SEL[MotorNum]) = PWM_NOR_SEL[MotorNum];
    
    // Un-invert PWM output
    HWREG(PWM0_BASE + PWM_O_INVERT) &= ~PWM_INV_SEL[MotorNum];
    
    // Lower direction control pin on motor driver
    HWREG(GPIO_PORTA_BASE+(GPIO_O_DATA + ALL_BITS)) &= ~MTR_DIR_SEL[MotorNum];
    
    // Calculate comparator value based on Load value
    SetComp = PWM_TICKS_PER_SEC * (100 - DutyCycle) / (PWM_FREQ * 200);
    HWREG(PWM0_BASE + PWM_CMP_SEL[MotorNum]) = SetComp;
  }
  
  else if( DutyCycle < 0 && DutyCycle > -99 ) {
    // Restore normal behavior on Zero
    HWREG(PWM0_BASE + PWM_GEN_SEL[MotorNum]) = PWM_NOR_SEL[MotorNum];
    
    // Invert PWM output
    HWREG(PWM0_BASE + PWM_O_INVERT) |= PWM_INV_SEL[MotorNum];
    
    // Raise direction control pin on motor driver
    HWREG(GPIO_PORTA_BASE+(GPIO_O_DATA + ALL_BITS)) |= MTR_DIR_SEL[MotorNum];
    
    // Calculate comparator value based on Load value
    SetComp = PWM_TICKS_PER_SEC * (100 + DutyCycle) / (PWM_FREQ * 200);
    HWREG(PWM0_BASE + PWM_CMP_SEL[MotorNum]) = SetComp;
  }
  
  else if( DutyCycle == 100 ) {
    // Un-invert PWM output
    HWREG(PWM0_BASE + PWM_O_INVERT) &= ~PWM_INV_SEL[MotorNum];
    
    // Set action on Zero to set output high
    HWREG(PWM0_BASE + PWM_GEN_SEL[MotorNum]) = PWM_100_SEL[MotorNum];
    
    // Lower direction control pin on motor driver
    HWREG(GPIO_PORTA_BASE+(GPIO_O_DATA + ALL_BITS)) &= ~MTR_DIR_SEL[MotorNum];
  }
  
  else if( DutyCycle == -100 ) {
    // Un-invert PWM output
    HWREG(PWM0_BASE + PWM_O_INVERT) &= ~PWM_INV_SEL[MotorNum];
    
    // Set action on Zero to set output low
    HWREG(PWM0_BASE + PWM_GEN_SEL[MotorNum]) = PWM_0_SEL[MotorNum];
    
    // Raise direction control pin on motor driver
    HWREG(GPIO_PORTA_BASE+(GPIO_O_DATA + ALL_BITS)) |= MTR_DIR_SEL[MotorNum];
  }
}

