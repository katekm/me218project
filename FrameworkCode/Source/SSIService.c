/****************************************************************************
 Module
   SSIService.c

 Revision
   1.0.1

 Description
   This is a SSI file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from SSIFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "SSIService.h"

#include "inc/hw_gpio.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_pwm.h"
#include "inc/hw_sysctl.h"
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_ssi.h"

#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/ssi.h"
#include "driverlib/pwm.h"
#include "driverlib/sysctl.h"

#include "SSIService.h"
#include "MotorLibrary.h"
#include "ES_ServiceHeaders.h"

/*----------------------------- Module Defines ----------------------------*/
#define BITS_PER_NIBBLE 4
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static volatile uint8_t output = 0;
static volatile uint8_t curr_rx_byte = 0;
static volatile uint8_t prev_rx_byte = 0xFE;
static volatile uint8_t backup_rx_byte = 0xFE;



/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitSSIService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitSSIService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
	InitSSI();
  
  ES_Timer_InitTimer(SSI_TIMER, 50);
  
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostSSIService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostSSIService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunSSIService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunSSIService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************
   in here you write your service code
   *******************************************/
  /*
	while(1) {
		//printf("Transmitting\n\r");
		ExecuteCommandCycle();
		//printf("Delay\n\r");
		//QueryCG();
    //printf("Command byte: %x \n\r", curr_command);
		//printf("One run done\n\r");
	}
  */
  
  if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == SSI_TIMER ) {
    ES_Timer_InitTimer(SSI_TIMER, 50);
    ExecuteCommandCycle();
  }
  
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/* Initialize the SSI0 module */ 

void InitSSI (void) {
	printf("Initializing SSI module \n\r"); 
  
  // Enable the clock to the GPIO Port
	HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0;
  
	// Enable the clock to the SSI module
	HWREG(SYSCTL_RCGCSSI) |= SYSCTL_RCGCSSI_R0;

  // Wait for GPIO port to be ready
	while((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R0) != SYSCTL_PRGPIO_R0)
	{
		;
	}
  
	// Program the GPIO to use the alternate functions
	// on the SSI pins
	// Using alternate pin functions on PA 2, 3, 4, and 5
	// Page 650 of datasheet:
	// PA2 is CLK of SSI0
	// PA3 is SS of SSI0
	// PA4 is MISO of SSI0
	// PA5 is MOSI of SSI0
	HWREG(GPIO_PORTA_BASE + GPIO_O_AMSEL) = 0x0;
	HWREG(GPIO_PORTA_BASE + GPIO_O_AFSEL) |= (BIT2HI | BIT4HI | BIT5HI); // BIT3HI causes distortion
	
  // Set Mux position in GPIOPCTRL to select SSI use of pins
  //0xFF0F00FF Bit mask is crucial
  HWREG(GPIO_PORTA_BASE  + GPIO_O_PCTL) = (HWREG(GPIO_PORTA_BASE + \
      GPIO_O_PCTL) & 0xFF00F0FF) + \
      (2 << (2 * BITS_PER_NIBBLE)) +
      //(2 << (3 * BITS_PER_NIBBLE)) +
      (2 << (4 * BITS_PER_NIBBLE)) + 
      (2 << (5 * BITS_PER_NIBBLE));
  
	// Program the port lines for digital I/O 
	HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= (BIT3HI | BIT4HI);

	// Program the required data directions on the port lines
	HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);
  
  // Raise SS
  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT3HI;
  
	// If using SPI mode 3, program the pull-up on the clock line
	HWREG(GPIO_PORTA_BASE + GPIO_O_PUR) |= (BIT2HI);

	// Wait for SSI0 to be ready
	while((HWREG(SYSCTL_PRSSI) & SYSCTL_PRSSI_R0) != SYSCTL_PRSSI_R0)
	{
		;
	}
	
	// Make sure that the SSI is disabled before programming mode bits
	HWREG(SSI0_BASE + SSI_O_CR1) &= ~SSI_CR1_SSE;

	// Select master mode (MS) & TXRIS indicating EOT
	HWREG(SSI0_BASE + SSI_O_CR1) &= ~SSI_CR1_MS;
	HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_EOT; // HAVE NOT TESTED YET
	
  // Configure the clock prescaler
  // System clock is 40 MHz
	// NEED 15kHz
  // 40 MHz / 0.015 MHz = 2666.66666 ~= 2666 (even number)
	// CPSDVSR * (1 + SCR) = 2666
  // CPSDVSR = 0d86 = 0x56
  // SCR = 0d30 = 0x1E
	HWREG(SSI0_BASE + SSI_O_CPSR) &= ~SSI_CPSR_CPSDVSR_M;
	HWREG(SSI0_BASE + SSI_O_CPSR) |= 0x56;
  
  // Configure clock rate, clock phase, & clock polarity
	HWREG(SSI0_BASE + SSI_O_CR0) &= ~(SSI_CR0_SCR_M);
  HWREG(SSI0_BASE + SSI_O_CR0) |= (0x1E) << 8;
	HWREG(SSI0_BASE + SSI_O_CR0) |= (SSI_CR0_SPH | SSI_CR0_SPO);

  // Configure frame mode
	HWREG(SSI0_BASE + SSI_O_CR0) &= ~SSI_CR0_FRF_M;
	HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_FRF_MOTO;

  // Configure data size
	HWREG(SSI0_BASE + SSI_O_CR0) &= ~SSI_CR0_DSS_M;
	HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_DSS_8;

	// Locally enable interrupts (if enabled, hard to test as SCK won't appear)
	HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM; 

	HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_SSE;
  
  // Enable NVIC interrupt for SSI when starting transmission
	// NVIC Enable Register 0 handles IRQs 0-31 
	// (corresponding to interrupts 16-47)
	// SSI0 is IRQ number 7, so BIT7HI 
	HWREG(NVIC_EN0) |= BIT7HI; // NOT TESTED YET
	
	printf("SSI Init Complete \n\r");
}

uint8_t CheckTXFifoEmpty(void) {
	uint8_t bool_out = ((HWREG(SSI0_BASE + SSI_O_SR) & SSI_SR_TFE) > 0) ? 1 : 0;
	return bool_out; 
}


uint8_t CheckTXFifoFull(void){
	uint8_t bool_out = ((HWREG(SSI0_BASE + SSI_O_SR) & SSI_SR_TNF) == 0) ? 1 : 0;
	return bool_out; 
}


uint8_t CheckRXFifoFull(void){
	uint8_t bool_out = ((HWREG(SSI0_BASE + SSI_O_SR) & SSI_SR_RNE) > 0) ? 1 : 0;
	return bool_out; 
}

uint8_t CheckRXFifoEmpty(void){
	uint8_t bool_out = ((HWREG(SSI0_BASE + SSI_O_SR) & SSI_SR_RFF) == 0) ? 1 : 0;
	return bool_out; 	
}

void TransmitSPI(uint8_t tx_data) {
  
  // Lower SS
  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT3LO;

	while(!CheckTXFifoEmpty()) {
		;
	}
	//printf("Ready\n\r");
	HWREG(SSI0_BASE + SSI_O_DR) = tx_data;
  delay();
	//printf("Sending \n\r"); // HELPS DELAY BETWEEN CG CALLS
  
  // Raise SS
  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT3HI;

}

void QueryCG(void){
  uint8_t query_char = 0xAA;
  TransmitSPI(query_char);
}

uint8_t ReceiveSPI(void) {
	// Wait for SPI TX Fifo to be empty
  // For loopback test
  /*
	while (!CheckTXFifoEmpty()) {
		;
	} */

	// Incoming byte
	uint8_t rx_data = 0;

	// Wait for SPI RX Fifo to be filled with 8 bits
	while (!CheckRXFifoFull()) {
		;
	}

	// Read from SSI Data register
	rx_data = HWREG(SSI0_BASE + SSI_O_DR);
	
	return rx_data;
}

void SPITxHandler(void) {
  
  // Clear SSI interrupt
  HWREG(SSI0_BASE + SSI_O_ICR) |= SSI_ICR_RTIC;
  
	prev_rx_byte = curr_rx_byte;
  curr_rx_byte = ReceiveSPI();
	
}

/**********************************************/


void ExecuteCommandCycle (void){
	//QueryCG();
	//printf("Delay\n\r");
	//InterpretCommand(curr_command);
	// TODO: Put millisecond timeout to prevent too fast
	// For now, use printf to delay
	// printf("One Command Cycle Executed\n\r");

}

void delay(void){
	for(uint32_t i = 0; i < 1000; i++) {
		;
	}
}

/* North team = 0x1
   South team = 0x0 */

void RegisterTeam(uint8_t team_polarity) {
  // Variable Initialization
  uint8_t reg_byte = 0x0;
  volatile uint8_t first_rt_rx_byte = 0xAA; // should become 0x00 
  volatile uint8_t second_rt_rx_byte = 0xAA; // should become 0xFF
  volatile uint8_t third_rt_rx_byte = 0xAA; // end should be 0x01 or 0x11 
  
  // REG byte formation
  if(team_polarity == 0x1) {
    reg_byte = 0x10;
  } else if (team_polarity == 0x0) {
    reg_byte = 0x01;
  } else {
    reg_byte = 0xFF;
    printf("Exiting with improper REG byte\n\r"); 
    exit(0);
  }
  
  // Transmit sequence 
  TransmitSPI(reg_byte);
  first_rt_rx_byte = curr_rx_byte;
  
  TransmitSPI(0x00);
  second_rt_rx_byte = curr_rx_byte;
  
  TransmitSPI(0x00);
  third_rt_rx_byte = curr_rx_byte;  
}  

void QueryStatus(void) {
  volatile uint8_t first_rt_rx_byte = 0xAA; // should become 0x00 
  volatile uint8_t second_rt_rx_byte = 0xAA; // should become 0xFF
  volatile uint8_t third_rt_rx_byte = 0xAA; // end should be 0x01 or 0x11

  // Transmit sequence
  TransmitSPI(0x78);
  first_rt_rx_byte = curr_rx_byte;
  
  TransmitSPI(0x00);
  second_rt_rx_byte = curr_rx_byte;
  
  TransmitSPI(0x00);
  third_rt_rx_byte = curr_rx_byte;  
}






/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

