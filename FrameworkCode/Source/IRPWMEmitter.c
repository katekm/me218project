/****************************************************************************
 Module
   PWMService.c

 Revision
   1.0.1

 Description
   This is a file to implement PWM

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/22/19 21:2 bjun2     Update for PWM control of a DC motor
 01/16/12 09:58 jec      began conversion from PWMFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"

#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_pwm.h"
#include "inc/hw_sysctl.h"
#include "inc/tm4c123gh6pm.h"

#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/sysctl.h"

#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

#include "IRPWMEmitter.h"

#define PERIOD_IN_US 500
#define PWM_TICKS_PER_MS 1250
#define BITS_PER_NIBBLE 4

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/

// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
//static uint16_t BaselineADCResult;
//static bool ReturnVal = false;
//static ES_Event_t ThisEvent;

//static uint32_t time;

// State of the motor
static StepState_t CurrentState = InitPSState;

// add a deferral queue for up to 3 pending deferrals +1 to allow for overhead
static struct ES_Event DeferralQueue[3+1];
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitPWMService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     Brian Jun, 01/22/19, 22:10
****************************************************************************/
bool InitPWMService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/

  // Hardware initialization of the PWM module
  //InitPWM();

  // Set one of the PWM to output at 100% duty cycle
  //Set100_DC();

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostPWMService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     Brian Jun, 01/22/19, 22:11
****************************************************************************/
bool PostPWMService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunPWMService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Run a service that adjusts the output frequency of the PWM module
   with respect to the ADC value from the potentiometer
 Notes

 Author
   Brian Jun, 01/22/19, 22:35
****************************************************************************/
ES_Event_t RunPWMService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  uint16_t NewPeriod;

  // Assume no errors
  ReturnEvent.EventType = ES_NO_EVENT; 
    
  // Switch the state of the SM based on CurrentState  
  switch (CurrentState){

    // If CurrentState is pseudo-init state
    case InitPSState:

      // If received event is ES_INIT
      if(ThisEvent.EventType == ES_INIT) {
        
        // initialize the deferal queue
        ES_InitDeferralQueueWith(DeferralQueue, ARRAY_SIZE(DeferralQueue));

        // move to the Initializing state
        CurrentState = Driving;
        SetPWM(50);
        
      }
      break;

    // While driving, if Emitter IR Period changes, update value
    case Driving:
      if(ThisEvent.EventType == GAME_IR_PERIOD_CHANGE) {
        NewPeriod = ThisEvent.EventParam;
        
        HWREG(PWM1_BASE + PWM_O_0_LOAD) = 
        ((NewPeriod * PWM_TICKS_PER_MS) / 1000) >> 1;
        
        printf("\n\rEmitter period changed to %d.\n\r", NewPeriod);
      }
      break;
  }
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
/****************************************************************************
 Function
    InitPWM

 Parameters
   None

 Returns
   None

 Description
   Hardware-level initialization of PWM module

 Notes

 Author
   Brian Jun, 01/22/19, 22:16
****************************************************************************/
bool InitPWM(void){
  // Enable clock to PWM0
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;
  
  // Enable clock to PWM1
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R1;
  
  // Enable clock to Port B
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
  
  // Enable clock to Port D
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R3;
  
  // Select PWM clock as System Clock divided by 32
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) | \
                      (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_32);
 
  // Check the PWM clock stabilized
  while((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0) {
    ;
  }
  
  // Check the PWM clock stabilized
  while((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R1) != SYSCTL_PRPWM_R1) {
    ;
  }
  
  // Disable the PWM during initialization
  HWREG(PWM0_BASE + PWM_O_0_CTL) = 0x0;
  
  // Disable the PWM during initialization
  HWREG(PWM1_BASE + PWM_O_0_CTL) = 0x0;
  
  // Program generators to go to 1 at rising compare A/B,
  // 0 on falling compare A/B
  
  HWREG(PWM0_BASE + PWM_O_0_GENA) = (PWM_0_GENA_ACTCMPAU_ONE | \
                                     PWM_0_GENA_ACTCMPAD_ZERO);
  
  HWREG(PWM0_BASE + PWM_O_0_GENB) = (PWM_0_GENB_ACTCMPBU_ONE | \
                                     PWM_0_GENB_ACTCMPBD_ZERO);
  
  HWREG(PWM1_BASE + PWM_O_0_GENA) = (PWM_1_GENA_ACTCMPAU_ONE | \
                                     PWM_1_GENA_ACTCMPAD_ZERO);
  
  HWREG(PWM1_BASE + PWM_O_0_GENB) = (PWM_1_GENB_ACTCMPBU_ONE | \
                                     PWM_1_GENB_ACTCMPBD_ZERO);

  // Set PWM period
  HWREG(PWM0_BASE + PWM_O_0_LOAD) = ((PERIOD_IN_US * PWM_TICKS_PER_MS) / 1000) >> 1; //TODO: JUST CHANGE THIS INDEPENDENTLY

  // Set PWM period
  HWREG(PWM1_BASE + PWM_O_0_LOAD) = ((PERIOD_IN_US * PWM_TICKS_PER_MS) / 1000) >> 1;

  // Set initial duty cycle on A to 50% 
  HWREG(PWM0_BASE + PWM_O_0_CMPA) = HWREG(PWM0_BASE + PWM_O_0_LOAD) >> 1;
  
  // Set initial duty cycle on B to 25%
  HWREG(PWM0_BASE + PWM_O_0_CMPB) = (HWREG(PWM0_BASE + PWM_O_0_LOAD)) - \
                                   (((PERIOD_IN_US * PWM_TICKS_PER_MS) / 1000)>>3);

  // Set initial duty cycle on A to 50% 
  HWREG(PWM1_BASE + PWM_O_0_CMPA) = HWREG(PWM0_BASE + PWM_O_0_LOAD) >> 1;
  
  // Set initial duty cycle on B to 25%
  HWREG(PWM1_BASE + PWM_O_0_CMPB) = (HWREG(PWM0_BASE + PWM_O_0_LOAD)) - \
                                   (((PERIOD_IN_US * PWM_TICKS_PER_MS) / 1000)>>3);

  // Enable PWM Outputs 
  HWREG(PWM0_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM1EN | \
                                      PWM_ENABLE_PWM0EN | 
                                      PWM_ENABLE_PWM2EN |
                                      PWM_ENABLE_PWM3EN);
                                      
    // Enable PWM Outputs 
  HWREG(PWM1_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM1EN | \
                                      PWM_ENABLE_PWM0EN | 
                                      PWM_ENABLE_PWM2EN |
                                      PWM_ENABLE_PWM3EN);
                                      
  // Now configure the Port B pins to be PWM outputs
  // Start by selecting the alternate function for PB6 & 7
  HWREG(GPIO_PORTB_BASE + GPIO_O_AFSEL) |= (BIT7HI | BIT6HI | BIT5HI | BIT4HI);
  
  HWREG(GPIO_PORTD_BASE + GPIO_O_AFSEL) |= (BIT1HI | BIT0HI);
  
  // Map PWM to these pins 
  HWREG(GPIO_PORTB_BASE  + GPIO_O_PCTL) = (HWREG(GPIO_PORTB_BASE + \
                                           GPIO_O_PCTL) & 0x00FFFFFF) + \
                                           (4<<(7*BITS_PER_NIBBLE)) + 
                                           (4<<(6*BITS_PER_NIBBLE)) +
                                           (4<<(5*BITS_PER_NIBBLE)) + 
                                           (4<<(4*BITS_PER_NIBBLE));

  HWREG(GPIO_PORTD_BASE  + GPIO_O_PCTL) = (HWREG(GPIO_PORTD_BASE + \
                                           GPIO_O_PCTL) & 0x00FFFFFF) + \
                                           (5<<(0*BITS_PER_NIBBLE)) + 
                                           (5<<(1*BITS_PER_NIBBLE));

  // Enable pins 6 and 7 on Port B
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT7HI | BIT6HI | BIT5HI | BIT4HI);
  
  // Setup pins 6 and 7 as outputs on Port B 
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT7HI | BIT6HI | BIT5HI | BIT4HI);
                                            
  // Enable pins 6 and 7 on Port D
  HWREG(GPIO_PORTD_BASE + GPIO_O_DEN) |= (BIT0HI | BIT1HI);
  
  // Setup pins 6 and 7 as outputs on Port D 
  HWREG(GPIO_PORTD_BASE + GPIO_O_DIR) |= (BIT0HI | BIT1HI);
                                            
  // Set up/down count mode, enable PWM generator, and make
  // both generator updates locally synchronized to zero count 
  HWREG(PWM0_BASE + PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
                                    PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
                                    
  // Set up/down count mode, enable PWM generator, and make
  // both generator updates locally synchronized to zero count 
  HWREG(PWM1_BASE + PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
                                    PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
                                    
  return true;
}

/****************************************************************************
 Function
    Set100_DC

 Parameters
   None

 Returns
   None

 Description
   Set PWM channel B to output at 100% duty cycle

 Notes

 Author
   Brian Jun, 01/22/19, 22:17
****************************************************************************/
void Set100_DC(void){
  // Set to 100% Duty Cycle
  HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENB_ACTZERO_ONE;
}

/****************************************************************************
 Function
    SetPWM

 Parameters
   uint8_t: the duty cycle to set PWM to

 Returns
   None

 Description
   Set PWM channel A to duty cycle passed in as argument

 Notes

 Author
   Brian Jun, 01/22/19, 22:17
****************************************************************************/
void SetPWM(uint8_t duty) {
  // Set according to percentage of PWM period 
  // Add one to prevent short from power to ground
  HWREG(PWM0_BASE + PWM_O_0_CMPA) = HWREG(PWM0_BASE + PWM_O_0_LOAD) * \
                                    (duty/(100.0f)) + 1;
  
  HWREG(PWM0_BASE + PWM_O_0_CMPB) = HWREG(PWM0_BASE + PWM_O_0_LOAD) * \
                                    (duty/(100.0f)) + 1;
  
  HWREG(PWM1_BASE + PWM_O_0_CMPA) = HWREG(PWM1_BASE + PWM_O_0_LOAD) * \
                                    (duty/(100.0f)) + 1;
  
  HWREG(PWM1_BASE + PWM_O_0_CMPB) = HWREG(PWM1_BASE + PWM_O_0_LOAD) * \
                                    (duty/(100.0f)) + 1;
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
