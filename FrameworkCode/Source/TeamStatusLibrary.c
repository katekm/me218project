#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Events.h"

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"

#include "COMPASSLibrary.h"
#include "bitdefs.h"
#include "TeamStatusLibrary.h"

uint8_t CheckforTeamLock(void) {
	return HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) & BIT3HI;
  
}

uint8_t GetSelectState(void) {
  while (CheckforTeamLock() != 0) {
    delay();
  }
  printf("Locked on\n\r");

  return HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) & BIT4HI;
}

uint8_t ShowGameOn(void) {
  HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT2HI;
  return 0x1;
}

uint8_t ShowGameOff(void) {
  HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT2LO;
  return 0x1;
}
