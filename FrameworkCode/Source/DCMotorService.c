/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "DCMotorService.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_nvic.h"

#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"

#include "ES_Port.h"
#include "termio.h"

#include "EncoderLibrary.h"
#include "MotorLibrary.h"
#include "ADMulti.h"

#include "NavService.h"

/*----------------------------- Module Defines ----------------------------*/

#define BitsPerNibble 4
#define NumberofMotors 2
#define TicksPerMS 40000

#define POSITION_MODE 1
#define VELOCITY_MODE 0

#define POSITION_MODE_RPM 30

#define RIGHT_MOTOR 1
#define LEFT_MOTOR 0

#define LINE_ARRAY_SIZE 10

#define Motor_Kp 0.5
#define Motor_Ki 0.03
#define Motor_Kd 0

#define Line_Kp 0.052 //0.005
#define Line_Kd 0.025//0.00003

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

void InitPIDInterrupt(void);
void Motor_PID_ISR(void);
uint32_t QueryPIDCounter(void);

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file

static uint8_t MyPriority;
static DCMotorState_t CurrentState;
static volatile uint32_t PIDCounter;
static volatile int16_t RPMTarget[NumberofMotors];
static volatile int32_t PositionTarget[NumberofMotors];
static bool DriveMode = VELOCITY_MODE;

static volatile int32_t RPMSumError[NumberofMotors];
static volatile int32_t RPMError[NumberofMotors];
static volatile int32_t RPMDiffError[NumberofMotors];
static volatile int32_t RPMPrevError[NumberofMotors];
static volatile int32_t RPMCurrent[NumberofMotors];
static volatile int8_t RequestedDuty[NumberofMotors];

static uint32_t LineValues[4];
static volatile int16_t LineError;
static volatile int16_t LineErrorPrev;
static volatile int16_t LineDiffError;
static volatile int16_t LineDiffRPM;
static volatile int16_t LineRPMRequested;
static volatile int16_t LINE_FOLLOWING_RPM;

static volatile uint16_t LineValueArrayLeft;
static volatile uint16_t LineValueArrayRight;
static volatile uint16_t LineValuesAvg[2];

static volatile float Test_MKp = 0.8;
static volatile float Test_MKi = 0.02;
static float Test_MKd = 1.0;

static float Test_LKp = 0.0;
static float Test_LKd = 0.0;

int32_t EncoderCount[2];

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitDCMotorService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitDCMotorService(uint8_t Priority)
{
  ES_Event_t ThisEvent;
  
  CurrentState = DCMotorInitPState;
  
  LINE_FOLLOWING_RPM = QueryLineFollowingRPM();

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostDCMotorService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostDCMotorService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunDCMotorService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunDCMotorService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  //ES_Event_t Event2Post;
  DCMotorState_t NextState = CurrentState;
  //int32_t RPM[2];
  /********************************************
   in here you write your service code
   *******************************************/
  
  switch(CurrentState) {
    case DCMotorInitPState:
      if( ThisEvent.EventType == ES_INIT ) {
        NextState = DCMotorAwaitingCommand;
        ES_Timer_InitTimer(TEST_TIMER, 2000);
      }
    break;
    
    case DCMotorAwaitingCommand:
      switch (ThisEvent.EventType) {
        
        // Events to be implemented in ES_Configure
        
        // Velocity Mode Controls
        // Will command motors to reach designated RPMs
        
        case DR_DRIVE_ALL:
          
          DriveMode = VELOCITY_MODE;
          RPMTarget[RIGHT_MOTOR] = ThisEvent.EventParam;        
          RPMTarget[LEFT_MOTOR] = ThisEvent.EventParam;
        
        break;
        
        case DR_DRIVE_ALL_REV:
          
          DriveMode = VELOCITY_MODE;
          RPMTarget[RIGHT_MOTOR] = -ThisEvent.EventParam;        
          RPMTarget[LEFT_MOTOR] = -ThisEvent.EventParam;
        
        break;
        
        case DR_ROTATE_CCW:
          
          DriveMode = VELOCITY_MODE;
          RPMTarget[RIGHT_MOTOR] = ThisEvent.EventParam;        
          RPMTarget[LEFT_MOTOR] = -ThisEvent.EventParam;
        
        break;
        
        case DR_ROTATE_CW:
          
          DriveMode = VELOCITY_MODE;
          RPMTarget[RIGHT_MOTOR] = -ThisEvent.EventParam;        
          RPMTarget[LEFT_MOTOR] = ThisEvent.EventParam;
        
        break;
        
        case DR_DRIVE_RIGHT:
          
          DriveMode = VELOCITY_MODE;
          RPMTarget[RIGHT_MOTOR] = ThisEvent.EventParam;
        
        break;
        
        case DR_DRIVE_LEFT:
          
          DriveMode = VELOCITY_MODE;
          RPMTarget[LEFT_MOTOR] = ThisEvent.EventParam;
        
        break;
        
        // Position Mode controls
        // Will drive motors until designated encoder position is reached
        // All position modes use the same target RPM; this can be changed.
        
        case DR_DRIVE_ALL_POS:
        
          DriveMode = POSITION_MODE;
        
          EncoderCount[RIGHT_MOTOR] = QueryEncoderCount(RIGHT_MOTOR);
          EncoderCount[LEFT_MOTOR] = QueryEncoderCount(LEFT_MOTOR);
          RPMTarget[RIGHT_MOTOR] = POSITION_MODE_RPM;        
          RPMTarget[LEFT_MOTOR] = POSITION_MODE_RPM;
        
          PositionTarget[RIGHT_MOTOR] = EncoderCount[RIGHT_MOTOR] + ThisEvent.EventParam;
          PositionTarget[LEFT_MOTOR] = EncoderCount[LEFT_MOTOR] + ThisEvent.EventParam;
        
        break;
        
        case DR_ROTATE_CCW_POS:
        
          DriveMode = POSITION_MODE;
        
          EncoderCount[RIGHT_MOTOR] = QueryEncoderCount(RIGHT_MOTOR);
          EncoderCount[LEFT_MOTOR] = QueryEncoderCount(LEFT_MOTOR);
        
          RPMTarget[RIGHT_MOTOR] = POSITION_MODE_RPM;        
          RPMTarget[LEFT_MOTOR] = -POSITION_MODE_RPM;
        
          PositionTarget[RIGHT_MOTOR] = EncoderCount[RIGHT_MOTOR] + ThisEvent.EventParam;
          PositionTarget[LEFT_MOTOR] = EncoderCount[LEFT_MOTOR] - ThisEvent.EventParam;
        
        break;
        
        case DR_ROTATE_CW_POS:
        
          DriveMode = POSITION_MODE;
        
          EncoderCount[RIGHT_MOTOR] = QueryEncoderCount(RIGHT_MOTOR);
          EncoderCount[LEFT_MOTOR] = QueryEncoderCount(LEFT_MOTOR);
        
          RPMTarget[RIGHT_MOTOR] = -POSITION_MODE_RPM;        
          RPMTarget[LEFT_MOTOR] = POSITION_MODE_RPM;
        
          PositionTarget[RIGHT_MOTOR] = EncoderCount[RIGHT_MOTOR] - ThisEvent.EventParam;
          PositionTarget[LEFT_MOTOR] = EncoderCount[LEFT_MOTOR] + ThisEvent.EventParam;
        
        break;
        
        case DR_DRIVE_RIGHT_POS:
        
          DriveMode = POSITION_MODE;
        
          EncoderCount[RIGHT_MOTOR] = QueryEncoderCount(RIGHT_MOTOR);
        
          RPMTarget[RIGHT_MOTOR] = POSITION_MODE_RPM;
        
          PositionTarget[RIGHT_MOTOR] = EncoderCount[RIGHT_MOTOR] + ThisEvent.EventParam;
        
        break;
        
        case DR_DRIVE_LEFT_POS:
        
          DriveMode = POSITION_MODE;
        
          EncoderCount[LEFT_MOTOR] = QueryEncoderCount(LEFT_MOTOR);
        
          RPMTarget[LEFT_MOTOR] = POSITION_MODE_RPM;
        
          PositionTarget[LEFT_MOTOR] = EncoderCount[LEFT_MOTOR] + ThisEvent.EventParam;
        
        break;
        
        // Update speed target commands
        
        case DR_CHANGE_ALL_RPM:
          
          RPMTarget[RIGHT_MOTOR] = ThisEvent.EventParam;
          RPMTarget[LEFT_MOTOR] = ThisEvent.EventParam;
          
        break;
        
        case DR_CHANGE_RIGHT_RPM:
          
          RPMTarget[RIGHT_MOTOR] = ThisEvent.EventParam;
          
        break;
        
        case DR_CHANGE_LEFT_RPM:
          
          RPMTarget[LEFT_MOTOR] = ThisEvent.EventParam;
          
        break;
        
        // Stop commands
        
        case DR_STOP_ALL:
          
          RPMTarget[RIGHT_MOTOR] = 0;        
          RPMTarget[LEFT_MOTOR] = 0;
        
        break;
        
        case DR_STOP_RIGHT:
          
          RPMTarget[RIGHT_MOTOR] = 0;
        
        break;
        
        case DR_STOP_LEFT:
          
          RPMTarget[LEFT_MOTOR] = 0;
        
        break;
        
        case DR_POS_REACHED:
          printf("\n\rPosition reached for Motor %d.\n\r", ThisEvent.EventParam);
          RPMTarget[ThisEvent.EventParam] = 0;
        
        break;
        
        case IND_LINE_PD:
          RPMTarget[LEFT_MOTOR] = LINE_FOLLOWING_RPM + LineDiffRPM;
          RPMTarget[RIGHT_MOTOR] = LINE_FOLLOWING_RPM - LineDiffRPM;
        
        break;
        
        // For debugging
        case ES_TIMEOUT:
          if (ThisEvent.EventParam == TEST_TIMER) {
            ES_Timer_InitTimer(TEST_TIMER,200);
            
            //printf("\n\rADC Values: %d, %d.\n\r", LineValues[0], LineValues[1]);
            //printf("\n\rRequestedRPM = %d, %d\n\r", RPMTarget[0], RPMTarget[1]);
            //printf("\n\rLine following differential RPM: %d\n\r", LineDiffRPM);
            //printf("\n\rLine values: %d, %d\n\r", LineValues[0], LineValues[1]);
            
            //printf("\n\rRPM = %d, %d\n\r", RPMCurrent[0], RPMCurrent[1]);
            //printf("\n\rRequestedDuty = %d, %d\n\r", RequestedDuty[0], RequestedDuty[1]);
            //printf("\n\rRPMError = %d, %d\n\r", RPMError[0], RPMError[1]);
            //printf("\n\rRPMSumError = %d, %d\n\r", RPMSumError[0], RPMSumError[1]);
            
          }
         break;
          
        case ES_NEW_KEY:
          switch(ThisEvent.EventParam) {
            case 'w':
              Test_LKp -= 0.05;
              printf("\n\rMotor K_p = %f.\n\r", Test_LKp);
            break;
            
             case 'W':
              Test_LKp += 0.05;
              printf("\n\rMotor K_p = %f.\n\r", Test_LKp);
            break;
            /*
             case 'a':
              Test_LKi -= 0.01;
              printf("\n\rMotor K_i = %f.\n\r", Test_MKi);
            break;
             
             case 'A':
              Test_LKi += 0.01;
              printf("\n\rMotor K_i = %f.\n\r", Test_MKi);
            break;
             */
            
            case 's':
              Test_LKd -= 0.05;
              printf("\n\rMotor K_d = %f.\n\r", Test_LKd);
            break;
            
            case 'S':
              Test_LKd += 0.05;
              printf("\n\rMotor K_d = %f.\n\r", Test_LKd);
            break;
            
            case 'd':
              
            break;
          }
        break;
          
        case GAME_ENDED:
          __disable_irq();
          SetDutyCycle(LEFT_MOTOR,0);
          SetDutyCycle(RIGHT_MOTOR,0);
        break;
        
        default:
          
        break;
      }
      
    break;
  }

  CurrentState = NextState;
  return ReturnEvent;
}

/***************************************************************************
 Function Definitions
 ***************************************************************************/
void InitPIDInterrupt(void){
 // start by enabling the clock to the timer (Wide Timer 1)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R1;
  
  // kill a few cycles to let the clock get going
  while((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R1) != SYSCTL_PRWTIMER_R1)
  {
    ;
  }
  
  // make sure that timer (Timer A) is disabled before configuring
  HWREG(WTIMER1_BASE+TIMER_O_CTL) &= (~TIMER_CTL_TAEN);
  
  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  HWREG(WTIMER1_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
  
  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value :-)
  HWREG(WTIMER1_BASE+TIMER_O_TAILR) = 0xffffffff;
  
  // set up timer A in periodic mode (TBMR = 1)
  HWREG(WTIMER1_BASE+TIMER_O_TAMR) = 
  (HWREG(WTIMER1_BASE+TIMER_O_TAMR) & ~TIMER_TAMR_TAMR_M) | 
  TIMER_TAMR_TAMR_PERIOD;
  
  // set timeout 
  HWREG(WTIMER1_BASE+TIMER_O_TAILR) = TicksPerMS * 2;
  
  // enable a local timeout interrupt. 
  HWREG(WTIMER1_BASE+TIMER_O_IMR) |= TIMER_IMR_TATOIM;
  
  // enable the Timer A in Wide Timer 1 interrupt in the NVIC
  // it is interrupt number 96 so appears in EN3 at bit 0
  HWREG(NVIC_EN3) |= BIT0HI;
  
  // Set lower priority for NVIC Interrupt 96 (Wide Timer 1A)
  HWREG(NVIC_PRI24) = (HWREG(NVIC_PRI24) & ~NVIC_PRI24_INTA_M) 
  + (1 << NVIC_PRI24_INTA_S);
  
  // make sure interrupts are enabled globally
  __enable_irq();
  
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER1_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
 }
 
void Motor_PID_ISR(void) {

  ES_Event_t Event2Post;
  
  // start by clearing the source of the interrupt (Wide Timer 5B)
  HWREG(WTIMER5_BASE+TIMER_O_ICR) = TIMER_ICR_TBTOCINT;
  
  //HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT0HI;
  
  // Calculate duty cycle for each motor
  for( uint8_t i = 0; i < NumberofMotors; i++) {
    // Calculate RPM from Period recorded by encoder
    RPMCurrent[i] = CalcRPM(i);
    
    // Calculate current error
    RPMError[i] = RPMTarget[i] - RPMCurrent[i];
    
    // Calculate integral error
    RPMSumError[i] += RPMError[i];
    
    // Calculate derivative error
    RPMDiffError[i] = RPMError[i] - RPMPrevError[i];
    
    // Calculate duty cycle from error terms
    //RequestedDuty[i] = Motor_Kp*(RPMError[i] + (Motor_Ki * RPMSumError[i])
    //+ Motor_Kd * RPMDiffError[i]);
    
    RequestedDuty[i] = Test_MKp*(RPMError[i] + (Test_MKi * RPMSumError[i])
    + Test_MKd * RPMDiffError[i]);
    
    // Anti-Windup for integrator
    if (RequestedDuty[i] > 100) {
      RequestedDuty[i] = 100;
      RPMSumError[i] -= RPMError[i];
    }
    else if (RequestedDuty[i] < -100) {
      RequestedDuty[i] = -100;
      RPMSumError[i] -= RPMError[i];
    }
    
    // Send duty cycle request to Motor Library
    SetDutyCycle(RequestedDuty[i], i);
    
    // Record current error value for next iteration
    RPMPrevError[i] = RPMError[i];
    
    /*
    // Logic for position control
    if( DriveMode == POSITION_MODE ) {
      // Retrieve EncoderCount from EncoderLibrary
      EncoderCount[i] = QueryEncoderCount(i);
      
      if(EncoderCount[i] == PositionTarget[i]) {
        // Post DR_POS_REACHED to DCMotorService
        Event2Post.EventParam = i;
        Event2Post.EventType = DR_POS_REACHED;
        
        PostDCMotorService(Event2Post);
      }
    }
    */
  }
  
  // Increment counter to ensure the ISR is running
  PIDCounter++;
  
  
  
  //HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT0LO;
}

void Line_PID_ISR(void) {
  ES_Event_t Event2Post;
  static volatile uint8_t i;
  
  // start by clearing the source of the interrupt (Wide Timer 3A)
  HWREG(WTIMER3_BASE+TIMER_O_ICR) = TIMER_ICR_TATOCINT;
  
  //HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT0HI;
  
  // Read sensor data from inductive sensors
  ADC_MultiRead(LineValues);
  
  // Sum consecutive values of ADC readings
  LineValueArrayRight += LineValues[0];
  LineValueArrayLeft += LineValues[1];
  
  i++;
  
  if(i == 10) {
    
    i = 0;
    
    LineValuesAvg[0] = LineValueArrayRight/10;
    LineValuesAvg[1] = LineValueArrayLeft/10;
  
    LineError = LineValuesAvg[0] - LineValuesAvg[1];
    
    LineDiffError = LineError - LineErrorPrev;
    
    LineDiffRPM = Line_Kp*(LineError + (Line_Kd * LineDiffError));
    
    //Set to 0 again for next calculation
    LineValueArrayRight = 0;
    LineValueArrayLeft = 0;
    
    // For tuning PD values; use only during field testing!
    //LineDiffRPM = Test_LKp*(LineError + (Test_LKd * LineDiffError));
    
    // Anti-windup
    if( LineDiffRPM > (LINE_FOLLOWING_RPM >> 1) ) {
      LineDiffRPM = (LINE_FOLLOWING_RPM >> 1);
    }
    
    Event2Post.EventType = IND_LINE_PD;
    Event2Post.EventParam = LineDiffRPM;
    PostDCMotorService(Event2Post);
    
    // Increment counter to ensure the ISR is running
    PIDCounter++;
    
    // Record error for this iteration
    LineErrorPrev = LineError;
    
    //HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT0LO;
  }
}

uint32_t QueryPIDCounter(void) {
  return PIDCounter;
}
