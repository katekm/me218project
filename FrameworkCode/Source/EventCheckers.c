/****************************************************************************
 Module
   EventCheckers.c

 Revision
   1.0.1

 Description
   This is the sample for writing event checkers along with the event
   checkers used in the basic framework test harness.

 Notes
   Note the use of static variables in sample event checker to detect
   ONLY transitions.

 History
 When           Who     What/Why
 -------------- ---     --------
 08/06/13 13:36 jec     initial version
****************************************************************************/

// this will pull in the symbolic definitions for events, which we will want
// to post in response to detecting events
#include "ES_Configure.h"
// this will get us the structure definition for events, which we will need
// in order to post events in response to detecting events
#include "ES_Events.h"
// if you want to use distribution lists then you need those function
// definitions too.
#include "ES_PostList.h"
// This include will pull in all of the headers from the service modules
// providing the prototypes for all of the post functions
#include "ES_ServiceHeaders.h"
// this test harness for the framework references the serial routines that
// are defined in ES_Port.c
#include "ES_Port.h"
// include our own prototypes to insure consistency between header &
// actual functionsdefinition
#include "EventCheckers.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"

#include "InductiveLibrary.h" 
#include "IRDetector.h"
#include "ProjectService.h"

//Calculating the high time ticks 
//(Period/2) * ticks per second 
#define W_RECYCLING_TICKS 600
#define E_RECYCLING_TICKS 500

#define SOUTH 0x1
#define NORTH 0x0

// This is the event checking function sample. It is not intended to be
// included in the module. It is only here as a sample to guide you in writing
// your own event checkers
#if 0
/****************************************************************************
 Function
   Check4Lock
 Parameters
   None
 Returns
   bool: true if a new event was detected
 Description
   Sample event checker grabbed from the simple lock state machine example
 Notes
   will not compile, sample only
 Author
   J. Edward Carryer, 08/06/13, 13:48
****************************************************************************/
bool Check4Lock(void)
{
  static uint8_t  LastPinState = 0;
  uint8_t         CurrentPinState;
  bool            ReturnVal = false;

  CurrentPinState = LOCK_PIN;
  // check for pin high AND different from last time
  // do the check for difference first so that you don't bother with a test
  // of a port/variable that is not going to matter, since it hasn't changed
  if ((CurrentPinState != LastPinState) &&
      (CurrentPinState == LOCK_PIN_HI)) // event detected, so post detected event
  {
    ES_Event ThisEvent;
    ThisEvent.EventType   = ES_LOCK;
    ThisEvent.EventParam  = 1;
    // this could be any of the service post function, ES_PostListx or
    // ES_PostAll functions
    ES_PostList01(ThisEvent);
    ReturnVal = true;
  }
  LastPinState = CurrentPinState; // update the state for next time

  return ReturnVal;
}

#endif

/****************************************************************************
 Function
   Check4Keystroke
 Parameters
   None
 Returns
   bool: true if a new key was detected & posted
 Description
   checks to see if a new key from the keyboard is detected and, if so,
   retrieves the key and posts an ES_NewKey event to TestHarnessService0
 Notes
   The functions that actually check the serial hardware for characters
   and retrieve them are assumed to be in ES_Port.c
   Since we always retrieve the keystroke when we detect it, thus clearing the
   hardware flag that indicates that a new key is ready this event checker
   will only generate events on the arrival of new characters, even though we
   do not internally keep track of the last keystroke that we retrieved.
 Author
   J. Edward Carryer, 08/06/13, 13:48
****************************************************************************/
bool Check4Keystroke(void)
{
  if (IsNewKeyReady())   // new key waiting?
  {
    ES_Event_t ThisEvent;
    ThisEvent.EventType   = ES_NEW_KEY;
    ThisEvent.EventParam  = GetNewKey();
    // test distribution list functionality by sending the 'L' key out via
    // a distribution list.

      //PostProjectService(ThisEvent);
      PostNavService(ThisEvent);
      PostDCMotorService(ThisEvent);
    return true;
  }
  return false;
}


/*
CheckButtonEvents
Takes no parameters, returns True if an event posted (11/04/11 jec)
Local ReturnVal = False, CurrentButtonState

*/

bool CheckButtonEvents ( void )
{
  bool ReturnVal = false;

  static uint8_t LastButtonState;
  uint8_t CurrentButtonState;
  uint8_t FrontLeftState;
  uint8_t FrontRightState;
  uint8_t BackLeftState;
  uint8_t BackRightState;

  ES_Event_t ThisEvent;

  //Set CurrentButtonState to state read from port pin
  CurrentButtonState = HWREG(GPIO_PORTE_BASE+(GPIO_O_DATA + ALL_BITS)) & \
                       (BIT2HI | BIT3HI | BIT4HI | BIT5HI); 

  FrontLeftState = CurrentButtonState & BIT2HI;//limit switch 1
  FrontRightState = CurrentButtonState & BIT3HI; // limit switch 2
  BackLeftState = CurrentButtonState & BIT4HI; //limit switch 3
  BackRightState = CurrentButtonState & BIT5HI; // limit switch 4

  if (CurrentButtonState != LastButtonState){
    
    if (!FrontRightState) { //if the front limit switches are pressed
  	  //PostEvent ButtonDown to ButtonDebounce queue
  	  ThisEvent.EventType = ES_LIMIT_FR_DN;
      //ThisEvent.EventParam = ES_Timer_GetTime();
      PostButtonDebounce(ThisEvent);
      ReturnVal = true;
      printf(" Front right button pressed, going to debounce\n\r");
    }

    if(!FrontLeftState) {
      ThisEvent.EventType = ES_LIMIT_FL_DN;
      PostButtonDebounce(ThisEvent);
      ReturnVal = true;
      printf(" Front left button pressed, going to debounce\n\r");
    }

    if(!BackRightState) {
      ThisEvent.EventType = ES_LIMIT_BR_DN;
      PostButtonDebounce(ThisEvent);
      ReturnVal = true;
      printf(" Back right button pressed, going to debounce\n\r");
    }

    if(!BackLeftState) {
      ThisEvent.EventType = ES_LIMIT_BL_DN;
      PostButtonDebounce(ThisEvent);
      ReturnVal = true;
      printf(" Back left button pressed, going to debounce\n\r");
    }

    ThisEvent.EventType = ES_BUTTON_UP;
    //ThisEvent.EventParam = ES_Timer_GetTime();
    PostButtonDebounce( ThisEvent );
    ReturnVal = true;
    printf("Buttons up\n\r");

  }

  LastButtonState = CurrentButtonState;
  return ReturnVal;
}


bool CheckLeftTapeSensor(void) {
  static bool LastState = false;
  bool CurrentState;
  ES_Event_t Event2Post;
  static volatile uint32_t TapeTime;
  uint32_t CurrentTime;
  
  CurrentState = (HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA + ALL_BITS)) & BIT2HI);
  
  if( CurrentState != LastState ) {
    
    if( CurrentState == 1 ) {
      
      TapeTime = ES_Timer_GetTime();
      
      // Update last state
      LastState = CurrentState;
      
      return true;
    }
    
    else {
      // Update last state
      LastState = CurrentState;
      
      CurrentTime = ES_Timer_GetTime();
      
      if( (CurrentTime - TapeTime) > 100 ) {
        Event2Post.EventType = TAPE_DETECTED_LEFT;
        PostNavService(Event2Post);
        puts("\n\rLeft tape event posted.\n\r");
      }
      
      return false;
    }
  }
  
  return false;
}

bool CheckRightTapeSensor(void) {
  static bool LastState = false;
  bool CurrentState;
  ES_Event_t Event2Post;
  static volatile uint32_t TapeTime;
  uint32_t CurrentTime;
  
  CurrentState = (HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA + ALL_BITS)) & BIT3HI);
  
  if( CurrentState != LastState ) {
    
    if( CurrentState == 1 ) {
      // Post detected event to Lab8Service
      
      TapeTime = ES_Timer_GetTime();
      
      // Update last state
      LastState = CurrentState;
      
      return true;
    }
    
    else {
      // Update last state
      LastState = CurrentState;
      
      CurrentTime = ES_Timer_GetTime();
      
      if( (CurrentTime - TapeTime) > 100 ) {
        Event2Post.EventType = TAPE_DETECTED_RIGHT;
        PostNavService(Event2Post);
        puts("\n\rRight tape event posted.\n\r");
      }
      
      return false;
    }
  }
  
  return false;
}

bool CheckInductiveLine(void) {
  ES_Event_t Event2Post; 
  
  uint32_t left_inductance = SampleInductiveSensor(LEFT_INDUCTOR);
  uint32_t right_inductance = SampleInductiveSensor(RIGHT_INDUCTOR);
  
  //printf("Left inductance %d\n\r", left_inductance);
  //printf("Right inductance %d\n\r", right_inductance); 
  
  if(InductancesAreMatched(left_inductance, right_inductance) && \
     InductancesAreStrong(left_inductance, right_inductance)) {
    Event2Post.EventType = IND_LINE_DETECTED;
    PostNavService(Event2Post);
    //puts("\n\r Inductive line detected \n\r");
    return true;
  }

  return false;
}

//Checks if Inductive line is off
bool CheckInductiveLineOff(void) {
  ES_Event_t Event2Post; 
  
  uint32_t left_inductance = SampleInductiveSensor(LEFT_INDUCTOR);
  uint32_t right_inductance =  SampleInductiveSensor(RIGHT_INDUCTOR);
  
  //printf("Left inductance %d\n\r", left_inductance);
 // printf("Right inductance %d\n\r", right_inductance); 
  
  if(!InductancesAreMatched(left_inductance, right_inductance) && \
     !InductancesAreStrong(left_inductance, right_inductance)) {
    Event2Post.EventType = IND_LINE_OFF;
    PostNavService(Event2Post);
    //puts("\n\r Inductive line detected \n\r");-
    return true;
  }

  return false;
}
bool CheckBeacon(void) {
  uint32_t period = GetBeaconPeriod();
  
  // Get which recycling station we are by querying ProjectService
  bool RecyclingStation = QueryRecyclingStation();
  
  //If West Recycling
  if(RecyclingStation == 0) {
    if( ( period > ((90 * W_RECYCLING_TICKS)/100) ) && ( (period < ((110 * W_RECYCLING_TICKS)/100) )) ) {
      ES_Event_t Event2Post;
      Event2Post.EventType = R_BEACON_DETECTED;
      PostNavService(Event2Post);
      return true;
    }
  }
  
  //If East Recycling
  else{
    if( ( period > ((90 * E_RECYCLING_TICKS)/100) ) && ( (period < ((110 * E_RECYCLING_TICKS)/100) )) ) {
      ES_Event_t Event2Post;
      Event2Post.EventType = R_BEACON_DETECTED;
      PostNavService(Event2Post);
      return true;
    }
  }
  
  return false;
}

