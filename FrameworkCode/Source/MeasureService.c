/****************************************************************************
 Module
   MeasureService.c

 Revision
   1.0.1

 Description
   This is a file to implement measurement on a DC motor

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/22/19 21:2 bjun2     Update for measurements on a DC motor
 01/16/12 09:58 jec      began conversion from MeasureFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"

#include "inc/hw_gpio.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_pwm.h"
#include "inc/hw_sysctl.h"
#include "inc/tm4c123gh6pm.h"

#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/sysctl.h"

#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"
#include "MeasureService.h"

#include "limits.h"
#include <stdio.h>
#include <stdlib.h>
/*----------------------------- Module Defines ----------------------------*/
#define TIER1 6
#define TIER2 10
#define TIER3 14
#define TIER4 18
#define TIER5 22
#define TIER6 26
#define TIER7 30
#define TIER8 34

#define NO_EDGE_TIMER 1
#define NO_EDGE_TIMER_PERIOD 950  

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static volatile uint32_t period;
static volatile uint32_t this_capture;
static volatile uint32_t last_period;
static volatile uint32_t last_capture;
static volatile uint32_t speriod; 
static volatile uint32_t rpm;

// State of the motor
static MeasureState_t CurrentState = InitPState;

// 1 for logic high and 0 for logic low
static volatile uint8_t last_level; 

// 1 for rising edge, 0 for falling edge
static volatile uint8_t last_edge = 0x1;

static struct ES_Event DeferralQueue[3+1];

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitMeasureService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     Brian Jun, 01/22/19, 21:28
****************************************************************************/
bool InitMeasureService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  
  // Set up timer to check for no incoming encoder value
  ES_Timer_SetTimer(NO_EDGE_TIMER, NO_EDGE_TIMER_PERIOD);

  // Start edge timer
  ES_Timer_StartTimer(NO_EDGE_TIMER);
  
  // Perform hardware initialization for LEDs
  InitLEDs();

  // Initialize interrupt routine for capturing edges
  InitInputCapturePeriod();
  
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostMeasureService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     Brian Jun, 01/22/19, 21:29
****************************************************************************/
bool PostMeasureService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunMeasureService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Run a service that measures the speed (in RPM) of the motor
 Notes

 Author
   Brian Jun, 01/22/19, 21:31
****************************************************************************/
ES_Event_t RunMeasureService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;

  // Assume no errors
  ReturnEvent.EventType = ES_NO_EVENT; 
    
  // Switch the state of the SM based on CurrentState  
  switch (CurrentState){
    
    // If CurrentState is pseudo-init state
    case InitPState:
      
      // If received event is ES_INIT
      if(ThisEvent.EventType == ES_INIT) {
        
        // initialize the deferal queue
        ES_InitDeferralQueueWith(DeferralQueue, ARRAY_SIZE(DeferralQueue));

        // move to the Initializing state
        CurrentState = Measuring;
        
      }
    break;

    // If CurrentState is Stepping state
    case Measuring:
    
      // If edge timer times out, then clear all LEDs
      if(ThisEvent.EventType == ES_TIMEOUT && \
         ThisEvent.EventParam == NO_EDGE_TIMER) {
           HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;
           HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0; 
           
           // Set rpm value to zero
           rpm = 0;

           // Stop edge timer
           ES_Timer_StopTimer(NO_EDGE_TIMER);
           
           // Set up AD Polling timer to timeout every 20 ms 
           ES_Timer_SetTimer(NO_EDGE_TIMER, NO_EDGE_TIMER_PERIOD);

           // Start AD timer
           ES_Timer_StartTimer(NO_EDGE_TIMER);

      // If timer did not time out
      } else {
         
        // Calculate the most recent type of edge 
        FindEdge();

        // Update LED Bars
        UpdateLEDs();
        
        // Calculate the speed of the motor in RPM
        //CalculateRPM();
        
        // Raise GPIO pin PB2 high for profiling
        HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT2HI;

        // print RPM value to terminal
        //printf("%d \n\r", rpm);

        // Lower GPIO pin PB2 low for profiling
        HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT2LO;
      }
        
      // Transition to next state
      CurrentState = Measuring;
      
    break;
  }

  // Return the event
  return ReturnEvent;

}

/***************************************************************************
 private functions
 ***************************************************************************/

/****************************************************************************
 Function
    InitInputCapturePeriod

 Parameters
   None

 Returns
   None

 Description
   Initialize WTIMER0 to do input capture on encoder values
 Notes

 Author
   Brian Jun, 01/22/19, 21:31
****************************************************************************/
void InitInputCapturePeriod(void) {

  // Requires a change in the startup_rvmdk.s file to the WTIMER0
  // interrupt vector

  // Enable clock to timer (WTIMER0)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
  
    // Enable the clock to Port B for debug 
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
  
  // Enable the clock to Port C 
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
  
  // Make sure that Timer A is disbaled before configuring 
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
  
  // Set timer to 32-bit wide mode
  HWREG(WTIMER0_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
  
  // Register the FULL 32-bit value)
  HWREG(WTIMER0_BASE + TIMER_O_TAILR) = 0xFFFFFFFF;
  
  // Set up timer A in capture mode
  HWREG(WTIMER0_BASE + TIMER_O_TAMR) = (HWREG(WTIMER0_BASE + TIMER_O_TAMR) &\
                                        ~TIMER_TAMR_TAAMS) | (TIMER_TAMR_TACDIR | \
                                        TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  
  // Using one timer, capture both edges
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
  
  // Trigger on both rising and falling edge
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |= (BIT3HI | BIT2HI);

  // Setup the port to do the capture by setting alternate function
  HWREG(GPIO_PORTC_BASE + GPIO_O_AFSEL) |= BIT4HI;

  // Map alternate function
  HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) & 0xFFF0FFFF) + \
                                         (7 << 16);

  // Enable pin 1 on Port B for digital I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= BIT1HI; 

  // Designate pin 1 on Port B as an output
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= BIT1HI;

  // Enable pin 4 on Port C for digital I/O
  HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= BIT4HI; 

  // Designate pin 4 on Port C as an input
  HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) &= BIT4LO;

  // Enable local capture interrupt 
  HWREG(WTIMER0_BASE + TIMER_O_IMR) |= TIMER_IMR_CAEIM;

  // Enable timer A in WTIMER0 interrupt in NVIC
  // Interrupt 94, so right shift by 5 and mod by 32
  HWREG(NVIC_EN2) |= BIT30HI;

  // Make sure interrupts are enabled globally
  __enable_irq();

  // Enable timer and its stall feature
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
}

/****************************************************************************
 Function
    InputCaptureResponse

 Parameters
   None

 Returns
   None

 Description
   Capture incoming edges and save period
 Notes

 Author
   Brian Jun, 01/22/19, 21:33
****************************************************************************/
void InputCaptureResponse(void) {
    
  // Debug
  //HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT1HI;
  
  // Clear the source of the interrupt
  HWREG(WTIMER0_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;
  
  // Calculate period from captured value
  this_capture = HWREG(WTIMER0_BASE + TIMER_O_TAR);
  
  // Clear captured edge
  HWREG(WTIMER0_BASE + TIMER_O_TAR) &= 0x0;

  // Calculate most recent period
  period = this_capture - last_capture;

  // Update last_capture
  last_capture = this_capture;
  
  // Debug
  //HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT1LO;

}

/****************************************************************************
 Function
   FindEdge

 Parameters
   None

 Returns
   None

 Description
   Update the state of the last edge; assert that any incorrect 
   edges are not read
 Notes

 Author
   Brian Jun, 01/22/19, 21:36
****************************************************************************/
void FindEdge(void) {
  // If last edge was falling edge and logic low
  if (!last_level && !last_edge ) {

      // Update to rising edge
      last_edge = 1;

  // Else if last edge was rising edge and logic high
  } else if (last_level && last_edge){

      // Update to falling edge
      last_edge = 0;

  // Else there was a bad transition
  } else {
    ;
  }
}

/****************************************************************************
 Function
   InitLEDs

 Parameters
   None

 Returns
   None

 Description
   Initialize the digital I/O for driving the LED bar
 Notes

 Author
   Brian Jun, 01/22/19, 21:38
****************************************************************************/
void InitLEDs(void) {
    // Enable pins to drive LEDs on port E and F 
  HWREG(GPIO_PORTE_BASE + GPIO_O_DEN) |= (BIT5HI | BIT4HI | \
                                          BIT3HI | BIT2HI | BIT1HI);
  HWREG(GPIO_PORTF_BASE + GPIO_O_DEN) |= (BIT4HI | BIT3HI | BIT2HI);


  // Designate LED pins as outputs
  HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) |= (BIT5HI | BIT4HI | \
                                          BIT3HI | BIT2HI | BIT1HI);
  HWREG(GPIO_PORTF_BASE + GPIO_O_DIR) |= (BIT4HI | BIT3HI | BIT2HI);

}

/****************************************************************************
 Function
   UpdateLEDs

 Parameters
   None

 Returns
   None

 Description
   Update the state of the bar graph
 Notes

 Author
   Brian Jun, 01/22/19, 21:39
****************************************************************************/
void UpdateLEDs(void) {
   // Find state of pin to identify rising or falling edge
  last_level = HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT4HI;
  
  // If last edge was rising edge and logic low
  if(last_edge && !last_level) {
    // Skip the most recent period
    period = last_period;
  // Else if last edge was falling edge and logic high
  } else if(!last_edge && last_level) {
    // Skip the most recent period
    period = last_period;
    // Else leave the most recent period value alone
  } else{
    ;
  }
  
  // Raise GPIO pin port PB0 high for profiling
  //HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT0HI;

  // Scale the period to map to the 8 LEDs
  speriod = (period / 1000);
  
  // Else-if ladder to check which LEDs should be driven
  if(speriod >= 0 && speriod < TIER1) {
    // Clear the LEDs
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;
    HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;

    // Drive one LED
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT1HI;
  } else if (speriod >= TIER1 && speriod < TIER2) {
    // Clear the LEDs
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;
    HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;

    // Drive two LEDs
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT2HI | BIT1HI);
  } else if (speriod >= TIER2 && speriod < TIER3) {
    // Clear the LEDs
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;
    HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;

    // Drive three LEDs
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= \
         (BIT3HI | BIT2HI | BIT1HI);
  } else if (speriod >= TIER3 && speriod < TIER4) {
    // Clear the LEDs
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;
    HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;

    // Drive four LEDs
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= \
         (BIT4HI | BIT3HI | BIT2HI |BIT1HI);
  } else if (speriod >= TIER4 && speriod < TIER5) {
    // Clear the LEDs
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;
    HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;

    // Drive five LEDs
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= \
         (BIT5HI | BIT4HI | BIT3HI | BIT2HI |BIT1HI);
  } else if (speriod >= TIER5 && speriod < TIER6) {
    // Clear the LEDs
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;
    HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;

    // Drive six LEDs
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= \
         (BIT5HI | BIT4HI | BIT3HI | BIT2HI |BIT1HI); 
    HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) |= \
          (BIT4HI);
  } else if (speriod >= TIER6 && speriod < TIER7) {
    // Clear the LEDs
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;
    HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;

    // Drive seven LEDs
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= \
         (BIT5HI | BIT4HI | BIT3HI | BIT2HI |BIT1HI); 
    HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) |= \
          (BIT4HI | BIT3HI);
  } else if (speriod >= TIER7 && speriod < TIER8) {
    // Clear the LEDs
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;
    HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) = 0x0;

    // Drive eight LEDs
    HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= \
         (BIT5HI | BIT4HI | BIT3HI | BIT2HI |BIT1HI); 
    HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) |= \
          (BIT4HI | BIT3HI | BIT2HI);
  } else {
    // Set period to maximum value for int to cause output of 0
    period = INT_MAX;
    
  }
  // Lower GPIO pin PB0 for profiling
  //HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT0LO;
}
/****************************************************************************
 Function
   CalculateRPM

 Parameters
   None

 Returns
   None

 Description
   Update the state of the bar graph
 Notes

 Author
   Brian Jun, 01/22/19, 22:15
****************************************************************************/
void CalculateRPM(void) {
  // Raise GPIO pin PB1 for profiling
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT1HI;
  // Calculate RPM 
  rpm = (1250 * 1000 * 30 * 59) / (5120 * period);
  // Lower GPIO pin PB1 for profiling
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT1LO;
  // update period value
  last_period = period;
}

uint32_t GetRPM(void) {
  return rpm;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

