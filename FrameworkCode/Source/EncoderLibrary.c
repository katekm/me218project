/****************************************************************************
 Module
   EncoderService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "EncoderLibrary.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_nvic.h"

#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"

#include "ES_Port.h"
#include "termio.h"

/*----------------------------- Module Defines ----------------------------*/

#define BitsPerNibble 4
#define STOP_THRESH 999999999
#define NumberofMotors 2

#define TICKS_PER_REV 150 // Encoder ticks per post-gearing revolution
const uint32_t TicksPerSec = 40000000;

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

bool InitEncoderInterrupt(void);
void Encoder0_ISR(void);
void Encoder1_ISR(void);
uint32_t CalcRPM(uint8_t MotorNum);
int32_t QueryEncoderCount(uint8_t MotorNum);

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file

static volatile uint32_t LastCapture[NumberofMotors];
static volatile uint32_t Period[NumberofMotors];
static volatile int32_t EncoderCount[NumberofMotors];
static volatile int8_t MotorDir[NumberofMotors];
// static bool StopFlag[2] = {0, 0};

/***************************************************************************
 Function Definitions
 ***************************************************************************/

bool InitEncoderInterrupt(void) {
  // start by enabling the clock to the timer (Wide Timer 1)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R1;
  
  // enable the clock to Port C
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
  
  // since we added this Port C clock init, we can immediately start
  // into configuring the timer, no need for further delay
  // make sure that timers (Timer A/B) are disabled before configuring
  HWREG(WTIMER1_BASE+TIMER_O_CTL) &= (~TIMER_CTL_TAEN & ~TIMER_CTL_TBEN);
  
  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  HWREG(WTIMER1_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
  
  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value :-)
  HWREG(WTIMER1_BASE+TIMER_O_TAILR) = 0xffffffff;
  
  // set up timer A and B in capture mode (TAMR=3, TAAMS = 0),
  // for edge time (TACMR = 1) and up-counting (TACDIR = 1)
  HWREG(WTIMER1_BASE+TIMER_O_TAMR) =
  (HWREG(WTIMER1_BASE+TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) |
  (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  
  HWREG(WTIMER1_BASE+TIMER_O_TBMR) =
  (HWREG(WTIMER1_BASE+TIMER_O_TBMR) & ~TIMER_TBMR_TBAMS) |
  (TIMER_TBMR_TBCDIR | TIMER_TBMR_TBCMR | TIMER_TBMR_TBMR_CAP);
  
  // To set the event to rising edge, we need to modify the TAEVENT bits
  // in GPTMCTL. Rising edge = 00, so we clear the TAEVENT bits
  HWREG(WTIMER1_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
  HWREG(WTIMER1_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEVENT_M;
  
  // Now Set up the port to do the capture (clock was enabled earlier)
  // start by setting the alternate function for Port C bit 6/7 (WT1CCP0)
  HWREG(GPIO_PORTC_BASE+GPIO_O_AFSEL) |= (BIT6HI | BIT7HI);
  
  // Then, map bit 4's alternate function to WT0CCP0
  // 7 is the mux value to select WT1CCP0, 4 BitsPerNibble
  HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) =
  (HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) & 0xfff0ffff) + 
  (7 << 6 * BitsPerNibble) + (7 << 7 * BitsPerNibble);
  
  // Enable pins on Port C for digital I/O
  HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= 0xC0; // 0xC0 = 11000000
  
  // Enable pins on Port B for digital I/O
  HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= 0x03; // 0x03 = 00000011
  
  // make pins 6-7 on Port C into inputs
  HWREG(GPIO_PORTC_BASE+GPIO_O_DIR) &= (BIT6LO & BIT7LO);
  
  // make pins 0-1 on Port C into inputs
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) &= (BIT0LO & BIT1LO);
  
  // back to the timer to enable a local capture interrupt
  HWREG(WTIMER1_BASE+TIMER_O_IMR) |= TIMER_IMR_CAEIM | TIMER_IMR_CBEIM;
  
  // enable the Timer A/B in Wide Timer 1 interrupt in the NVIC
  // it is interrupt number 96/97 so it appears in EN3, Bit 0/1
  HWREG(NVIC_EN3) |= BIT0HI | BIT1HI;
  
  // make sure interrupts are enabled globally
  __enable_irq();
  
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER1_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
  HWREG(WTIMER1_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
  
  return true;
}

void Encoder0_ISR(void) {
  uint32_t ThisCapture;
  bool EncoderDir;
  // ES_Event_t Event2Post;
  
  // start by clearing the source of the interrupt, the input capture event
  HWREG(WTIMER1_BASE+TIMER_O_ICR) = TIMER_ICR_CAECINT;
  
  //HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) ^= BIT1HI;
  
  // Read direction of Encoder 0 (Port B, Pin 0)
  EncoderDir = HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) & BIT0HI;
    
  // now grab the captured value and calculate the period
  ThisCapture = HWREG(WTIMER1_BASE+TIMER_O_TAR);
  Period[0] = ThisCapture - LastCapture[0];
    
  // update LastCapture to prepare for the next edge
  LastCapture[0] = ThisCapture;
  
  // Increment/Decrement encoder depending on direction and log direction of rotation
  if( EncoderDir == 0 ) {
    EncoderCount[0]++;
    MotorDir[0] = -1;
  }
  else {
    EncoderCount[0]--;
    MotorDir[0] = 1;
  }
}

void Encoder1_ISR(void) {
  uint32_t ThisCapture;
  bool EncoderDir;
  // ES_Event_t Event2Post;
  
  // start by clearing the source of the interrupt, the input capture event
  HWREG(WTIMER1_BASE+TIMER_O_ICR) = TIMER_ICR_CBECINT;
  
  //HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) ^= BIT1HI;
  
  // Read direction of Encoder 1 (Port B, Pin 1)
  EncoderDir = HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) & BIT1HI;
    
  // now grab the captured value and calculate the period
  ThisCapture = HWREG(WTIMER1_BASE+TIMER_O_TBR);
  Period[1] = ThisCapture - LastCapture[1];
    
  // update LastCapture to prepare for the next edge
  LastCapture[1] = ThisCapture;
  
  // Increment/Decrement encoder depending on direction and log direction of rotation
  if( EncoderDir == 0 ) {
    EncoderCount[1]++;
    MotorDir[1] = -1;
  }
  else {
    EncoderCount[1]--;
    MotorDir[1] = 1;
  }
}

uint32_t CalcRPM(uint8_t MotorNum) {
  int32_t RPM;
  uint32_t ThisCapture = HWREG(WTIMER1_BASE+TIMER_O_TBR);
  
    RPM = (TicksPerSec * 60) / (Period[MotorNum] * TICKS_PER_REV);
    RPM *= MotorDir[MotorNum];
  
  return RPM;
}

int32_t QueryEncoderCount(uint8_t MotorNum) {
  return EncoderCount[MotorNum];
}
