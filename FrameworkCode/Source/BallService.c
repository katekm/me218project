/****************************************************************************
 Module
   BallService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "BallService.h"

#include "ServoLibrary.h"
#include "ProjectService.h"
#include "NavService.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

#define REJECT_TIME_MS 500
#define MAX_BALLS 10

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static BallState_t CurrentState;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitBallService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitBallService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostBallService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostBallService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunBallService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunBallService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ES_Event_t Event2Post;
  BallState_t NextState = CurrentState;
  uint8_t TeamColor;
  static volatile uint8_t BallCount;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************
   in here you write your service code
   *******************************************/
  switch(CurrentState) {
    case BallInitPState:
      if(ThisEvent.EventType == ES_INIT) {
        NextState = BallAwaitingCommand;
        RejectServoOff();
        DispenseServoOff();
      }
    break;
    
    case BallAwaitingCommand:
      switch(ThisEvent.EventType) {
        case BALL_REJECT:
          TeamColor = QueryTeamColor();
        
          // Reject ball if color is not our team color or blue (4)
          if(ThisEvent.EventParam != TeamColor && ThisEvent.EventParam != 4) {
            RejectServoOn();
            puts("\n\rRejecting ball.\n\r");
            ES_Timer_InitTimer(REJECT_TIMER, REJECT_TIME_MS);
          }
          
          // Otherwise, increase count of balls in storage
          else {
            BallCount++;
            
            // Ensure dispensing door is closed
            DispenseServoOff();
            
            if(BallCount == MAX_BALLS) {
              // Post ball full event to Project Service
              Event2Post.EventType = BALL_FULL;
              PostProjectService(Event2Post);
            }
          }
        break;
        
        case BALL_DISPENSE:
          DispenseServoOn();
        break;
        
        case ES_TIMEOUT:
          if(ThisEvent.EventParam == REJECT_TIMER) {
            RejectServoOff();
          }
        break;
        
        default:
        break;
      }
    break;
  }
  CurrentState = NextState;
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

