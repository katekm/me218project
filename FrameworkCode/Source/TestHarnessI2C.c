/****************************************************************************
 Module
   TestHarnessService0.c

 Revision
   1.0.1

 Description
   This is the first service for the Test Harness under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/26/17 18:26 jec     moves definition of ALL_BITS to ES_Port.h
 10/19/17 21:28 jec     meaningless change to test updating
 10/19/17 18:42 jec     removed referennces to driverlib and programmed the
                        ports directly
 08/21/17 21:44 jec     modified LED blink routine to only modify bit 3 so that
                        I can test the new new framework debugging lines on PF1-2
 08/16/17 14:13 jec      corrected ONE_SEC constant to match Tiva tick rate
 11/02/13 17:21 jec      added exercise of the event deferral/recall module
 08/05/13 20:33 jec      converted to test harness service
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/

// Hardware
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

#include <math.h>

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Port.h"

// Our application
#include "I2CService.h"

// This module
#include "TestHarnessI2C.h"

#include "BallService.h"

/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000
#define HALF_SEC (ONE_SEC / 2)
#define TWO_SEC (ONE_SEC * 2)
#define FIVE_SEC (ONE_SEC * 5)

#define COLOR_OPTIONS 7
#define COLOR_VECTOR_SIZE 3

#define RED_R_NORM 71.10
#define RED_G_NORM 14.76
#define RED_B_NORM 13.85

#define ORANGE_R_NORM 63.38
#define ORANGE_G_NORM 20.90
#define ORANGE_B_NORM 11.77

#define YELLOW_R_NORM 44.00
#define YELLOW_G_NORM 36.37
#define YELLOW_B_NORM 14.13

#define GREEN_R_NORM 35.12
#define GREEN_G_NORM 42.95
#define GREEN_B_NORM 16.42

#define BLUE_R_NORM 9.60
#define BLUE_G_NORM 31.80
#define BLUE_B_NORM 55.00

#define PINK_R_NORM 48.20
#define PINK_G_NORM 22.15
#define PINK_B_NORM 29.96

#define AMBIENT_R_NORM 47.55
#define AMBIENT_G_NORM 27.80
#define AMBIENT_B_NORM 23.10

#define MY_MIN(x,y) ((x) <= (y)) ? (x) : (y)
#define MY_MAX(x,y) ((x) >= (y)) ? (x) : (y)

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/


/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

// Options for colored garbage in ROYGBIV order 
float garbage_ref[COLOR_OPTIONS][COLOR_VECTOR_SIZE] = {
  {RED_R_NORM, RED_G_NORM, RED_B_NORM}, 
  {ORANGE_R_NORM, ORANGE_G_NORM, ORANGE_B_NORM}, 
  {YELLOW_R_NORM, YELLOW_G_NORM, YELLOW_B_NORM}, 
  {GREEN_R_NORM, GREEN_G_NORM, GREEN_B_NORM}, 
  {BLUE_R_NORM, BLUE_G_NORM, BLUE_B_NORM}, 
  {PINK_R_NORM, PINK_G_NORM, PINK_B_NORM}, 
  {AMBIENT_R_NORM, AMBIENT_G_NORM, AMBIENT_B_NORM}
}; 

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTestHarnessI2C

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 02/03/19, 10:46
****************************************************************************/
bool InitTestHarnessI2C(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTestHarnessI2C

 Parameters
     ES_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
J. Edward Carryer, 02/03/19, 10:48
****************************************************************************/
bool PostTestHarnessI2C(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTestHarnessI2C

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
J. Edward Carryer, 02/03/19, 10:49
****************************************************************************/
ES_Event_t RunTestHarnessI2C(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ES_Event_t Event2Post;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  static uint8_t prev_sensed_color;
  switch (ThisEvent.EventType)
  {
    case ES_INIT:
    {
      ES_Timer_InitTimer(I2C_TEST_TIMER, (ONE_SEC));
      puts("I2C TestHarness:");
      printf("\rES_INIT received in Service %d\r\n", MyPriority);
    }
    break;
    case ES_TIMEOUT:   // re-start timer & announce
    {
      uint16_t ClearValue;
      uint16_t RedValue;
      uint16_t GreenValue;
      uint16_t BlueValue;
      
      // Timing
      ES_Timer_InitTimer(I2C_TEST_TIMER, 50);
      ClearValue = I2C_GetClearValue();
      RedValue   = I2C_GetRedValue();
      GreenValue = I2C_GetGreenValue();
      BlueValue  = I2C_GetBlueValue();
      /*
      printf("Clr: %d, Red: %d, Grn: %d, Blu: %d, R%%: %.2f, G%% %.2f, B%% %.2f \r\n",
          ClearValue, RedValue, GreenValue, BlueValue, 
          ((float)RedValue*100/ClearValue),
          ((float)GreenValue*100/ClearValue),
          ((float)BlueValue*100/ClearValue)); */
      
      uint8_t sensed_color = FindClosestColor(((float)RedValue*100/ClearValue),
                                              ((float)GreenValue*100/ClearValue),
                                               ((float)BlueValue*100/ClearValue)); 
                                              
      // If detected color is different from previous and is not ambient

      if( sensed_color != prev_sensed_color && sensed_color != 6 ) {
        Event2Post.EventParam = sensed_color;
        Event2Post.EventType = BALL_REJECT;
        //PostBallService(Event2Post);
        prev_sensed_color = sensed_color;
      }
    }
    break;
    case ES_NEW_KEY:   // announce
    {
      printf("ES_NEW_KEY received with -> %c <- in Service 0\r\n",
          (char)ThisEvent.EventParam);
    }
    break;
    default:
    {}
     break;
  }

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

float FloatDotProduct(float red_a, float green_a, float blue_a,
                      float red_b, float green_b, float blue_b) {
  return (red_a * red_b) + (green_a * green_b) + (blue_a * blue_b);
}

float FloatL2Norm (float red_a, float green_a, float blue_a) {
  return sqrt((red_a * red_a) + (green_a * green_a) + (blue_a * blue_a));
}

uint8_t FindClosestColor(float input_red, float input_green, float input_blue){
  float cosine_array[COLOR_OPTIONS];
  float red_cosine = FloatDotProduct(input_red, input_green, input_blue,
                               garbage_ref[0][0], garbage_ref[0][1], garbage_ref[0][2]) /
              (FloatL2Norm(input_red, input_green, input_blue) * 
               FloatL2Norm(garbage_ref[0][0], garbage_ref[0][1], garbage_ref[0][2]));
  float orange_cosine = FloatDotProduct(input_red, input_green, input_blue,
                               garbage_ref[1][0], garbage_ref[1][1], garbage_ref[1][2]) /
              (FloatL2Norm(input_red, input_green, input_blue) * 
               FloatL2Norm(garbage_ref[1][0], garbage_ref[1][1], garbage_ref[1][2]));
  float yellow_cosine = FloatDotProduct(input_red, input_green, input_blue,
                               garbage_ref[2][0], garbage_ref[2][1], garbage_ref[2][2]) /
              (FloatL2Norm(input_red, input_green, input_blue) * 
               FloatL2Norm(garbage_ref[2][0], garbage_ref[2][1], garbage_ref[2][2]));
  float green_cosine = FloatDotProduct(input_red, input_green, input_blue,
                               garbage_ref[3][0], garbage_ref[3][1], garbage_ref[3][2]) /
              (FloatL2Norm(input_red, input_green, input_blue) * 
               FloatL2Norm(garbage_ref[3][0], garbage_ref[3][1], garbage_ref[3][2]));
  float blue_cosine = FloatDotProduct(input_red, input_green, input_blue,
                               garbage_ref[4][0], garbage_ref[4][1], garbage_ref[4][2]) /
              (FloatL2Norm(input_red, input_green, input_blue) * 
               FloatL2Norm(garbage_ref[4][0], garbage_ref[4][1], garbage_ref[4][2]));
  float pink_cosine = FloatDotProduct(input_red, input_green, input_blue,
                               garbage_ref[5][0], garbage_ref[5][1], garbage_ref[5][2]) /
              (FloatL2Norm(input_red, input_green, input_blue) * 
               FloatL2Norm(garbage_ref[5][0], garbage_ref[5][1], garbage_ref[5][2]));
  float ambient_cosine = FloatDotProduct(input_red, input_green, input_blue,
                               garbage_ref[6][0], garbage_ref[6][1], garbage_ref[6][2]) /
              (FloatL2Norm(input_red, input_green, input_blue) * 
               FloatL2Norm(garbage_ref[6][0], garbage_ref[6][1], garbage_ref[6][2]));
  //printf("Red cosine %f\n\r", red_cosine);
  //printf("Orange cosine %f\n\r", orange_cosine);
  //printf("Yellow cosine %f\n\r", yellow_cosine);
  //printf("Green cosine %f\n\r", green_cosine);
  //printf("Blue cosine %f\n\r", blue_cosine);
  //printf("Pink cosine %f\n\r", pink_cosine);
  //printf("Ambient cosine %f\n\r", ambient_cosine);

  float min_cosine = MY_MAX(MY_MAX(MY_MAX(MY_MAX(red_cosine, orange_cosine), 
                       MY_MAX(yellow_cosine, green_cosine)),
                       MY_MAX(blue_cosine, pink_cosine)),ambient_cosine);
  
  if (min_cosine == red_cosine) {
    //printf("Red\n\r");
    return 0;
  } else if (min_cosine == orange_cosine) {
    //printf("Orange\n\r");
    return 1;
  } else if (min_cosine == yellow_cosine) {
    //printf("Yellow\n\r");
    return 2;
  } else if (min_cosine == green_cosine) {
    //printf("Green\n\r");
    return 3;
  } else if (min_cosine == blue_cosine) {
    //printf("Blue\n\r");
    return 4;
  } else if (min_cosine == pink_cosine) {
    //printf("Pink\n\r");
    return 5;
  } else {
    //printf("Ambient\n\r");
    return 6;
  }
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

