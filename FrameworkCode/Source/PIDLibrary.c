/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "PIDLibrary.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_nvic.h"

#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"

#include "ES_Port.h"
#include "termio.h"

#include "EncoderLibrary.h"
#include "MotorLibrary.h"

/*----------------------------- Module Defines ----------------------------*/

#define BitsPerNibble 4
#define NumberofMotors 2
#define TicksPerMS 40000

#define Kp 1
#define Ki 0.1

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

void InitPIDInterrupt(void);
void PID_Handler(void);
uint32_t QueryPIDCounter(void);

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file

static volatile uint32_t PIDCounter = 0;

/***************************************************************************
 Function Definitions
 ***************************************************************************/
void InitPIDInterrupt(void){
 // start by enabling the clock to the timer (Wide Timer 1)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R1;
  
  // kill a few cycles to let the clock get going
  while((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R1) != SYSCTL_PRWTIMER_R1)
  {
    ;
  }
  
  // make sure that timer (Timer A) is disabled before configuring
  HWREG(WTIMER1_BASE+TIMER_O_CTL) &= (~TIMER_CTL_TAEN);
  
  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  HWREG(WTIMER1_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
  
  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value :-)
  HWREG(WTIMER1_BASE+TIMER_O_TAILR) = 0xffffffff;
  
  // set up timer A in periodic mode (TBMR = 1)
  HWREG(WTIMER1_BASE+TIMER_O_TAMR) = 
  (HWREG(WTIMER1_BASE+TIMER_O_TAMR) & ~TIMER_TAMR_TAMR_M) | 
  TIMER_TAMR_TAMR_PERIOD;
  
  // set timeout 
  HWREG(WTIMER1_BASE+TIMER_O_TAILR) = TicksPerMS * 2;
  
  // enable a local timeout interrupt. 
  HWREG(WTIMER1_BASE+TIMER_O_IMR) |= TIMER_IMR_TATOIM;
  
  // enable the Timer A in Wide Timer 1 interrupt in the NVIC
  // it is interrupt number 96 so appears in EN3 at bit 0
  HWREG(NVIC_EN3) |= BIT0HI;
  
  // Set lower priority for NVIC Interrupt 96 (Wide Timer 1A)
  HWREG(NVIC_PRI24) = (HWREG(NVIC_PRI24) & ~NVIC_PRI24_INTA_M) 
  + (1 << NVIC_PRI24_INTA_S);
  
  // make sure interrupts are enabled globally
  __enable_irq();
  
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER1_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
 }
 
void PID_Handler(void) {
  static int32_t SumError[2];
  static int32_t TargetRPM[2];
  int32_t RPMError[2];
  int32_t CurrentRPM[2];
  int8_t RequestedDuty[2];
  
  // Raise Pin B0
  HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= BIT0HI;
  
  // start by clearing the source of the interrupt (Wide Timer 1A)
  HWREG(WTIMER1_BASE+TIMER_O_ICR) = TIMER_ICR_TATOCINT;
  
  // Calculate duty cycle for each motor
  for( uint8_t i = 0; i < NumberofMotors; i++) {
    // Calculate RPM from Period recorded by encoder
    CurrentRPM[i] = CalcRPM(i);
    
    // Retrieve TargetRPM from MotorTest service
    //TargetRPM[i] = QueryTargetRPM(i);
    
    // Calculate current error
    RPMError[i] = TargetRPM[i] - CurrentRPM[i];
    
    // Calculate integral error
    SumError[i] += RPMError[i];
    
    // Calculate duty cycle from error terms
    RequestedDuty[i] = Kp*(RPMError[i] + (Ki * SumError[i]));
    
    // Anti-Windup for integrator
    if (RequestedDuty[i] > 100) {
      RequestedDuty[i] = 100;
      SumError[i] -= RPMError[i];
    }
    else if (RequestedDuty[i] < -100) {
      RequestedDuty[i] = -100;
      SumError[i] -= RPMError[i];
    }
    
    SetDutyCycle(RequestedDuty[i], i);
  }
  
  // Increment counter to ensure the ISR is running
  PIDCounter++;
}

uint32_t QueryPIDCounter(void) {
  return PIDCounter;
}
