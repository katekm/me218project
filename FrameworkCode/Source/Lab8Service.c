/****************************************************************************
 Module
   Lab8Service.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Events.h"

/* include header files for hardware access
*/
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"

#include "Lab8Service.h"
#include "MotorLibrary.h"
#include "SSIService.h"

/*----------------------------- Module Defines ----------------------------*/
#define DELAY_90DEG 1700
#define DELAY_45DEG 850
#define FULL_SPEED 95
#define HALF_SPEED 75 // duty cycle
#define IR_PERIOD_TICKS 13800 // Define for infrared high time in clock ticks

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

void InitIRInputCapture(void);
void IRInputCaptureISR(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static Lab8State_t CurrentState;

static volatile uint32_t this_capture;
static volatile uint32_t period;
static volatile uint32_t last_capture;
static volatile uint32_t last_period;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitLab8Service

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitLab8Service(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  // Initialize motor hardware
  InitMotorHW();
  
  // Initialize IR Input Capture
  InitIRInputCapture();
  
  // post the initial transition event
  CurrentState = Lab8InitPState;
  
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostLab8Service

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostLab8Service(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunLab8Service

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunLab8Service(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ES_Event_t Event2Post;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  Lab8State_t NextState;
  static volatile uint8_t CurrentCommand;
  static volatile uint32_t TapeTime;
  static bool enable_flag = true;
  
  /********************************************
   in here you write your service code
   *******************************************/
  
  switch (CurrentState) {
    
    case Lab8InitPState:
      if( ThisEvent.EventType == ES_INIT ) {
        NextState = AwaitingCommand;
      }
    break;
    
    case AwaitingCommand: {
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= ~BIT0HI;
    
      if( (ThisEvent.EventType == SSI_COMMAND && enable_flag == 1) ||
           ThisEvent.EventType == SSI_TEST ) {
        CurrentCommand = ThisEvent.EventParam;
        
        // Stop motor 0
        SetDutyCycle(0, 0);
        // Stop motor 1
        SetDutyCycle(0, 1);
        
        HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= ~BIT5HI & ~BIT6HI;
        
        switch (CurrentCommand) {
          case 0x00:
          // Test statement
          printf("Stop, hold position, do not move or rotate\n\r");
          
          // TODO: execute command (Stop, hold position, do not move or rotate)
          
          // Stop motor 0
          SetDutyCycle(0, 0);
          // Stop motor 1
          SetDutyCycle(0, 1);
          break;

        case 0x02:
          // Test statement
          printf("Rotate Clockwise by 90 degrees\n\r");
        
          // TODO: execute command (Rotate clockwise 90 degrees)
        
          // Rotate motor 1
          SetDutyCycle(FULL_SPEED, 0);
          // Rotate motor 2
          SetDutyCycle(-FULL_SPEED, 1);
        
          // Initiate timer for stopping motors
          ES_Timer_InitTimer(STOP_TIMER, DELAY_90DEG);
          //NextState = ExecutingCommand;
          break;

        case 0x03:
          // Test statement
          printf("Rotate Clockwise by 45 degrees\n\r");
        
          // TODO: execute command (Rotate clockwise 45 degrees)
        
          // Rotate motor 1
          SetDutyCycle(FULL_SPEED, 0);
          // Rotate motor 2
          SetDutyCycle(-FULL_SPEED, 1);
        
          // Initiate timer for stopping motors
          ES_Timer_InitTimer(STOP_TIMER, DELAY_45DEG);
          //NextState = ExecutingCommand;
          break;

        case 0x04: 
          // Test statement
          printf("Rotate Counter-clockwise by 90 degrees\n\r");
        
          // TODO: execute command (Rotate counterclockwise 90 degrees)
        
          // Rotate motor 1
          SetDutyCycle(-FULL_SPEED, 0);
          // Rotate motor 2
          SetDutyCycle(FULL_SPEED, 1);
        
          // Initiate timer for stopping motors
          ES_Timer_InitTimer(STOP_TIMER, DELAY_90DEG);
          //NextState = ExecutingCommand;
          break;

        case 0x05:
          // Test statement
          printf("Rotate Counter-clockwise by 45 degrees\n\r");
        
          // TODO: execute command (Rotate counterclockwise 45 degrees)
        
          // Rotate motor 1
          SetDutyCycle(-FULL_SPEED, 0);
          // Rotate motor 2
          SetDutyCycle(FULL_SPEED, 1);
        
          // Initiate timer for stopping motors
          ES_Timer_InitTimer(STOP_TIMER, DELAY_45DEG);
          //NextState = ExecutingCommand;

          break;

        case 0x08:
          // Test statement
          printf("Drive forward half speed\n\r");
        
          // TODO: execute command (drive forward at half speed)
          
          // Rotate motor 1
          SetDutyCycle(HALF_SPEED, 0);
          // Rotate motor 2
          SetDutyCycle(HALF_SPEED, 1);
        
          break;

        case 0x09:
          // Test statement
          printf("Drive forward full speed\n\r");
        
          // TODO: execute command (drive forward at full speed)
        
          // Rotate motor 1
          SetDutyCycle(FULL_SPEED, 0);
          // Rotate motor 2
          SetDutyCycle(FULL_SPEED, 1);
        
          break;

        case 0x10: 
          // Test statement
          printf("Drive in reverse half speed\n\r");
        
          // TODO: execute command (drive in reverse at half speed)
        
          // Rotate motor 1
          SetDutyCycle(-HALF_SPEED, 0);
          // Rotate motor 2
          SetDutyCycle(-HALF_SPEED, 1);
        
          break;

        case 0x11:
          // Test statement
          printf("Drive in reverse full speed\n\r");
        
          // TODO: execute command (drive in reverse at flul speed)
        
          // Rotate motor 1
          SetDutyCycle(-FULL_SPEED, 0);
          // Rotate motor 2
          SetDutyCycle(-FULL_SPEED, 1);
        
          break;

        case 0x20: 
          // Test statement
          printf("Align with beacon\n\r");
        
          // TODO: execute command (rotate until beacon detected)
        
          // Rotate motor 1
          SetDutyCycle(-FULL_SPEED, 0);
          // Rotate motor 2
          SetDutyCycle(FULL_SPEED, 1);
        
          HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT6HI;
        
          ES_Timer_InitTimer(STOP_TIMER, 5000);
        
          //NextState = ExecutingCommand;
        
          break;

        case 0x40:
          // Test statement
          printf("Drive forward until tape detected\n\r");
        
          // TODO: execute command (move forward until tape detected)
        
          // Rotate motor 1
          SetDutyCycle(FULL_SPEED, 0);
          // Rotate motor 2
          SetDutyCycle(FULL_SPEED, 1);
        
          HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT5HI;
        
            ES_Timer_InitTimer(STOP_TIMER, 30000);
        
          //NextState = ExecutingCommand;
        
          break;

        default:
          printf("Invalid code received\n\r");
          break;
        }
      }
      
      else if( ThisEvent.EventType == ES_NEW_KEY ) {
        
        switch( ThisEvent.EventParam ) {
          
          case 'a':
            Event2Post.EventType = SSI_TEST;
            Event2Post.EventParam = 0x00;
            PostLab8Service(Event2Post);
          break;
          
          case 'b':
            Event2Post.EventType = SSI_TEST;
            Event2Post.EventParam = 0x02;
            PostLab8Service(Event2Post);
          break;
          
          case 'c':
            Event2Post.EventType = SSI_TEST;
            Event2Post.EventParam = 0x03;
            PostLab8Service(Event2Post);
          break;
          
          case 'd':
            Event2Post.EventType = SSI_TEST;
            Event2Post.EventParam = 0x04;
            PostLab8Service(Event2Post);
          break;
          
          case 'e':
            Event2Post.EventType = SSI_TEST;
            Event2Post.EventParam = 0x05;
            PostLab8Service(Event2Post);
          break;
          
          case 'f':
            Event2Post.EventType = SSI_TEST;
            Event2Post.EventParam = 0x08;
            PostLab8Service(Event2Post);
          break;
          
          case 'g':
            Event2Post.EventType = SSI_TEST;
            Event2Post.EventParam = 0x09;
            PostLab8Service(Event2Post);
          break;
          
          case 'h':
            Event2Post.EventType = SSI_TEST;
            Event2Post.EventParam = 0x10;
            PostLab8Service(Event2Post);
          break;
          
          case 'i':
            Event2Post.EventType = SSI_TEST;
            Event2Post.EventParam = 0x11;
            PostLab8Service(Event2Post);
          break;
          
          case 'j':
            Event2Post.EventType = SSI_TEST;
            Event2Post.EventParam = 0x20;
            PostLab8Service(Event2Post);
          break;
          
          case 'k':
            Event2Post.EventType = SSI_TEST;
            Event2Post.EventParam = 0x40;
            PostLab8Service(Event2Post);
          break;
          
          case 't':
            enable_flag ^= 1;
            printf("\n\rCommand Generator flag: %d.\n\r", enable_flag);
          break;
          
          default:
            
          break;
        }
      }
      
      else if( ThisEvent.EventType == ES_TIMEOUT &&
          ThisEvent.EventParam == STOP_TIMER ) {
            
            // Stop motor 0
            SetDutyCycle(0, 0);
            // Stop motor 1
            SetDutyCycle(0, 1);
            
            puts("\n\r Stop timer expired.  Stopping motors.\n\r");
          }
          
      else if( ThisEvent.EventType == BEACON_DETECTED && CurrentCommand == 0x20 ) {
        // Stop motor 0
        SetDutyCycle(0, 0);
        // Stop motor 1
        SetDutyCycle(0, 1);
            
        HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT6LO;
            
        puts("\n\r Beacon detected.  Stopping motors.\n\r");
      }
      
      else if( ThisEvent.EventType == TAPE_DETECTED && CurrentCommand == 0x40 ) {
        // Stop motor 0
        SetDutyCycle(0, 0);
        // Stop motor 1
        SetDutyCycle(0, 1);
            
        HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT5LO;
            
        puts("\n\r Tape detected.  Stopping motors.\n\r");
      }
      
        break;
    }
      
    break;
    
    case ExecutingCommand:
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT0HI;
    
      switch( ThisEvent.EventType) {
        
        /*
        
        case SSI_COMMAND:
          if( enable_flag == 1 ) {
            // Abort current operation and execute new command
            Event2Post.EventType = SSI_COMMAND;
            Event2Post.EventParam = ThisEvent.EventParam;
            NextState = AwaitingCommand;
          
            HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT6LO;
            HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT5LO;
          
            puts("\n\rNew command received; old command terminated.\n\r");
          }
        break;
        */
        
        case SSI_TEST:
          // Abort current operation and execute new command
          Event2Post.EventType = SSI_TEST;
          Event2Post.EventParam = ThisEvent.EventParam;
          NextState = AwaitingCommand;
        
          HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT6LO;
          HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT5LO;
        
          puts("\n\rNew test command received; old command terminated.\n\r");
        break;
          
        case BEACON_DETECTED:
          // If current command is to find beacon, stop motors
          if( CurrentCommand == 0x20 ) {
            // Stop motor 0
            SetDutyCycle(0, 0);
            // Stop motor 1
            SetDutyCycle(0, 1);
            
            HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT6LO;
            
            puts("\n\r Beacon detected.  Stopping motors.\n\r");
            
            NextState = AwaitingCommand;
          }
        break;
        
        case TAPE_DETECTED:
          
          printf("In tape detected; current_command = %x\n\r", CurrentCommand);
          // If current command is to find tape, stop motors
          if( CurrentCommand == 0x40 ) {
            // Stop motor 0
            SetDutyCycle(0, 0);
            // Stop motor 1
            SetDutyCycle(0, 1);
            
            HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT5LO;
            
            puts("\n\r Tape detected.  Stopping motors.\n\r");
            
            NextState = AwaitingCommand;
          }
        break;
          
        case ES_TIMEOUT:
          if( ThisEvent.EventParam == STOP_TIMER ) {
            
            // Stop motor 0
            SetDutyCycle(0, 0);
            // Stop motor 1
            SetDutyCycle(0, 1);
            
            puts("\n\r Stop timer expired.  Stopping motors.\n\r");
            
            NextState = AwaitingCommand;
          }
        break;
          
        default:
          // Nothing happens here
        break;
      }
    break;
  }
  
  CurrentState = NextState;
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/****************************************************************************
 Function
    InitInputCapturePeriod

 Parameters
   None

 Returns
   None

 Description
   Initialize WTIMER0 to do input capture on encoder values
 Notes

 Author
   Brian Jun, 01/22/19, 21:31
****************************************************************************/
void InitIRInputCapture(void) {

  // Requires a change in the startup_rvmdk.s file to the WTIMER0
  // interrupt vector

  // Enable clock to timer (WTIMER0)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
  
  // Enable the clock to Port C 
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
  
  // Make sure that Timer A is disbaled before configuring 
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
  
  // Set timer to 32-bit wide mode
  HWREG(WTIMER0_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
  
  // Register the FULL 32-bit value)
  HWREG(WTIMER0_BASE + TIMER_O_TAILR) = 0xFFFFFFFF;
  
  // Set up timer A in capture mode
  HWREG(WTIMER0_BASE + TIMER_O_TAMR) = (HWREG(WTIMER0_BASE + TIMER_O_TAMR) &\
                                        ~TIMER_TAMR_TAAMS) | (TIMER_TAMR_TACDIR | \
                                        TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  
  // Using one timer, capture both edges
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
  
  // Trigger on both rising and falling edge
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |= (BIT3HI | BIT2HI);

  // Setup the port to do the capture by setting alternate function
  HWREG(GPIO_PORTC_BASE + GPIO_O_AFSEL) |= BIT4HI;

  // Map alternate function
  HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) & 0xFFF0FFFF) + \
                                         (7 << 16);

  // Enable pin 4 on Port C for digital I/O
  HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= BIT4HI | BIT5HI | BIT6HI; 

  // Designate pin 4 on Port C as an input
  HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) &= BIT4LO;
  HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) |= BIT5HI | BIT6HI;

  // Enable local capture interrupt 
  HWREG(WTIMER0_BASE + TIMER_O_IMR) |= TIMER_IMR_CAEIM;

  // Enable timer A in WTIMER0 interrupt in NVIC
  // Interrupt 94, so right shift by 5 and mod by 32
  HWREG(NVIC_EN2) |= BIT30HI;

  // Make sure interrupts are enabled globally
  __enable_irq();

  // Enable timer and its stall feature
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
}

/****************************************************************************
 Function
    InputCaptureResponse

 Parameters
   None

 Returns
   None

 Description
   Capture incoming edges and save period
 Notes

 Author
   Brian Jun, 01/22/19, 21:33
****************************************************************************/
void IRInputCaptureISR(void) {
  

    
  // Debug
  //HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT1HI;
  
  // Clear the source of the interrupt
  HWREG(WTIMER0_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;
  
  // Calculate period from captured value
  this_capture = HWREG(WTIMER0_BASE + TIMER_O_TAR);
  
  // Clear captured edge
  HWREG(WTIMER0_BASE + TIMER_O_TAR) &= 0x0;

  // Calculate most recent period
  period = this_capture - last_capture;

  // Update last_capture
  last_capture = this_capture;
  
  if( ( period > ((85 * IR_PERIOD_TICKS)/100) ) && ( (period < ((115 * IR_PERIOD_TICKS)/100) )) ) {
    ES_Event_t Event2Post;
    Event2Post.EventType = BEACON_DETECTED;
    PostLab8Service(Event2Post);
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT0HI;
    
  }
  else {
    HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT0LO;
  
  last_period = period;
  
  // Debug
  //HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT1LO;
  }

}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

