/****************************************************************************
 Module
   NavService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "NavService.h"
#include "DCMotorService.h"
#include "MotorLibrary.h"
#include "EncoderLibrary.h"
#include "ProjectService.h"
#include "IRDetector.h"
#include "InductiveLibrary.h"
#include "COMPASSLibrary.h"
#include "ServoLibrary.h"
/*----------------------------- Module Defines ----------------------------*/

#define RIGHT_MOTOR 1
#define LEFT_MOTOR 0
#define INTAKE_MOTOR 2
#define SHOOTER_MOTOR 3

#define BACKUP_TIME_MS 600
#define ROOMBA_TIME_MS 1000
#define LINE_TIME_MS 300
#define ALIGNMENT_TIMER_MS 100

// The variable name says RPM, but it will be DutyCycle until PID is operational
#define BALL_FINDING_RPM 40    // Backwards because intake rollers are in back
#define BACKUP_RPM 45
#define LINE_FINDING_RPM 40
#define LINE_FOLLOWING_RPM 20

#define ROOMBA_ROTATE_RPM 40
#define RECYCLE_FINDING_ROTATE_RPM 30
#define LINE_ROTATE_RPM 20

#define WALL_THRESHOLD 50

#define RPM_LIMIT 100

#define IR_THRESHOLD 20


/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

void EnableIntake(void);
void DisableIntake(void);
void EnableShooter(void);
void DisableShooter(void);
void DisableLineFollowing(void);
void EnableLineFollowing(void);

int16_t QueryLineFollowingRPM(void);
int16_t QueryDCTarget (uint8_t MotorNum);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable

static uint8_t MyPriority;
static NavState_t CurrentState;
static int IRCounter = 0;
static volatile int LastCenterIndex = 4; //indexing used for IR counter

//static volatile int32_t PositionTarget[2];   // Encoder position target for motors
static volatile int16_t DCTarget[2] = {0,0};         // RPM Target for drive motors
static volatile uint8_t MisalignedCondVar = 0; 


/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitNavService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitNavService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  
  CurrentState = NavInitPState;
  /********************************************
   in here you write your initialization code
   *******************************************/
  
    printf("Running NAV service\n\r");
  
  // Enable timer 3 for Line-following PD scheme
  HWREG(WTIMER3_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostNavService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostNavService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunNavService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunNavService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ES_Event_t Event2Post;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  NavState_t NextState = CurrentState;
  
  int32_t CurrEncoderValue[2];
  static int32_t PrevEncoderValue[2];
  static uint8_t AlignedCount;
  
  /********************************************
   in here you write your service code
   *******************************************/
  

  switch(CurrentState) {
    case NavInitPState:
      if(ThisEvent.EventType == ES_INIT) {
        NextState = NavAwaitingCommand;
        
        DisableIntake();
        DisableShooter();
        DisableLineFollowing();
        Event2Post.EventType = DR_STOP_ALL;
        PostDCMotorService(Event2Post);
        
        if(1) { // for field testing
        NextState = NavLineFinding; //TEMP FOR 
        Event2Post.EventType = DR_DRIVE_ALL;
        Event2Post.EventParam = LINE_FINDING_RPM;
        PostDCMotorService(Event2Post);
      }
      }
    break;
      
    case NavAwaitingCommand:
      
      switch(ThisEvent.EventType) {
        case NV_FIND_BALLS:
          // Commence ball finding algorithm
          NextState = NavBallFinding;
        
          // Send robot backward X RPM
          Event2Post.EventType = DR_DRIVE_ALL_REV;
          Event2Post.EventParam = BALL_FINDING_RPM;
          PostDCMotorService(Event2Post);
        
          EnableIntake();
        
          // Using duty cycle as placeholder until PID is operational
          //SetDutyCycle(BALL_FINDING_RPM, LEFT_MOTOR);
          //SetDutyCycle(BALL_FINDING_RPM, RIGHT_MOTOR);
        
        break;
        
        case NV_DEPOSIT_BALLS:
          // Commence ball deposit algorithm
          NextState = NavLineFinding;
        
          // Send robot forward X RPM
          Event2Post.EventType = DR_DRIVE_ALL;
          Event2Post.EventParam = LINE_FINDING_RPM;
          PostDCMotorService(Event2Post);
          /*
          // Using duty cycle as placeholder until PID is operational
          SetDutyCycle(LINE_FINDING_RPM, LEFT_MOTOR);
          SetDutyCycle(LINE_FINDING_RPM, RIGHT_MOTOR);
          */
        break;
        
        default:
          
        break;
      }
      

#if 1
      // Keyboard commands for testing.  Comment out when not needed!
      if( ThisEvent.EventType == ES_NEW_KEY ) {
        switch (ThisEvent.EventParam) {
          case 'x':
            DCTarget[RIGHT_MOTOR] = 0;
            DCTarget[LEFT_MOTOR] = 0;
            printf("\n\rAll Target DC = %d.\n\r", DCTarget[RIGHT_MOTOR]);
          
            //SetDutyCycle(DCTarget[RIGHT_MOTOR], RIGHT_MOTOR);
          
          
            Event2Post.EventType = DR_STOP_ALL;
            PostDCMotorService(Event2Post);
          
          break;
            
          case 'R':
            DCTarget[RIGHT_MOTOR] += 1;
            printf("\n\rRight Target DC = %d.\n\r", DCTarget[RIGHT_MOTOR]);
          
            //SetDutyCycle(DCTarget[RIGHT_MOTOR], RIGHT_MOTOR);
          
          
          
            if (DCTarget[RIGHT_MOTOR] > RPM_LIMIT) {
              DCTarget[RIGHT_MOTOR] = RPM_LIMIT;
            }
          
            Event2Post.EventType = DR_DRIVE_RIGHT;
            Event2Post.EventParam = DCTarget[RIGHT_MOTOR];
            PostDCMotorService(Event2Post);
          
          break;
          
          case 'r':
            DCTarget[RIGHT_MOTOR] -= 1;
            printf("\n\rRight Target DC = %d.\n\r", DCTarget[RIGHT_MOTOR]);
          
          //SetDutyCycle(DCTarget[RIGHT_MOTOR], RIGHT_MOTOR);
          
            if (DCTarget[RIGHT_MOTOR] < -RPM_LIMIT) {
              DCTarget[RIGHT_MOTOR] = -RPM_LIMIT;
            }
          
            Event2Post.EventType = DR_DRIVE_RIGHT;
            Event2Post.EventParam = DCTarget[RIGHT_MOTOR];
            PostDCMotorService(Event2Post);
          
          break;
          
          case 'L':
            DCTarget[LEFT_MOTOR] += 1;
            printf("\n\rLeft Target DC = %d.\n\r", DCTarget[LEFT_MOTOR]);
          
          //SetDutyCycle(DCTarget[LEFT_MOTOR], LEFT_MOTOR);
         
            if (DCTarget[LEFT_MOTOR] > RPM_LIMIT) {
              DCTarget[LEFT_MOTOR] = RPM_LIMIT;
            }
          
            Event2Post.EventType = DR_DRIVE_LEFT;
            Event2Post.EventParam = DCTarget[LEFT_MOTOR];
            PostDCMotorService(Event2Post);
          
          break;
          
          case 'l':
            DCTarget[LEFT_MOTOR] -= 1;
            printf("\n\rLeft Target DC = %d.\n\r", DCTarget[LEFT_MOTOR]);
          
          //SetDutyCycle(DCTarget[LEFT_MOTOR], LEFT_MOTOR);
         
            if (DCTarget[LEFT_MOTOR] < -RPM_LIMIT) {
              DCTarget[LEFT_MOTOR] = -RPM_LIMIT;
            }
          
            Event2Post.EventType = DR_DRIVE_LEFT;
            Event2Post.EventParam = DCTarget[LEFT_MOTOR];
            PostDCMotorService(Event2Post);
          
          break;
          
          case 'n':
            DisableIntake();
            puts("\n\rIntake motor disabled.\n\r");
          break;
          
          case 'N':
            EnableIntake();
          puts("\n\rIntake motor enabled.\n\r");
          break;
          
          case 'F':
            // Commence ball finding algorithm
            printf("Starting ball finding \n\r");
          
            NextState = NavBallFinding;
          
            // Send robot backward X RPM
            Event2Post.EventType = DR_DRIVE_ALL_REV;
            Event2Post.EventParam = BALL_FINDING_RPM;
            PostDCMotorService(Event2Post);
            
            printf("Moving backwards \n\r");
          break;
          
          case 'C':
            // Commence IR finding
            printf("Starting IR finding \n\r");

            NextState = NavRecyclingFinding;

          break;
          
          case 'D':
            // Commence ball deposit algorithm
            printf("Starting ball depositing \n\r");

            NextState = NavLineFinding;
          
            // Send robot forward X RPM
            Event2Post.EventType = DR_DRIVE_ALL;
            Event2Post.EventParam = LINE_FINDING_RPM;
            PostDCMotorService(Event2Post);
          
            // Using duty cycle as placeholder until PID is operational
            SetDutyCycle(LINE_FINDING_RPM, LEFT_MOTOR);
            SetDutyCycle(LINE_FINDING_RPM, RIGHT_MOTOR);
          break;
          
          case 'Q':
            RejectServoOn();
          break;
          
          case 'q':
            RejectServoOff();
          break;
          
          case 'v':
            RejectServoBlock();
          break;
          
          case 'E':
            DispenseServoOn();
          break;
          
          case 'e':
            DispenseServoOff();
          break;
          
          default:
          break;
        }
      } 
    break;
    #endif 
   
    case NavBallFinding:
      
      // Modified roomba algorithm:
      // The robot moves BACKWARD (where the intake is) until it hits an obstacle 
      // Then, it rotates X degrees and goes forward again
      // This continues until ProjectService tells it to find a line.
    
      switch(ThisEvent.EventType) {
        case ES_DBLIMIT_BR_DN:

          // "Back up" robot a little
          Event2Post.EventType = DR_DRIVE_ALL_REV;
          Event2Post.EventParam = BACKUP_RPM;
          PostDCMotorService(Event2Post);
          
          ES_Timer_InitTimer(NV_BACKUP_TIMER, BACKUP_TIME_MS);
        break;

        case ES_DBLIMIT_BL_DN:
          
          // "Back up" robot a little
          Event2Post.EventType = DR_DRIVE_ALL_REV;
          Event2Post.EventParam = BACKUP_RPM;
          PostDCMotorService(Event2Post);
          
          ES_Timer_InitTimer(NV_BACKUP_TIMER, BACKUP_TIME_MS);
        break;
        
        // Temporary timers to do "position control"
        case ES_TIMEOUT:
          
          if(ThisEvent.EventParam == NV_BACKUP_TIMER) {
            printf("Backing up timeout \n\r");
            
            // Rotate robot X degrees
            Event2Post.EventType = DR_ROTATE_CW;
            Event2Post.EventParam = ROOMBA_ROTATE_RPM;
            PostDCMotorService(Event2Post);
            
            ES_Timer_InitTimer(NV_ROTATE_TIMER, ROOMBA_TIME_MS);
          }
          
          else if( ThisEvent.EventParam == NV_ROTATE_TIMER ) {
             printf("Rotating timeout \n\r");
            
            // Send robot backward X RPM
            Event2Post.EventType = DR_DRIVE_ALL_REV;
            Event2Post.EventParam = BALL_FINDING_RPM;
            PostDCMotorService(Event2Post);
          }
        break;
          
        case NV_DEPOSIT_BALLS:
          // Commence ball deposit algorithm
          NextState = NavLineFinding;
          
          // Send robot forward X RPM
          Event2Post.EventType = DR_DRIVE_ALL;
          Event2Post.EventParam = LINE_FINDING_RPM;
          PostDCMotorService(Event2Post);
        break;
        
        default:
        break;
      }
    break;
      
    case NavLineFinding:
      switch(ThisEvent.EventType) {
        
        // Roomba algorithm: if the robot runs into a wall, back up and rotate
        
        case ES_DBLIMIT_FR_DN:
          // Reverse robot X inches
          Event2Post.EventType = DR_DRIVE_ALL_REV;
          Event2Post.EventParam = BACKUP_RPM;
          PostDCMotorService(Event2Post);
        
          ES_Timer_InitTimer(NV_BACKUP_TIMER, BACKUP_TIME_MS);
          printf("\n\r Wall pressed backing up ");
        break;

        case ES_DBLIMIT_FL_DN:
          // Reverse robot X inches
          Event2Post.EventType = DR_DRIVE_ALL_REV;
          Event2Post.EventParam = BACKUP_RPM;
          PostDCMotorService(Event2Post);
        
          ES_Timer_InitTimer(NV_BACKUP_TIMER, BACKUP_TIME_MS);
          printf("\n\r Wall pressed backing up ");
        break;
        
        // Temporary timers to do "position control"
        case ES_TIMEOUT:
          if(ThisEvent.EventParam == NV_BACKUP_TIMER) {
            // Rotate robot X degrees
            Event2Post.EventType = DR_ROTATE_CW;
            Event2Post.EventParam = ROOMBA_ROTATE_RPM;
            PostDCMotorService(Event2Post);
            
            puts("\n\rBack up completed.  Rotating.\n\r");
            
            ES_Timer_InitTimer(NV_ROTATE_TIMER, ROOMBA_TIME_MS);
          }
          
          else if( ThisEvent.EventParam == NV_ROTATE_TIMER ) {
            // Send robot forward X RPM
            Event2Post.EventType = DR_DRIVE_ALL;
            Event2Post.EventParam = LINE_FINDING_RPM;
            PostDCMotorService(Event2Post);
            
            puts("\n\rRotation completed.  Moving forward.\n\r");
          }
        break;
          
        // Line detected handling.  This is the goal of this state!
        // Using inductive sensors rather than tape to avoid false positives, and to
        // more accurately position the robot's center of mass right over the line
          
        case IND_LINE_DETECTED:
          
          printf("Line detected \n\r");
          
          // Rotate robot slowly
          Event2Post.EventType = DR_ROTATE_CW;
          Event2Post.EventParam = RECYCLE_FINDING_ROTATE_RPM;
          PostDCMotorService(Event2Post);
        
          // Post inductive line found event to Project Service
          Event2Post.EventType = NV_LINE_FOUND;
          PostNavService(Event2Post);
        
          // Roughly orient the robot towards the recycling center.
          // Doesn't have to be too accurate!  Just need general direction to be right.
          NextState = NavRecyclingFinding;
        break;
        
        default:
        break;
      }
    break;
    
    case NavRecyclingFinding:
      // Waits for an event from the IR Receiver ISR.  Needs to check with COMPASS to
      // make sure it's the right recycling beacon!
      if( ThisEvent.EventType == R_BEACON_DETECTED ) {
        printf("Posted to recycling finding nav service\n\rCDC");
        if (ThisEvent.EventParam == LastCenterIndex){
          IRCounter += 1; 
          LastCenterIndex = ThisEvent.EventParam;
          printf("Index: %d 2 = West, 3 = East \n\r", ThisEvent.EventParam);
          //add to IR counter if same center has been detected 
        }
        else{
          //clear the IRCounter if it is false center detection
          IRCounter = 0;
          LastCenterIndex = ThisEvent.EventParam;
        }
        
        if (IRCounter > IR_THRESHOLD){
          IRCounter = 0;
          printf("Beacon found.  Going forward. \n\r");
        
          // Drive motors forward
          Event2Post.EventType = DR_DRIVE_ALL;
          Event2Post.EventParam = LINE_FOLLOWING_RPM;
          PostDCMotorService(Event2Post);
        
          // Post recycling beacon found event to Project Service
          Event2Post.EventType = NV_RECYCLING_FOUND;
          PostNavService(Event2Post);
          
          EnableLineFollowing();
          DisableIntake();
          NextState = NavLineFollowing;
        }
      }
    break;
    
    case NavLineFollowing:
      // Simplified version of line following that doesn't rely on analog math
      // Could use refinement!
    
      // It goes forward until the front bumper is depressed.  If inductive sensors post
      // an event, it adjusts the RPMs to re-locate the line
    
      switch(ThisEvent.EventType) {
        
        // If the robot wanders off the line, attempt line finding again.
        case IND_LINE_OFF:
          Event2Post.EventType = DR_DRIVE_ALL;
          Event2Post.EventParam = LINE_FINDING_RPM;
          PostDCMotorService(Event2Post);
        
          NextState = NavLineFinding;
        break;
        
        case ES_DBLIMIT_FL_DN:
          
          // If no prior detections had occurred, disable line following, spin other wheel,
          // and wait for right bumper detection
          if(MisalignedCondVar == 0) {
            
            MisalignedCondVar++;
            
            DisableLineFollowing();       
            
            Event2Post.EventType = DR_STOP_LEFT;
            PostDCMotorService(Event2Post);  
            
          // If prior detection had occurred, back up and prepare for shooting
          } 
          
          else {
           MisalignedCondVar--;
            
            // Check if wall is hit by looking at wheel encoder values
            
            for( uint8_t i = 0; i < 2; i++ ) {
              CurrEncoderValue[i] = QueryEncoderCount(i);
              if(CurrEncoderValue[i] > (PrevEncoderValue[i] + WALL_THRESHOLD) ||
                 CurrEncoderValue[i] < (PrevEncoderValue[i] - WALL_THRESHOLD)) {
                PrevEncoderValue[i] = CurrEncoderValue[i];
                AlignedCount = 0;
              }
                 
              else {
                AlignedCount++;
              }
            }
            
            // If both wheels are aligned, move on to shot prepping
            
            if(AlignedCount == 2) {
              AlignedCount = 0;
              NextState = NavShotPrepping;
            }
            
            // Otherwise, back up and try again
            else {
              AlignedCount = 0;
              Event2Post.EventType = DR_DRIVE_ALL_REV;
              Event2Post.EventParam = BACKUP_RPM;
              PostDCMotorService(Event2Post); 
            
              ES_Timer_InitTimer(NV_BACKUP_TIMER, BACKUP_TIME_MS);
            }
          }

        case ES_DBLIMIT_FR_DN:
          if(MisalignedCondVar == 0) {
            
            MisalignedCondVar++;
            
            DisableLineFollowing();       
            
            Event2Post.EventType = DR_STOP_RIGHT;
            PostDCMotorService(Event2Post);  
            
          } 
          
          else {
           MisalignedCondVar--;
            
            // Check if wall is hit by looking at wheel encoder values
            
            for( uint8_t i = 0; i < 2; i++ ) {
              CurrEncoderValue[i] = QueryEncoderCount(i);
              if(CurrEncoderValue[i] > (PrevEncoderValue[i] + WALL_THRESHOLD) ||
                 CurrEncoderValue[i] < (PrevEncoderValue[i] - WALL_THRESHOLD)) {
                PrevEncoderValue[i] = CurrEncoderValue[i];
                AlignedCount = 0;
              }
                 
              else {
                AlignedCount++;
              }
            }
            
            // If both wheels are aligned, move on to shot prepping
            
            if(AlignedCount == 2) {
              AlignedCount = 0;
              NextState = NavShotPrepping;
            }
            
            // Otherwise, back up and try again
            else {
              AlignedCount = 0;
              Event2Post.EventType = DR_DRIVE_ALL_REV;
              Event2Post.EventParam = BACKUP_RPM;
              PostDCMotorService(Event2Post); 
            
              ES_Timer_InitTimer(NV_BACKUP_TIMER, BACKUP_TIME_MS);
            }
          }
          
        // If bump occurred but was not wall, reenable line following
          
        case ES_TIMEOUT:
          if(ThisEvent.EventType == NV_BACKUP_TIMER) {
            EnableLineFollowing();
          }
        break;
          
        default:
        break;
      }
    break;
      
    case NavShotPrepping:
      switch(ThisEvent.EventType) {
        
        // After backing up, start looking for recyling beacon again
        case ES_TIMEOUT:
          if(ThisEvent.EventParam == NV_BACKUP_TIMER) {
            Event2Post.EventType = DR_ROTATE_CW;
            PostDCMotorService(Event2Post);
          }
          
          // Shooting done, await further commands
          else if(ThisEvent.EventParam == NV_SHOOT_TIMER) {
            Event2Post.EventType = DR_STOP_ALL;
            PostDCMotorService(Event2Post);
            
            NextState = NavBallFinding;
            EnableIntake();
          }
        break;
          
        // If the recycling beacon is found
        case R_BEACON_DETECTED:
          // Stop timer
          ES_Timer_StopTimer(NV_BACKUP_TIMER);
        
          Event2Post.EventType = NV_RECYCLING_REACHED;
          PostProjectService(Event2Post);
          
          // Move forward to give the balls some forward velocity
          Event2Post.EventType = DR_DRIVE_ALL;
          Event2Post.EventParam = BACKUP_RPM;
          PostDCMotorService(Event2Post);
        
          ES_Timer_InitTimer(NV_SHOOT_TIMER, BACKUP_TIME_MS - 50);
        break;
      
    default:
    break;                 
  }
}
    
  CurrentState = NextState;
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

void EnableIntake(void) {
  SetDutyCycle(-40, INTAKE_MOTOR);
}

void DisableIntake(void) {
  SetDutyCycle(0, INTAKE_MOTOR);
}

void EnableShooter(void) {
  SetDutyCycle(30, SHOOTER_MOTOR);
}

void DisableShooter(void) {
  SetDutyCycle(0, SHOOTER_MOTOR);
}

void DisableLineFollowing(void) {
  // Disable local timer interrupt 3A
  HWREG(WTIMER3_BASE + TIMER_O_IMR) &= ~TIMER_IMR_TATOIM;
}

void EnableLineFollowing(void) {
  // Enable local timer interrupt 3A
  HWREG(WTIMER3_BASE + TIMER_O_IMR) |= TIMER_IMR_TATOIM;
}

int16_t QueryLineFollowingRPM(void) {
  return(LINE_FOLLOWING_RPM);
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

