/****************************************************************************
 Module
   ProjectService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Events.h"

/* include header files for hardware access
*/
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"

#include "ProjectService.h"
#include "MotorLibrary.h"
#include "COMPASSLibrary.h"
#include "IRDetector.h"
#include "HWInitLibrary.h"
#include "InductiveLibrary.h"
#include "TeamStatusLibrary.h"

#include "NavService.h"
#include "BallService.h"
#include "IRPWMEmitter.h"

/*----------------------------- Module Defines ----------------------------*/
#define GAME_TIME 100

#define RED_COLOR 0
#define ORANGE_COLOR 1
#define YELLOW_COLOR 2
#define GREEN_COLOR 3
#define BLUE_COLOR 4
#define PINK_COLOR 5

#define WEST_RECYCLE 0
#define EAST_RECYCLE 1

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

bool QueryRecyclingStation(void);
uint16_t QueryTeamIRPeriod(void);
uint8_t QueryTeamColor(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static ProjectServiceState_t CurrentState;

static uint8_t GameTime = 0;    // Measured in seconds
static uint8_t TenthSecond = 0; // Measured in tenths of a second

static volatile uint16_t TeamIRPeriod;
static volatile bool TeamRecyclingStation;
static volatile uint8_t TeamColor;

const static uint16_t IR_EMITTER_FREQ[16] = \
  {1000, 947, 893, 840, 787, 733, 680, 627, 573, 520, 467, 413, 360, 307, 253, 200};

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitProjectService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitProjectService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  
  printf("In InitProjectService\n\r");
  // Initialize motor hardware
/*
  InitAllTivaPins();
  puts("\n\r Tiva Pins initialized.\n\r");
  InitAllPWM();
  puts("\n\r PWM initialized.\n\r");
  InitAllInterruptTimers();
  puts("\n\r Interrupts initialized.\n\r");
   */
  
  /********** GAME START LOGIC **********/
  InitAllTivaPins();
  InitSSI();
  
  //uint8_t team = GetSelectState();
  //while (RegisterTeam(team) != 0x1){
   //delay(); 
  //}

  //while(QueryStatus() != 0x1) {delay();}
  //ShowGameOn();     
 /********** GAME START LOGIC **********/

  puts("\n\r Tiva Pins initialized.\n\r");
  InitAllPWM();
  puts("\n\r PWM initialized.\n\r");
  InitAllInterruptTimers();
  puts("\n\r Interrupts initialized.\n\r");
  
  printf("COMPASS init completed\n\r");
  
  InitInductiveSensor();
    
  /*
  printf("COMPASS init completed\n\r");
  
  InitInductiveSensor(); */
  
  // Initialize IR Input Capture
  //InitIRInputCapture();

  // post the initial transition event
  CurrentState = ProjectServiceInitPState;
  
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostProjectService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostProjectService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunProjectService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunProjectService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ES_Event_t Event2Post;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  ProjectServiceState_t NextState = CurrentState;
  static volatile uint8_t CurrentCommand;
  
  uint8_t StatusByte;
  uint8_t TeamIDByte;
  
  uint8_t GameState;
  uint8_t WestColor;
  uint8_t EastColor;

  //static bool enable_flag = true;
  
  /********************************************
   in here you write your service code
   *******************************************/
  //printf("Running project service\n\r");
  
 
  switch (CurrentState) {
    
    case ProjectServiceInitPState:
      if( ThisEvent.EventType == ES_INIT ) {
        Event2Post.EventType = NV_FIND_BALLS;
        //PostNavService(Event2Post); // Comment out if testing
        NextState = GameRunning;
      }
    break;
    
    
    case GameRunning:
      switch(ThisEvent.EventType) {
        case ES_TIMEOUT:
          if(ThisEvent.EventParam == GAME_TIMER) {
            TenthSecond++;
            
            // If 10 iterations have passed
            if(TenthSecond == 10) {
              GameTime++; // Measured in seconds
            }
            
            // Query team information from COMPASS
            TeamIDByte = QueryTeamInfo();
            
            if( (TeamIDByte & 0x0E) != TeamColor ) {
              TeamColor = TeamIDByte & 0x0E;
              Event2Post.EventParam = TeamColor;
              // Post TEAM_COLOR_CHANGE event to BallService
            }
            
            if( IR_EMITTER_FREQ[TeamIDByte & 0xF0] != TeamIRPeriod ) {
              TeamIRPeriod = IR_EMITTER_FREQ[TeamIDByte & 0xF0];
              Event2Post.EventParam = TeamIRPeriod;
              Event2Post.EventType = GAME_IR_PERIOD_CHANGE;
              PostPWMService(Event2Post);
            }
            
            // Query game status information
            StatusByte = QueryUnmaskedStatus();
            
            // Check if game has ended
            GameState = StatusByte & 0x03;
            if( GameState == 0x02 ) {
              NextState = GameEnded;
            }
            
            // Check for which color is accepted at the west station
            WestColor = (StatusByte & 0x1C) >> 2;
            if( WestColor == TeamColor ) {
              TeamRecyclingStation = WEST_RECYCLE;
            }
            
            // Check for which color is accepted at the east station
            EastColor = (StatusByte & 0xE0) >> 5;
            if( EastColor == TeamColor ) {
              TeamRecyclingStation = EAST_RECYCLE;
            }
          }
        break;
          
          // If our compartment is full, tell NavService to find line and deposit balls.
        case BALL_FULL:
          Event2Post.EventType = NV_DEPOSIT_BALLS;
          PostNavService(Event2Post);
        break;
        
        // If we're at the recycling station, drop off all the balls.
        case NV_RECYCLING_REACHED:
          Event2Post.EventType = BALL_DISPENSE;
          PostBallService(Event2Post);
        break;
        
        default:
        break;
      }

    case GameEnded:
          ShowGameOff();
          Event2Post.EventType = GAME_ENDED;
          ES_PostAll(Event2Post);
    
    // DCMotorService kills all interrupts upon receiving this event, effectively
    // preventing the motors from being driven.
    break;
      }
    CurrentState = NextState;
    return ReturnEvent;
  }
 
/*---------------------------- Module Functions ---------------------------*/
/* Definitions for public functions for this service/
*/
  
// Returns which recycling station is currently accepting our balls.
// 0 = West, 1 = East
bool QueryRecyclingStation(void) {
  return TeamRecyclingStation;
}

uint16_t QueryTeamIRPeriod(void) {
  return TeamIRPeriod;
}

uint8_t QueryTeamColor(void) {
  return TeamColor;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

