#include "ES_Configure.h"
#include "ES_Framework.h"

#include "inc/hw_gpio.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_pwm.h"
#include "inc/hw_sysctl.h"
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_ssi.h"

#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/ssi.h"
#include "driverlib/pwm.h"
#include "driverlib/sysctl.h"

#include "MotorLibrary.h"
#include "COMPASSLibrary.h"
#include "ES_ServiceHeaders.h"

#define BITS_PER_NIBBLE 4
#define SOUTH 0x1
#define NORTH 0x0

/* Initialize the SSI0 module */ 

bool InitSSI (void) {
	//printf("Initializing SSI module \n\r"); 
  
  // Enable the clock to the GPIO Port
	HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0;
  
	// Enable the clock to the SSI module
	HWREG(SYSCTL_RCGCSSI) |= SYSCTL_RCGCSSI_R0;

  // Wait for GPIO port to be ready
	while((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R0) != SYSCTL_PRGPIO_R0)
	{
		;
	}
  
	// Program the GPIO to use the alternate functions
	// on the SSI pins
	// Using alternate pin functions on PA 2, 3, 4, and 5
	// Page 650 of datasheet:
	// PA2 is CLK of SSI0
	// PA3 is SS of SSI0
	// PA4 is MISO of SSI0
	// PA5 is MOSI of SSI0
	HWREG(GPIO_PORTA_BASE + GPIO_O_AMSEL) = 0x0;
	HWREG(GPIO_PORTA_BASE + GPIO_O_AFSEL) |= (BIT2HI | BIT4HI | BIT5HI); // BIT3HI causes distortion
	
  // Set Mux position in GPIOPCTRL to select SSI use of pins
  //0xFF0F00FF Bit mask is crucial
  HWREG(GPIO_PORTA_BASE  + GPIO_O_PCTL) = (HWREG(GPIO_PORTA_BASE + \
      GPIO_O_PCTL) & 0xFF00F0FF) + \
      (2 << (2 * BITS_PER_NIBBLE)) +
      //(2 << (3 * BITS_PER_NIBBLE)) +
      (2 << (4 * BITS_PER_NIBBLE)) + 
      (2 << (5 * BITS_PER_NIBBLE));
  
	// Program the port lines for digital I/O 
	HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= (BIT3HI | BIT4HI);

	// Program the required data directions on the port lines
	HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);
  
  // Raise SS
  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT3HI;
  
	// If using SPI mode 3, program the pull-up on the clock line
	HWREG(GPIO_PORTA_BASE + GPIO_O_PUR) |= (BIT2HI);

	// Wait for SSI0 to be ready
	while((HWREG(SYSCTL_PRSSI) & SYSCTL_PRSSI_R0) != SYSCTL_PRSSI_R0)
	{
		;
	}
	
	// Make sure that the SSI is disabled before programming mode bits
	HWREG(SSI0_BASE + SSI_O_CR1) &= ~SSI_CR1_SSE;

	// Select master mode (MS) & TXRIS indicating EOT
	HWREG(SSI0_BASE + SSI_O_CR1) &= ~SSI_CR1_MS;
	HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_EOT; // HAVE NOT TESTED YET
	
  // Configure the clock prescaler
  // System clock is 40 MHz
	// NEED 15kHz
  // 40 MHz / 0.015 MHz = 2666.66666 ~= 2666 (even number)
	// CPSDVSR * (1 + SCR) = 2666
  // CPSDVSR = 0d86 = 0x56
  // SCR = 0d30 = 0x1E
	HWREG(SSI0_BASE + SSI_O_CPSR) &= ~SSI_CPSR_CPSDVSR_M;
	HWREG(SSI0_BASE + SSI_O_CPSR) |= 0x56;
  
  // Configure clock rate, clock phase, & clock polarity
	HWREG(SSI0_BASE + SSI_O_CR0) &= ~(SSI_CR0_SCR_M);
  HWREG(SSI0_BASE + SSI_O_CR0) |= (0x1E) << 8;
	HWREG(SSI0_BASE + SSI_O_CR0) |= (SSI_CR0_SPH | SSI_CR0_SPO);

  // Configure frame mode
	HWREG(SSI0_BASE + SSI_O_CR0) &= ~SSI_CR0_FRF_M;
	HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_FRF_MOTO;

  // Configure data size
	HWREG(SSI0_BASE + SSI_O_CR0) &= ~SSI_CR0_DSS_M;
	HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_DSS_8;

	// Locally enable interrupts (if enabled, hard to test as SCK won't appear)
	HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM; 

	HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_SSE;
  
  // Enable NVIC interrupt for SSI when starting transmission
	// NVIC Enable Register 0 handles IRQs 0-31 
	// (corresponding to interrupts 16-47)
	// SSI0 is IRQ number 7, so BIT7HI 
	HWREG(NVIC_EN0) |= BIT7HI; // NOT TESTED YET
	
	//printf("SSI Init Complete \n\r");
  
  return true;
}

uint8_t CheckTXFifoEmpty(void) {
	uint8_t bool_out = ((HWREG(SSI0_BASE + SSI_O_SR) & SSI_SR_TFE) > 0) ? 1 : 0;
	return bool_out; 
}


uint8_t CheckTXFifoFull(void){
	uint8_t bool_out = ((HWREG(SSI0_BASE + SSI_O_SR) & SSI_SR_TNF) == 0) ? 1 : 0;
	return bool_out; 
}


uint8_t CheckRXFifoFull(void){
	uint8_t bool_out = ((HWREG(SSI0_BASE + SSI_O_SR) & SSI_SR_RNE) > 0) ? 1 : 0;
	return bool_out; 
}

uint8_t CheckRXFifoEmpty(void){
	uint8_t bool_out = ((HWREG(SSI0_BASE + SSI_O_SR) & SSI_SR_RFF) == 0) ? 1 : 0;
	return bool_out; 	
}

void TransmitSPI(uint8_t tx_data) {
  
  // Lower SS
  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT3LO;

	while(!CheckTXFifoEmpty()) {
		;
	}
	////printf("Ready\n\r");
	HWREG(SSI0_BASE + SSI_O_DR) = tx_data;
  //delay();
	////printf("Sending \n\r"); // HELPS DELAY BETWEEN CG CALLS
  
  // Raise SS

}

uint8_t ReceiveSPI(void) {
	// Wait for SPI TX Fifo to be empty
  // For loopback test
  /*
	while (!CheckTXFifoEmpty()) {
		;
	} */

	// Incoming byte
	uint8_t rx_data = 0xAA;

	// Wait for SPI RX Fifo to be filled with 8 bits
	while (!CheckRXFifoFull()) {
		;
	}

	// Read from SSI Data register
	rx_data = HWREG(SSI0_BASE + SSI_O_DR);
	
	return rx_data;
}

void SPITxHandler(void) {
  
  // Clear SSI interrupt
  HWREG(SSI0_BASE + SSI_O_ICR) |= SSI_ICR_RTIC;	
}

/**********************************************/




void delay(void){
	for(uint32_t i = 0; i < 10000; i++) {
		;
	}
}

void message_delay(void){
	for(uint32_t i = 0; i < 500000; i++) {
		;
	}
}

/* North team = 0x0
   South team = 0x1 */

uint8_t RegisterTeam(uint8_t team_polarity) {
  // Variable Initialization
  uint8_t reg_byte = 0x0;
  volatile uint8_t first_rt_rx_byte = 0xAA; // should become 0x00 
  volatile uint8_t second_rt_rx_byte = 0xAA; // should become 0xFF
  volatile uint8_t third_rt_rx_byte = 0xAA; // end should be 0x01 or 0x11 
  
  // REG byte formation
  if(team_polarity == NORTH) {
    reg_byte = 0x10;
    //printf("Registered North\n\r");
  } else  {
    reg_byte = 0x01;
    //printf("Registered South\n\r");
  }
  
  ////printf("REG Byte: %x \n\r", reg_byte);
  // Transmit sequence 
  TransmitSPI(reg_byte);
  delay();
  ////printf("Transmitted %x\n\r", reg_byte);
  //first_rt_rx_byte = curr_rx_byte;
    
  TransmitSPI(0x00);
  delay();
  //second_rt_rx_byte = curr_rx_byte;
  
  TransmitSPI(0x00);
  delay();
  //third_rt_rx_byte = curr_rx_byte; 

  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT3HI;
  
  first_rt_rx_byte = ReceiveSPI();
  delay();
  second_rt_rx_byte = ReceiveSPI();
  delay();
  third_rt_rx_byte = ReceiveSPI();
  delay();
  
  /*
  //printf("First RX: %x\n\r", first_rt_rx_byte);
  //printf("Second RX: %x\n\r", second_rt_rx_byte);
  //printf("Third RX: %x\n\r", third_rt_rx_byte); */
  
  return third_rt_rx_byte & 0x1;

}  

uint8_t QueryStatus(void) {
  volatile uint8_t first_rt_rx_byte = 0xAA; // should become 0x00 
  volatile uint8_t second_rt_rx_byte = 0xAA; // should become 0xFF
  volatile uint8_t third_rt_rx_byte = 0xAA; // end should be 0x01 or 0x11

  ////printf("In query status\n\r");
  
  TransmitSPI(0x78);
  delay();
  ////printf("Transmitted %x\n\r", reg_byte);
  //first_rt_rx_byte = curr_rx_byte;
    
  TransmitSPI(0x00);
  delay();
  //second_rt_rx_byte = curr_rx_byte;
  
  TransmitSPI(0x00);
  delay();
  //third_rt_rx_byte = curr_rx_byte; 
  
  ////printf("Still alive\n\r");


  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT3HI;
  
  
  first_rt_rx_byte = ReceiveSPI();
  delay();
  second_rt_rx_byte = ReceiveSPI();
  delay();
  third_rt_rx_byte = ReceiveSPI();
  delay();
  
  ////printf("First RX: %x\n\r", first_rt_rx_byte);
  ////printf("Second RX: %x\n\r", second_rt_rx_byte);
  ////printf("Third RX: %x\n\r", third_rt_rx_byte);
  
  return (third_rt_rx_byte & 0x3); 
}

uint8_t QueryUnmaskedStatus(void) {
  volatile uint8_t first_rt_rx_byte = 0xAA; // should become 0x00 
  volatile uint8_t second_rt_rx_byte = 0xAA; // should become 0xFF
  volatile uint8_t third_rt_rx_byte = 0xAA; // end should be 0x01 or 0x11

  ////printf("In query status\n\r");
  
  TransmitSPI(0x78);
  delay();
  ////printf("Transmitted %x\n\r", reg_byte);
  //first_rt_rx_byte = curr_rx_byte;
    
  TransmitSPI(0x00);
  delay();
  //second_rt_rx_byte = curr_rx_byte;
  
  TransmitSPI(0x00);
  delay();
  //third_rt_rx_byte = curr_rx_byte; 
  
  ////printf("Still alive\n\r");


  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT3HI;
  
  
  first_rt_rx_byte = ReceiveSPI();
  delay();
  second_rt_rx_byte = ReceiveSPI();
  delay();
  third_rt_rx_byte = ReceiveSPI();
  delay();
  
  ////printf("First RX: %x\n\r", first_rt_rx_byte);
  ////printf("Second RX: %x\n\r", second_rt_rx_byte);
  ////printf("Third RX: %x\n\r", third_rt_rx_byte);
  
  return (third_rt_rx_byte); 
}

uint8_t QueryTeamInfo(void) {
  volatile uint8_t first_rt_rx_byte = 0xAA; // should become 0x00 
  volatile uint8_t second_rt_rx_byte = 0xAA; // should become 0xFF
  volatile uint8_t third_rt_rx_byte = 0xAA; // end should be 0x01 or 0x11

  TransmitSPI(0xD2);
  delay();
  ////printf("Transmitted %x\n\r", reg_byte);
  //first_rt_rx_byte = curr_rx_byte;
    
  TransmitSPI(0x00);
  delay();
  //second_rt_rx_byte = curr_rx_byte;
  
  TransmitSPI(0x00);
  delay();
  //third_rt_rx_byte = curr_rx_byte; 

  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT3HI;
  
  first_rt_rx_byte = ReceiveSPI();
  delay();
  second_rt_rx_byte = ReceiveSPI();
  delay();
  third_rt_rx_byte = ReceiveSPI();
  delay();
  
  //printf("First RX: %x\n\r", first_rt_rx_byte);
  //printf("Second RX: %x\n\r", second_rt_rx_byte);
  //printf("Third RX: %x\n\r", third_rt_rx_byte);
  return third_rt_rx_byte;
}

void QueryScore(void) {
  volatile uint8_t first_rt_rx_byte = 0xAA; // should become 0x00 
  volatile uint8_t second_rt_rx_byte = 0xAA; // should become 0xFF
  volatile uint8_t third_rt_rx_byte = 0xAA; // end should be 0x01 or 0x11
  volatile uint8_t fourth_rt_rx_byte = 0xAA; // should become 0xFF
  volatile uint8_t fifth_rt_rx_byte = 0xAA; // end should be 0x01 or 0x11
  volatile uint8_t sixth_rt_rx_byte = 0xAA; // end should be 0x01 or 0x11
  TransmitSPI(0xB4);
  delay();
  ////printf("Transmitted %x\n\r", reg_byte);
  //first_rt_rx_byte = curr_rx_byte;
    
  TransmitSPI(0x00);
  delay();
  //second_rt_rx_byte = curr_rx_byte;
  
  TransmitSPI(0x00);
  delay();
  //third_rt_rx_byte = curr_rx_byte; 
  
  TransmitSPI(0x00);
  delay();
  //second_rt_rx_byte = curr_rx_byte;
  
  TransmitSPI(0x00);
  delay();
  
  TransmitSPI(0x00);
  delay();
  //second_rt_rx_byte = curr_rx_byte;
  

  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT3HI;
  
  first_rt_rx_byte = ReceiveSPI();
  delay();
  second_rt_rx_byte = ReceiveSPI();
  delay();
  third_rt_rx_byte = ReceiveSPI();
  delay();
  fourth_rt_rx_byte = ReceiveSPI();
  delay();
  fifth_rt_rx_byte = ReceiveSPI();
  delay();
  sixth_rt_rx_byte = ReceiveSPI();
  delay();
  
  //printf("First RX: %x\n\r", first_rt_rx_byte);
  //printf("Second RX: %x\n\r", second_rt_rx_byte);
  //printf("Third RX: %x\n\r", third_rt_rx_byte);
  //printf("Fourth RX: %x\n\r", fourth_rt_rx_byte);
  //printf("Fifth RX: %x\n\r", fifth_rt_rx_byte);
  //printf("Sixth RX: %x\n\r", sixth_rt_rx_byte);
}

void QueryRecyclingValue(void) {
  volatile uint8_t first_rt_rx_byte = 0xAA; // should become 0x00 
  volatile uint8_t second_rt_rx_byte = 0xAA; // should become 0xFF
  volatile uint8_t third_rt_rx_byte = 0xAA; // end should be 0x01 or 0x11

  TransmitSPI(0x69);
  delay();
  ////printf("Transmitted %x\n\r", reg_byte);
  //first_rt_rx_byte = curr_rx_byte;
    
  TransmitSPI(0x00);
  delay();
  //second_rt_rx_byte = curr_rx_byte;
  
  TransmitSPI(0x00);
  delay();
  //third_rt_rx_byte = curr_rx_byte; 

  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT3HI;
  
  first_rt_rx_byte = ReceiveSPI();
  delay();
  second_rt_rx_byte = ReceiveSPI();
  delay();
  third_rt_rx_byte = ReceiveSPI();
  delay();
  
  //printf("First RX: %x\n\r", first_rt_rx_byte);
  //printf("Second RX: %x\n\r", second_rt_rx_byte);
  //printf("Third RX: %x\n\r", third_rt_rx_byte);
}

void ExecuteCommandCycle (void){
  QueryStatus();
	////printf("Delay\n\r");
	//InterpretCommand(curr_command);
	// TODO: Put millisecond timeout to prevent too fast
	// For now, use //printf to delay
	// //printf("One Command Cycle Executed\n\r");

}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
