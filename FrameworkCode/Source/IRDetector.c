/****************************************************************************
 Module
   IRDetector.c

 Revision
   1.0.1

 Description
   This service detects IR and distinguishes the recycling center/landfill

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Events.h"

/* include header files for hardware access
*/
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"

#include "ProjectService.h"
#include "MotorLibrary.h"
#include "COMPASSLibrary.h"
#include "IRDetector.h"
#include "NavService.h"

/*----------------------------- Module Defines ----------------------------*/

//Calculating the high time ticks 
//(Period/2) * ticks per second 
#define W_RECYCLING_TICKS 600
#define E_RECYCLING_TICKS 500

#define SOUTH 0x1
#define NORTH 0x0

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
//static uint8_t MyPriority;
//static Lab8State_t CurrentState;

volatile uint32_t this_capture;
volatile uint32_t period;
volatile uint32_t last_capture;
volatile uint32_t last_period;

/****************************************************************************
 Function
    InitInputCapturePeriod

 Parameters
   None

 Returns
   None

 Description
   Initialize WTIMER5 to do input capture on IR period
 Notes
    Using WT5CCP0 = PD6 IO 

 Author
   Brian Jun, 01/22/19, 21:31
****************************************************************************/
bool InitIRInputCapture(void) {

  // Requires a change in the startup_rvmdk.s file to the WTIMER0
  // interrupt vector

  // Enable clock to timer (WTIMER5)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R5;
  
  // Enable the clock to Port D 
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R3;
  
  // Make sure that Timer A is disbaled before configuring 
  HWREG(WTIMER5_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
  
  // Set timer to 32-bit wide mode
  HWREG(WTIMER5_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
  
  // Register the FULL 32-bit value)
  HWREG(WTIMER5_BASE + TIMER_O_TAILR) = 0xFFFFFFFF;
  
  // Set up timer A in capture mode
  HWREG(WTIMER5_BASE + TIMER_O_TAMR) = (HWREG(WTIMER5_BASE + TIMER_O_TAMR) &\
                                        ~TIMER_TAMR_TAAMS) | (TIMER_TAMR_TACDIR | \
                                        TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  
  // Using one timer, capture both edges
  HWREG(WTIMER5_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
  
  // Trigger on both rising and falling edge
  HWREG(WTIMER5_BASE + TIMER_O_CTL) |= (BIT3HI | BIT2HI);

  // Setup the port to do the capture by setting alternate function
  HWREG(GPIO_PORTD_BASE + GPIO_O_AFSEL) |= BIT6HI;

  // Map alternate function
  HWREG(GPIO_PORTD_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTD_BASE+GPIO_O_PCTL) & 0xF0FFFFFF) + \
                                         (7 << 16);

  // Enable pin 6 on Port D for digital I/O
  HWREG(GPIO_PORTD_BASE + GPIO_O_DEN) |= BIT6HI; 

  // Designate pin 6 on Port D as an input
  HWREG(GPIO_PORTD_BASE + GPIO_O_DIR) &= BIT6LO;
  
  //Debugging
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= BIT0HI; 
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= BIT0HI;
  

  // Enable local capture interrupt 
  HWREG(WTIMER5_BASE + TIMER_O_IMR) |= TIMER_IMR_CAEIM;

  // Enable timer A in WTIMER5 interrupt in NVIC
  // Interrupt 104
  HWREG(NVIC_EN3) |= BIT8HI;

  // Make sure interrupts are enabled globally
  __enable_irq();

  // Enable timer and its stall feature
  HWREG(WTIMER5_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
  
  return true; 
}


/****************************************************************************
 Function
    InputCaptureResponse

 Parameters
   None

 Returns
   None

 Description
   Capture incoming edges and save period
 Notes

 Author
   Brian Jun, 01/22/19, 21:33
****************************************************************************/
void IRInputCaptureISR(void) {
    
  // Debug
  HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) ^= BIT1HI;
  
  // Clear the source of the interrupt
  HWREG(WTIMER5_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;
  
  // Calculate period from captured value
  this_capture = HWREG(WTIMER5_BASE + TIMER_O_TAR);
  
  // Clear captured edge
  HWREG(WTIMER5_BASE + TIMER_O_TAR) &= 0x0;

  // Calculate most recent period
  period = (this_capture - last_capture)/40;

  // Update last_capture
  last_capture = this_capture;
  
  
  /*
  //if North Landfill
  if( ( period > ((90 * N_LANDFILL_TICKS)/100) ) && ( (period < ((110 * N_LANDFILL_TICKS)/100) )) ) {
    ES_Event_t Event2Post;
    Event2Post.EventParam = 0;
    Event2Post.EventType = R_BEACON_DETECTED;
    PostNavService(Event2Post);
     printf("N");
  }
  //If South Landfill
  else if( ( period > ((90 * S_LANDFILL_TICKS)/100) ) && ( (period < ((110 * S_LANDFILL_TICKS)/100) )) ) {
    ES_Event_t Event2Post;
    Event2Post.EventParam = 1;
    Event2Post.EventType = R_BEACON_DETECTED;
    PostNavService(Event2Post);
    printf("S");
  }
  */
  /*
  //If West Recycling
  if( ( period > ((90 * W_RECYCLING_TICKS)/100) ) && ( (period < ((110 * W_RECYCLING_TICKS)/100) )) ) {
    ES_Event_t Event2Post;
    Event2Post.EventParam = 2;
    Event2Post.EventType = R_BEACON_DETECTED;
    PostNavService(Event2Post);
  }
  //If East Recycling
  else if( ( period > ((90 * E_RECYCLING_TICKS)/100) ) && ( (period < ((110 * E_RECYCLING_TICKS)/100) )) ) {
    ES_Event_t Event2Post;
    Event2Post.EventParam = 3;
    Event2Post.EventType = R_BEACON_DETECTED;
    PostNavService(Event2Post);
  }
  else {
    //last_period = period;
  } */

}

uint32_t GetBeaconPeriod(void) {
    /*
  //if North Landfill
  if( ( period > ((90 * N_LANDFILL_TICKS)/100) ) && ( (period < ((110 * N_LANDFILL_TICKS)/100) )) ) {
    ES_Event_t Event2Post;
    Event2Post.EventParam = 0;
    Event2Post.EventType = R_BEACON_DETECTED;
    PostNavService(Event2Post);
     printf("N");
  }
  //If South Landfill
  else if( ( period > ((90 * S_LANDFILL_TICKS)/100) ) && ( (period < ((110 * S_LANDFILL_TICKS)/100) )) ) {
    ES_Event_t Event2Post;
    Event2Post.EventParam = 1;
    Event2Post.EventType = R_BEACON_DETECTED;
    PostNavService(Event2Post);
    printf("S");
  }
  */
  /*
  //If West Recycling
  if( ( period > ((80 * W_RECYCLING_TICKS)/100) ) && ( (period < ((120 * W_RECYCLING_TICKS)/100) )) ) {
    ES_Event_t Event2Post;
    Event2Post.EventParam = 2;
    Event2Post.EventType = R_BEACON_DETECTED;
    PostNavService(Event2Post);
    printf("WEST \n\r");
  }
  //If East Recycling
  else if( ( period > ((80 * E_RECYCLING_TICKS)/100) ) && ( (period < ((120 * E_RECYCLING_TICKS)/100) )) ) {
    ES_Event_t Event2Post;
    Event2Post.EventParam = 3;
    Event2Post.EventType = R_BEACON_DETECTED;
    PostNavService(Event2Post);
    printf("EAST \n\r");
  }
  else {
    //last_period = period;
  }  */
  //printf("Period: %d \n\r", period);

  return period;
}
