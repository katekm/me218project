/****************************************************************************
 Module
   ProjectService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Events.h"

/* include header files for hardware access
*/
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"

#include "ProjectService.h"
#include "MotorLibrary.h"
#include "COMPASSLibrary.h"
#include "IRDetector.h"
#include "HWInitLibrary.h"
#include "InductiveLibrary.h"
#include "TeamStatusLibrary.h"

/*----------------------------- Module Defines ----------------------------*/
#define DELAY_90DEG 1700
#define DELAY_45DEG 850
#define FULL_SPEED 95
#define HALF_SPEED 75 // duty cycle
#define TICKS_PER_S 40000000
#define IR_PERIOD_TICKS 13800 // Define for infrared high time in clock ticks

#define SOUTH 0x1
#define NORTH 0x0

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static Lab8State_t CurrentState;

static volatile uint32_t this_capture;
static volatile uint32_t period;
static volatile uint32_t last_capture;
static volatile uint32_t last_period;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitProjectService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitProjectService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  
  printf("In InitProjectService\n\r");
  // Initialize motor hardware
/*
<<<<<<< HEAD
  InitAllTivaPins();
  puts("\n\r Tiva Pins initialized.\n\r");
  InitAllPWM();
  puts("\n\r PWM initialized.\n\r");
  InitAllInterruptTimers();
  puts("\n\r Interrupts initialized.\n\r");
   */
=======
>>>>>>> 62839ef... implement smoother line following and COMPASS-based start
  InitAllTivaPins();
  InitSSI();
  
  uint8_t team = GetSelectState();
  
  RegisterTeam(team);
  
  printf("Registered team\n\r");
  printf("NOTHING >_< \n\r");
  
  while(QueryStatus() != 0x1) {;}
        
  puts("\n\r Tiva Pins initialized.\n\r");
  InitAllPWM();
  puts("\n\r PWM initialized.\n\r");
  InitAllInterruptTimers();
  puts("\n\r Interrupts initialized.\n\r");
   */
  InitAllTivaPins();
  InitSSI();
          RegisterTeam(NORTH);
        printf("Registered team\n\r");
        printf("NOTHING >_< \n\r");
        while(QueryStatus() != 0x1) {
        }
        
        puts("\n\r Tiva Pins initialized.\n\r");
        InitAllPWM();
        puts("\n\r PWM initialized.\n\r");
        InitAllInterruptTimers();
        puts("\n\r Interrupts initialized.\n\r");
  
        printf("COMPASS init completed\n\r");
  
<<<<<<< HEAD
  printf("COMPASS init completed\n\r");
  
  InitInductiveSensor();
    
  /*
  printf("COMPASS init completed\n\r");
  
=======
        InitInductiveSensor();
  /*
  printf("COMPASS init completed\n\r");
  
>>>>>>> 62839ef... implement smoother line following and COMPASS-based start
  InitInductiveSensor(); */
  
  // Initialize IR Input Capture
  //InitIRInputCapture();

  // post the initial transition event
  CurrentState = Lab8InitPState;
  
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostProjectService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostProjectService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunProjectService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunProjectService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  //ES_Event_t Event2Post;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  Lab8State_t NextState;
  static volatile uint8_t CurrentCommand;

  //static bool enable_flag = true;
  
  /********************************************
   in here you write your service code
   *******************************************/
  //printf("Running project service\n\r");
  
 
  switch (CurrentState) {
    
    case Lab8InitPState:
      if( ThisEvent.EventType == ES_INIT ) {
        ThisEvent.EventType = RUNNING;
        printf("Running\n\r");
        SetDutyCycle(0, 0);
        SetDutyCycle(0,1);

        printf ("READY!!!! \n\r");
        //ThisEvent.EventParam = 0x09; //start robot moving forward
        //PostProjectService(ThisEvent);
        //NextState = ExecutingCommand;
      }
    break;
    
    
    case ExecutingCommand:
      //puts("\n\r executing command");
      //printf("\n\r %d ", ThisEvent.EventType);
      HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT0HI;


        if (ThisEvent.EventType == RUNNING){
          // Abort current operation and execute new command

          // Rotate motor 1
          SetDutyCycle(-HALF_SPEED, 0);
          // Rotate motor 2
          SetDutyCycle(HALF_SPEED, 1);

          //puts("\n\r running motors \n\r");
        }

        if (ThisEvent.EventType == ES_DBFRONT_BUTTON_DOWN){ //front limit switch has been set
          
          // Stop motor 0
          SetDutyCycle(0, 0);
          // Stop motor 1
          SetDutyCycle(0, 1); 
          
          puts("\n\r Wall detected.  Reversing motors.\n\r");
          

          
           // Rotate motor 1
          SetDutyCycle(HALF_SPEED, 0);
          // Rotate motor 2
          SetDutyCycle(-HALF_SPEED, 1);
           
          NextState = ExecutingCommand;
        }
        
        if (ThisEvent.EventType == ES_DBBACK_BUTTON_DOWN){ //back limit switch has been set

            puts("\n\r Wall detected. Stopping motors.\n\r");
            
            // Stop motor 0
            SetDutyCycle(0, 0);
            // Stop motor 1
            SetDutyCycle(0, 1);
            
            NextState = ExecutingCommand;
        }
         
          
        default:
          // Nothing happens here
          //printf("In default >_<\n\r");
          NextState = ExecutingCommand;

        break;
      }
    CurrentState = NextState;
    return ReturnEvent;
  }
 

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

