/****************************************************************************
 Module
   NewADService.c

 Revision
   1.0.1

 Description
   This is a NewAD file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/22/19 21:2 bjun2     Update for AD measurement of potentiometer
 01/16/12 09:58 jec      began conversion from NewADFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "NewADService.h"
#include "ES_DeferRecall.h"
#include "PWMService.h"

/*----------------------------- Module Defines ----------------------------*/
#define ONE_SEC 1000
#define HALF_SEC (ONE_SEC/2)
#define TWO_SEC (ONE_SEC*2)
#define FIVE_SEC (ONE_SEC*5)
#define NUM_ADC_CHANNELS 1
#define AD_TIMER 0
#define AD_TIMER_PERIOD 200

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

static uint32_t ADCResults[NUM_ADC_CHANNELS];
static uint16_t BaselineADCResult;
static bool ReturnVal = false;
static ES_Event_t ThisEvent;

// State of the motor
static ADState_t CurrentState = PInitState;

// add a deferral queue for up to 3 pending deferrals +1 to allow for overhead
static struct ES_Event DeferralQueue[3+1];

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitNewADService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     Brian Jun, 01/22/19, 22:32
****************************************************************************/
bool InitNewADService(uint8_t Priority)
{
 // Initialize one ADC channel for motor speed control
  ADC_MultiInit(NUM_ADC_CHANNELS);
   
  //Put us into the initial pseudo-state to set up for the initial transition
	CurrentState = PInitState;

  // Set up AD Polling timer to timeout every 20 ms 
  ES_Timer_SetTimer(AD_TIMER, AD_TIMER_PERIOD);

  // Start AD timer
  ES_Timer_StartTimer(AD_TIMER);
  
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService( MyPriority, ThisEvent) == true)
  {  
    return true;
  }else
  {
      return false;
  }
}

/****************************************************************************
 Function
     PostNewADService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     Brian Jun, 01/22/19, 22:35
****************************************************************************/
bool PostNewADService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunNewADService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Run a service that samples the output of the potentiometer
 Notes

 Author
   Brian Jun, 01/22/19, 22:37
****************************************************************************/
ES_Event_t RunNewADService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  // Assume no errors
  ReturnEvent.EventType = ES_NO_EVENT;

  //printf("Running AD service\n\r");

  // Switch the state of the SM based on CurrentState  
  switch (CurrentState){

    // If CurrentState is pseudo-init state
    case PInitState:
            
      // If received event is ES_INIT
      if(ThisEvent.EventType == ES_INIT) {

        // initialize the deferal queue
        ES_InitDeferralQueueWith(DeferralQueue, ARRAY_SIZE(DeferralQueue));

        // move to the Sampling state
        CurrentState = Sampling;
      }
      break;
			
    // If CurrentState is Sampling state
    case Sampling:
			
      // If ADTimer sends timeout event and timer number is ADTimer's
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == AD_TIMER) {

        // Sample potentiometer voltage and put into ADC buffer
        ADC_MultiRead(ADCResults);
        

				// Start AD timer
				ES_Timer_StopTimer(AD_TIMER);
				
				 // Set up AD Polling timer to timeout every 20 ms 
				ES_Timer_SetTimer(AD_TIMER, AD_TIMER_PERIOD);

				// Start AD timer
				ES_Timer_StartTimer(AD_TIMER);

        // Post to PWM service
        PostPWMService(ThisEvent);
        
        // Transition to next state
        CurrentState = Sampling;
      }
      break;
  }
  return ReturnEvent;

}

/****************************************************************************
 Function
    getPotValue

 Parameters
   None

 Returns
   uint32_t: the result from the ADC reading

 Description
   Outputs the reading from the ADC via potentiometer
 Notes

 Author
   Brian Jun, 01/22/19, 22:38
****************************************************************************/
uint32_t getPotValue(void) {
	return ADCResults[0];
}
/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

