/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "HWInitLibrary.h"

/* include header files for hardware access
*/
#include <stdint.h>
#include <stdbool.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"

#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"

/*----------------------------- Module Defines ----------------------------*/

#define BITS_PER_NIBBLE 4
#define TicksPerMS 40000
#define PERIOD_IN_US 600
#define PWM_TICKS_PER_MS 1250
#define PID_PERIOD_MS 2     // Period between PID calculations
#define PWM_FREQ_0_Hz 3500  // For drive motors
#define PWM_FREQ_1_Hz 3500  // For intake/shooter motors
#define PWM_FREQ_3_Hz 50  // For sorting servos


/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
                                                                                      
bool InitAllTivaPins(void);
bool InitAllPWM(void);
bool InitAllInterruptTimers(void);

/***************************************************************************
 private functions
 ***************************************************************************/
 
 bool InitAllTivaPins(void) {
   // Initialize Port A
   // Enable the clock to Port A
   HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0;
   while ((HWREG(SYSCTL_PRGPIO) & BIT0HI) != BIT0HI) {}
   
   // Enable Port A for Digital I/O
   HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= 0xFC; // 0xFC = 11111100
   
   // Set Pin Directions for Port A
   HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= 0xEC; // 0xEC = 11101100;
   HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= BIT4LO;
   
   // Initialize Port B
   // Enable the clock to Port B
   HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
   while ((HWREG(SYSCTL_PRGPIO) & BIT1HI) != BIT1HI) {}
   
   // Enable Port B for Digital I/O
   HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= 0xFF; // 0xFF = 11110011
   
   // Set Pin Directions for Port B
   HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= 0xF0; // 0xF0 = 11110000;
   HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= 0xFC; // 0xFC = 11111100;
   
   // Initialize Port C
   // Enable the clock to Port C
   HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
   while ((HWREG(SYSCTL_PRGPIO) & BIT2HI) != BIT2HI) {}
   
   // Enable Port C for Digital I/O
   HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= 0xF0; // 0xF0 = 11110000
   
   // Set Pin Directions for Port C
   HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) |= 0x30; // 0x30 = 00110000;
   HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) &= 0x3F; // 0x3F = 00111111;
   
   // Initialize Port D
   // Enable the clock to Port D
   HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R3;
   while ((HWREG(SYSCTL_PRGPIO) & BIT3HI) != BIT3HI) {}
   
   // Enable Port D for Digital I/O
   HWREG(GPIO_PORTD_BASE + GPIO_O_DEN) |= 0xCF; // 0xCF = 11001111
   
   // Set Pin Directions for Port D
   HWREG(GPIO_PORTD_BASE + GPIO_O_DIR) |= 0x03; // 0x03 = 00000011
   HWREG(GPIO_PORTD_BASE + GPIO_O_DIR) &= 0xB3; // 0xB3 = 10110011
   
   // Initialize Port E
   // Enable the clock to Port E
   HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R4;
   while ((HWREG(SYSCTL_PRGPIO) & BIT4HI) != BIT4HI) {}
   
   // Enable Port E for Digital I/O
   HWREG(GPIO_PORTE_BASE + GPIO_O_DEN) |= 0x3F; // 0x3F = 00111111
   
   // Set Pin Directions for Port E
   HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) &= 0xC0; // 0xC0 = 11000000
   
   // Initialize Port F
   // Enable the clock to Port F
   HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R5;
   while ((HWREG(SYSCTL_PRGPIO) & BIT5HI) != BIT5HI) {}
   
   // Enable Port F for Digital I/O
   HWREG(GPIO_PORTF_BASE + GPIO_O_DEN) |= 0x1F; // 0x1F = 00011111
   
   // Set Pin Directions for Port F
   HWREG(GPIO_PORTF_BASE + GPIO_O_DIR) |= 0x7; // 0x1F = 00011111
   return true;
 }
 
 bool InitAllPWM(void){
   // Enable clock to PWM0
   HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;
  
   // Enable clock to PWM1
   HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R1;
   
   // All port clocks already enabled, so do nothing here
  
  // Select PWM clock as System Clock divided by 32
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) | \
                      (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_32);
 
  // Check the PWM clock stabilized
  while((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0) {
    ;
  }
  
  // Check the PWM clock stabilized
  while((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R1) != SYSCTL_PRPWM_R1) {
    ;
  }
  
  // Disable the PWM during initialization
  HWREG(PWM0_BASE + PWM_O_0_CTL) = 0x0;
  HWREG(PWM0_BASE + PWM_O_1_CTL) = 0x0;
  HWREG(PWM0_BASE + PWM_O_3_CTL) = 0x0;
  
  // Disable the PWM during initialization
  HWREG(PWM1_BASE + PWM_O_0_CTL) = 0x0;
  
  // Program generators to go to 1 at rising compare A/B,
  // 0 on falling compare A/B
  
  HWREG(PWM0_BASE + PWM_O_0_GENA) = (PWM_0_GENA_ACTCMPAU_ONE | \
                                     PWM_0_GENA_ACTCMPAD_ZERO);
  
  HWREG(PWM0_BASE + PWM_O_0_GENB) = (PWM_0_GENB_ACTCMPBU_ONE | \
                                     PWM_0_GENB_ACTCMPBD_ZERO);
  
  HWREG(PWM0_BASE + PWM_O_1_GENA) = (PWM_0_GENA_ACTCMPAU_ONE | \
                                     PWM_0_GENA_ACTCMPAD_ZERO);
  
  HWREG(PWM0_BASE + PWM_O_1_GENB) = (PWM_0_GENB_ACTCMPBU_ONE | \
                                     PWM_0_GENB_ACTCMPBD_ZERO);
                                     
  HWREG(PWM0_BASE + PWM_O_3_GENA) = (PWM_0_GENA_ACTCMPAU_ONE | \
                                     PWM_0_GENA_ACTCMPAD_ZERO);
  
  HWREG(PWM0_BASE + PWM_O_3_GENB) = (PWM_0_GENB_ACTCMPBU_ONE | \
                                     PWM_0_GENB_ACTCMPBD_ZERO);
  
  HWREG(PWM1_BASE + PWM_O_0_GENA) = (PWM_1_GENA_ACTCMPAU_ONE | \
                                     PWM_1_GENA_ACTCMPAD_ZERO);
  
  HWREG(PWM1_BASE + PWM_O_0_GENB) = (PWM_1_GENB_ACTCMPBU_ONE | \
                                     PWM_1_GENB_ACTCMPBD_ZERO);

  // Set PWM period
  HWREG(PWM0_BASE + PWM_O_0_LOAD) = (PWM_TICKS_PER_MS*1000/PWM_FREQ_0_Hz) >> 1;
  HWREG(PWM0_BASE + PWM_O_1_LOAD) = (PWM_TICKS_PER_MS*1000/PWM_FREQ_1_Hz) >> 1;
  HWREG(PWM0_BASE + PWM_O_3_LOAD) = (PWM_TICKS_PER_MS*1000/PWM_FREQ_3_Hz) >> 1;

  // Set PWM period
  HWREG(PWM1_BASE + PWM_O_0_LOAD) = ((PERIOD_IN_US * PWM_TICKS_PER_MS) / 1000) >> 1;

  // Set initial duty cycle on Module 0 to 99% 
  
  HWREG(PWM0_BASE + PWM_O_0_CMPA) = HWREG(PWM0_BASE + PWM_O_0_LOAD) >> 1 ;
  HWREG(PWM0_BASE + PWM_O_0_CMPB) = HWREG(PWM0_BASE + PWM_O_0_LOAD) >> 1 ;
  HWREG(PWM0_BASE + PWM_O_1_CMPA) = HWREG(PWM0_BASE + PWM_O_1_LOAD) >> 1;
  HWREG(PWM0_BASE + PWM_O_1_CMPB) = HWREG(PWM0_BASE + PWM_O_1_LOAD) >> 2;
  HWREG(PWM0_BASE + PWM_O_3_CMPA) = HWREG(PWM0_BASE + PWM_O_3_LOAD) >> 1;
  HWREG(PWM0_BASE + PWM_O_3_CMPB) = HWREG(PWM0_BASE + PWM_O_3_LOAD) >> 2;
  

  // Set initial duty cycle on M1/0A to 50% 
  HWREG(PWM1_BASE + PWM_O_0_CMPA) = HWREG(PWM1_BASE + PWM_O_0_LOAD) >> 1;
  
  // Set initial duty cycle on M1/0B to 25%
  HWREG(PWM1_BASE + PWM_O_0_CMPB) = (HWREG(PWM1_BASE + PWM_O_0_LOAD)) - \
                                   (((PERIOD_IN_US * PWM_TICKS_PER_MS) / 1000)>>3);

  // Enable PWM Outputs 
  HWREG(PWM0_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM0EN | \
                                      PWM_ENABLE_PWM1EN | 
                                      PWM_ENABLE_PWM2EN |
                                      PWM_ENABLE_PWM3EN |
                                      PWM_ENABLE_PWM6EN |
                                      PWM_ENABLE_PWM7EN);
                                      
  // Invert PWM Outputs for Module 0, Block 3
  HWREG(PWM0_BASE + PWM_O_INVERT) |= PWM_INVERT_PWM6INV | PWM_INVERT_PWM7INV;
                                      
    // Enable PWM Outputs 
  HWREG(PWM1_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM1EN | \
                                      PWM_ENABLE_PWM0EN);
                                      
  // Now configure the Tiva pins to be PWM outputs
  // Start by selecting the alternate function for PB4-7, PC4-5, PD0-1
  HWREG(GPIO_PORTB_BASE + GPIO_O_AFSEL) |= (BIT7HI | BIT6HI | BIT5HI | BIT4HI);
  
  HWREG(GPIO_PORTC_BASE + GPIO_O_AFSEL) |= (BIT5HI | BIT4HI);
  
  HWREG(GPIO_PORTD_BASE + GPIO_O_AFSEL) |= (BIT1HI | BIT0HI);
  
  // Map PWM to these pins 
  HWREG(GPIO_PORTB_BASE  + GPIO_O_PCTL) = (HWREG(GPIO_PORTB_BASE + \
                                           GPIO_O_PCTL) & 0x0000FFFF) + \
                                           (4<<(7*BITS_PER_NIBBLE)) + 
                                           (4<<(6*BITS_PER_NIBBLE)) +
                                           (4<<(5*BITS_PER_NIBBLE)) + 
                                           (4<<(4*BITS_PER_NIBBLE));
                                           
  HWREG(GPIO_PORTC_BASE  + GPIO_O_PCTL) = (HWREG(GPIO_PORTC_BASE + \
                                           GPIO_O_PCTL) & 0xFF00FFFF) + \
                                           (4<<(5*BITS_PER_NIBBLE)) + 
                                           (4<<(4*BITS_PER_NIBBLE));

  HWREG(GPIO_PORTD_BASE  + GPIO_O_PCTL) = (HWREG(GPIO_PORTD_BASE + \
                                           GPIO_O_PCTL) & 0xFFFFFF00) + \
                                           (5<<(0*BITS_PER_NIBBLE)) + 
                                           (5<<(1*BITS_PER_NIBBLE));
                                            
  // Set up/down count mode, enable PWM generator, and make
  // both generator updates locally synchronized to zero count 
  HWREG(PWM0_BASE + PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
                                    PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
  
  HWREG(PWM0_BASE + PWM_O_1_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
                                    PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
                                    
  HWREG(PWM0_BASE + PWM_O_3_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
                                    PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);

  // Set up/down count mode, enable PWM generator, and make
  // both generator updates locally synchronized to zero count 
  HWREG(PWM1_BASE + PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
                                    PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
                                    
  return true;
}
 
bool InitAllInterruptTimers(void) {

  // Requires a change in the startup_rvmdk.s file to the WTIMER1/5
  // interrupt vector

  // Enable clock to timers (WTIMER1/WTIMER5)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R1;
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R3;
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R5;
  
  // All port clocks already enabled
  
  // Make sure that Timers are disabled before configuring 
  HWREG(WTIMER1_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN & ~TIMER_CTL_TBEN;
  HWREG(WTIMER3_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN & ~TIMER_CTL_TBEN;
  HWREG(WTIMER5_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN & ~TIMER_CTL_TBEN;
  
  // Set timers to 32-bit wide mode
  HWREG(WTIMER1_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
  HWREG(WTIMER3_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
  HWREG(WTIMER5_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
  
  // Register the FULL 32-bit value)
  HWREG(WTIMER1_BASE + TIMER_O_TAILR) = 0xFFFFFFFF;
  HWREG(WTIMER3_BASE + TIMER_O_TAILR) = 0xFFFFFFFF;
  HWREG(WTIMER5_BASE + TIMER_O_TAILR) = 0xFFFFFFFF;
  
  // Set up timers in capture mode
  // Timer 1A
  HWREG(WTIMER1_BASE+TIMER_O_TAMR) =
  (HWREG(WTIMER1_BASE+TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) |
  (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  
  // Timer 1B
  HWREG(WTIMER1_BASE+TIMER_O_TBMR) =
  (HWREG(WTIMER1_BASE+TIMER_O_TBMR) & ~TIMER_TBMR_TBAMS) |
  (TIMER_TBMR_TBCDIR | TIMER_TBMR_TBCMR | TIMER_TBMR_TBMR_CAP);
  
  // Timer 5A
  HWREG(WTIMER5_BASE+TIMER_O_TAMR) =
  (HWREG(WTIMER5_BASE+TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) |
  (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  
  // Set up Timer 3A in periodic mode (TAMR = 1)
  HWREG(WTIMER3_BASE + TIMER_O_TAMR) = (HWREG(WTIMER3_BASE + TIMER_O_TAMR) &\
                                        ~TIMER_TAMR_TAMR_M) | 
                                        TIMER_TAMR_TAMR_PERIOD; 
                                        
  // Set up Timer 5B in periodic mode (TBMR = 1)
  HWREG(WTIMER5_BASE + TIMER_O_TBMR) = (HWREG(WTIMER5_BASE + TIMER_O_TBMR) &\
                                        ~TIMER_TBMR_TBMR_M) | 
                                        TIMER_TBMR_TBMR_PERIOD; 

  // set timeout 
  HWREG(WTIMER3_BASE+TIMER_O_TAILR) = TicksPerMS * PID_PERIOD_MS;
  HWREG(WTIMER5_BASE+TIMER_O_TBILR) = TicksPerMS * PID_PERIOD_MS;
  
  // Using one timer, capture rising edge
  HWREG(WTIMER1_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
  HWREG(WTIMER1_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TBEVENT_M;
  HWREG(WTIMER5_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
  
  // Trigger on both rising and falling edge
  HWREG(WTIMER5_BASE + TIMER_O_CTL) |= (BIT2HI);

  // Setup the port to do the capture by setting alternate function
  HWREG(GPIO_PORTC_BASE + GPIO_O_AFSEL) |= (BIT6HI | BIT7HI);
  HWREG(GPIO_PORTD_BASE + GPIO_O_AFSEL) |= (BIT6HI | BIT7HI);

  // Map alternate function
  HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) =
  (HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) & 0x00FFFFFF) + 
  (7 << 6 * BITS_PER_NIBBLE) + (7 << 7 * BITS_PER_NIBBLE);
  
  HWREG(GPIO_PORTD_BASE + GPIO_O_PCTL) = 
  (HWREG(GPIO_PORTD_BASE+GPIO_O_PCTL) & 0x00FFFFFF) + (7 << 2 * BITS_PER_NIBBLE) +
  (7 << 6 * BITS_PER_NIBBLE) + (7 << 7 * BITS_PER_NIBBLE);

  // Enable local capture interrupt 
  HWREG(WTIMER1_BASE + TIMER_O_IMR) |= TIMER_IMR_CAEIM | TIMER_IMR_CBEIM;
  HWREG(WTIMER5_BASE + TIMER_O_IMR) |= TIMER_IMR_CAEIM;
  
  // Enable a local timeout interrupt. 
  //HWREG(WTIMER3_BASE + TIMER_O_IMR) |= TIMER_IMR_TATOIM;
  HWREG(WTIMER5_BASE + TIMER_O_IMR) |= TIMER_IMR_TBTOIM;

  // Enable interrupts in NVIC
  // Interrupts 96, 97, 104, 105
  HWREG(NVIC_EN3) |= BIT0HI | BIT1HI;
  HWREG(NVIC_EN3) |= BIT4HI;
  HWREG(NVIC_EN3) |= BIT8HI | BIT9HI;
  
  // Set lower priority for NVIC Interrupt 100 (Wide Timer 3A)
  HWREG(NVIC_PRI25) = (HWREG(NVIC_PRI25) & ~NVIC_PRI25_INTA_M) 
  + (1 << NVIC_PRI25_INTA_S);
  
  // Set lower priority for NVIC Interrupt 104 (Wide Timer 5A)
  HWREG(NVIC_PRI26) = (HWREG(NVIC_PRI26) & ~NVIC_PRI26_INTA_M) 
  + (2 << NVIC_PRI26_INTA_S);
  
  // Set lower priority for NVIC Interrupt 105 (Wide Timer 5B)
  HWREG(NVIC_PRI26) = (HWREG(NVIC_PRI26) & ~NVIC_PRI26_INTB_M) 
  + (1 << NVIC_PRI26_INTB_S);

  // Make sure interrupts are enabled globally
  __enable_irq();

  // Enable timer and its stall feature
  HWREG(WTIMER1_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
  HWREG(WTIMER1_BASE + TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
  // Timer 3 is not enabled here, but in a separate service, to ensure that it is offset from Timer 5
  HWREG(WTIMER5_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
  HWREG(WTIMER5_BASE + TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL); // Comment out to disable PID
  
  return true;
}
