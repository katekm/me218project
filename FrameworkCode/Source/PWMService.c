/****************************************************************************
 Module
   PWMService.c

 Revision
   1.0.1

 Description
   This is a file to implement PWM

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/22/19 21:2 bjun2     Update for PWM control of a DC motor
 01/16/12 09:58 jec      began conversion from PWMFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"

#include "inc/hw_gpio.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_pwm.h"
#include "inc/hw_sysctl.h"
#include "inc/tm4c123gh6pm.h"

#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/sysctl.h"



#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

#include "PWMService.h"
#include "NewADService.h"
#include "MeasureService.h"

#define PERIOD_IN_MS 25
#define PWM_TICKS_PER_MS 25
#define BITS_PER_NIBBLE 4

/*----------------------------- Module Defines ----------------------------*/
#define TICKS_PER_MS 98
#define CONTROL_INTERVAL 200
#define RPM_MAX 78
#define P_VAL 2.5
#define I_VAL 0.9
#define DUTY_MAX 99
#define DUTY_MIN 1

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/

// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint16_t BaselineADCResult;
static bool ReturnVal = false;
static ES_Event_t ThisEvent;

static uint32_t time;

// State of the motor
static StepState_t CurrentState = InitPSState;

// add a deferral queue for up to 3 pending deferrals +1 to allow for overhead
static struct ES_Event DeferralQueue[3+1];

static volatile uint32_t TimeoutCount = 0;

static volatile int32_t p_err = 0;
static volatile int32_t i_err = 0;
static volatile int32_t d_err = 0;
static volatile int32_t deltaDuty = 0;
static volatile int32_t duty_out = 0;
static volatile int32_t prev_p_err = 0;
static volatile int32_t v_out = 0;
static volatile int32_t v_prev = 0;
static volatile int32_t v_desired = 0;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitPWMService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     Brian Jun, 01/22/19, 22:10
****************************************************************************/
bool InitPWMService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/

  // Hardware initialization of the PWM module
  InitPWM();
  
  InitPeriodicInt();

  // Set one of the PWM to output at 100% duty cycle
  Set100_DC();

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostPWMService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     Brian Jun, 01/22/19, 22:11
****************************************************************************/
bool PostPWMService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunPWMService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Run a service that adjusts the output frequency of the PWM module
   with respect to the ADC value from the potentiometer
 Notes

 Author
   Brian Jun, 01/22/19, 22:35
****************************************************************************/
ES_Event_t RunPWMService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;

  // Assume no errors
  ReturnEvent.EventType = ES_NO_EVENT; 
    
  // Switch the state of the SM based on CurrentState  
  switch (CurrentState){

    // If CurrentState is pseudo-init state
    case InitPSState:

      // If received event is ES_INIT
      if(ThisEvent.EventType == ES_INIT) {
        
        // initialize the deferal queue
        ES_InitDeferralQueueWith(DeferralQueue, ARRAY_SIZE(DeferralQueue));

        // move to the Initializing state
        CurrentState = Driving;
        
      }
      break;

    // If CurrentState is Stepping state
    case Driving:
      
      // If StepTimer sends timeout event and timer number is StepTimer's
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == AD_TIMER) {
        
        // Read potentiometer value 
        uint32_t result = getPotValue();

        // Calculate duty cycle
        uint32_t duty = (result * 100.0f) / (4096.f);

        // Print duty cycle to terminal for characterization
        //SetPWM(duty_out);

        // Set PWM value to duty cycle
        /*
        printf("Duty: %d\n\r", duty);
        printf("v_out: %d\n\r", v_out);
        printf("Last Speed: : %d\n\r", v_prev);
        printf("v_desired: %d\n\r", v_desired);
        printf("RPM: %d\n\r", GetRPM());
        printf("pot value: %d\n\r", getPotValue());
        printf("p_err %d\n\r", p_err);
        printf("i_err %d\n\r", i_err);
        printf("Commanded Duty: %d\n\r\n\r", duty_out); */
        
        // Post event
        PostMeasureService(ThisEvent);
        
        // Transition to next state
        CurrentState = Driving;
      } 
      

      

      break;
  }
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
/****************************************************************************
 Function
    InitPWM

 Parameters
   None

 Returns
   None

 Description
   Hardware-level initialization of PWM module

 Notes

 Author
   Brian Jun, 01/22/19, 22:16
****************************************************************************/
void InitPWM(void){
  // Enable clock to PWM0
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;
  
  // Enable clock to Port B
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
  
  // Select PWM clock as System Clock divided by 32
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) | \
                      (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_16);
 
  // Check the PWM clock stabilized
  while((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0) {
    ;
  }
  
  // Disable the PWM during initialization
  HWREG(PWM0_BASE + PWM_O_0_CTL) = 0x0;
  
  // Program generators to go to 1 at rising compare A/B,
  // 0 on falling compare A/B
  
  HWREG(PWM0_BASE + PWM_O_0_GENA) = (PWM_0_GENA_ACTCMPAU_ONE | \
                                     PWM_0_GENA_ACTCMPAD_ZERO);
  
  HWREG(PWM0_BASE + PWM_O_0_GENB) = (PWM_0_GENB_ACTCMPBU_ONE | \
                                     PWM_0_GENB_ACTCMPBD_ZERO);
  
  // Set PWM period
  HWREG(PWM0_BASE + PWM_O_0_LOAD) = ((PERIOD_IN_MS * PWM_TICKS_PER_MS)) >> 1;
  
  // Set initial duty cycle on A to 50% 
  HWREG(PWM0_BASE + PWM_O_0_CMPA) = HWREG(PWM0_BASE + PWM_O_0_LOAD) >> 1;
  
  // Set initial duty cycle on B to 25%
  HWREG(PWM0_BASE + PWM_O_0_CMPB) = (HWREG(PWM0_BASE + PWM_O_0_LOAD)) - \
                                   (((PERIOD_IN_MS * PWM_TICKS_PER_MS))>>3);
  
  // Enable PWM Outputs 
  HWREG(PWM0_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM1EN | \
                                      PWM_ENABLE_PWM0EN);
                                      
  // Now configure the Port B pins to be PWM outputs
  // Start by selecting the alternate function for PB6 & 7
  HWREG(GPIO_PORTB_BASE + GPIO_O_AFSEL) |= (BIT7HI | BIT6HI);
  
  // Map PWM to these pins 
  HWREG(GPIO_PORTB_BASE  + GPIO_O_PCTL) = (HWREG(GPIO_PORTB_BASE + \
                                           GPIO_O_PCTL) & 0x00FFFFFF) + \
                                           (4<<(7*BITS_PER_NIBBLE)) + 
                                           (4<<(6*BITS_PER_NIBBLE));
  
  // Enable pins 6 and 7 on Port B
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT7HI | BIT6HI | \
                                          BIT0HI | BIT1HI | BIT2HI);
  
  // Setup pins 6 and 7 as outputs on Port B 
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT7HI | BIT6HI | \
                                            BIT0HI | BIT1HI | BIT2HI);
                                            
  // Set up/down count mode, enable PWM generator, and make
  // both generator updates locally synchronized to zero count 
  HWREG(PWM0_BASE + PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
                                    PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
}

/****************************************************************************
 Function
    Set100_DC

 Parameters
   None

 Returns
   None

 Description
   Set PWM channel B to output at 100% duty cycle

 Notes

 Author
   Brian Jun, 01/22/19, 22:17
****************************************************************************/
void Set100_DC(void){
  // Set to 100% Duty Cycle
  HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENB_ACTZERO_ONE;
}

/****************************************************************************
 Function
    SetPWM

 Parameters
   uint8_t: the duty cycle to set PWM to

 Returns
   None

 Description
   Set PWM channel A to duty cycle passed in as argument

 Notes

 Author
   Brian Jun, 01/22/19, 22:17
****************************************************************************/
void SetPWM(uint8_t duty) {
  // Set according to percentage of PWM period 
  // Add one to prevent short from power to ground
  HWREG(PWM0_BASE + PWM_O_0_CMPA) = HWREG(PWM0_BASE + PWM_O_0_LOAD) * \
                                    (duty/(100.0f)) + 1;
}

void InitPeriodicInt(void){
  // Assumes that previous calls have initialized the function
  
  // Skip clock enable and waiting for clock timer
  
  // Disable Timer B
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
  
  // Skip setup, as WTIMER0 is already in 32-bit wide mode

  // Set up Timer B in one-shot mode
  HWREG(WTIMER0_BASE + TIMER_O_TBMR) = \
        (HWREG(WTIMER0_BASE + TIMER_O_TBMR) & ~TIMER_TBMR_TBMR_M) |\
         TIMER_TBMR_TBMR_PERIOD;
  
  // Set timeout
  HWREG(WTIMER0_BASE + TIMER_O_TBILR) = CONTROL_INTERVAL * TICKS_PER_MS;
  
  // Enable local timeout interrupt
  HWREG(WTIMER0_BASE + TIMER_O_IMR) |= TIMER_IMR_TBTOIM;
  
  // Enable Timer B in Wide Timer 0 interrupt 
  // in the NVIC; it's the interrupt number 95
  HWREG(NVIC_EN2) |= BIT31HI;
  
  // Put priotiy level lower
  HWREG(NVIC_PRI23) |= BIT29HI;
  
  // Make sure interrupts are enabled globally 
  __enable_irq();
  
  // Kick off the timer
  HWREG(WTIMER0_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | 
    TIMER_CTL_TBSTALL);
}

void PeriodicIntResponse(void){
  //Clear the source of the interrupts
  HWREG(WTIMER0_BASE + TIMER_O_ICR) = TIMER_ICR_TBTOCINT; 
  
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT0HI;
  
  // Sample motor speed
  v_desired = ((getPotValue() * 100.0f /4096.0f) * RPM_MAX) / 100;
  CalculateRPM();
  v_out = GetRPM();
  
  // Perform PI control 
  p_err = v_desired - v_out;
  i_err += p_err;
  
  // Calculate delta duty
  deltaDuty =  (P_VAL * (p_err + (I_VAL * i_err)));
  
  // Update step
  duty_out = deltaDuty; 
  prev_p_err = p_err;
  
  // Protect duty inside
  if (duty_out >= DUTY_MAX) {
    duty_out = DUTY_MAX;
    i_err -= p_err;
  } else if (duty_out < DUTY_MIN) {
    duty_out = DUTY_MIN;
    i_err -= p_err;
  } else {
    ;
  }
  
  // Set PWM value
  SetPWM(100 - duty_out);
 
  
  // increment our counter so we can tell this is running
  ++TimeoutCount;
  
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT0LO;

}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

