/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ServoLibrary.h"

/* include header files for hardware access
*/
#include <stdint.h>
#include <stdbool.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"

#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"

/*----------------------------- Module Defines ----------------------------*/

#define SERVO_PERIOD_MIN_US 1000
#define SERVO_PERIOD_MAX_US 2000

#define REJECT_ON_US 1000
#define REJECT_OFF_US 1600
#define REJECT_BLOCK_US 1300

#define DISPENSE_ON_US 700
#define DISPENSE_OFF_US 1900

#define PWM_TICKS_PER_MS 1250


/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
                                                                                      
void RejectServoOn(void);
void RejectServoOff(void);
void RejectServoBlock(void);

void DispenseServoOn(void);
void DispenseServoOff(void);

/***************************************************************************
 private functions
 ***************************************************************************/

void RejectServoOff(void) {
	HWREG(PWM0_BASE + PWM_O_3_CMPB) = ((REJECT_OFF_US * PWM_TICKS_PER_MS) / 1000) >> 1;
}

void RejectServoOn(void) {
	HWREG(PWM0_BASE + PWM_O_3_CMPB) = ((REJECT_ON_US * PWM_TICKS_PER_MS) / 1000) >> 1;
}

void RejectServoBlock(void) {
	HWREG(PWM0_BASE + PWM_O_3_CMPB) = ((REJECT_BLOCK_US * PWM_TICKS_PER_MS) / 1000) >> 1;
}

void DispenseServoOff(void) {
	HWREG(PWM0_BASE + PWM_O_3_CMPA) = ((DISPENSE_OFF_US * PWM_TICKS_PER_MS) / 1000) >> 1;
}

void DispenseServoOn(void) {
	HWREG(PWM0_BASE + PWM_O_3_CMPA) = ((DISPENSE_ON_US * PWM_TICKS_PER_MS) / 1000) >> 1;
}
