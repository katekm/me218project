/************ INCLUDE IN ALEX CODE ***************/


/* Put below variables in whatever service 
  requires ADC values */

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "math.h"
#include "ADMulti.h"


#define INDUCTIVE_THRESHOLD 4000
#define DIFFERENTIAL_TOLERANCE 100

#define SUCCESS 1
#define ERROR 0

#define LEFT_OFFCENTER 0
#define RIGHT_OFFCENTER 1

#define DIFFERENTIAL_BASELINE_RPM 10
#define MAX_ADC_COUNTS 4095
#define AFFINE_SHIFT 0

#define MY_MIN(x,y) ((x) <= (y)) ? (x) : (y)

static uint32_t ADCResults[4];
static volatile uint8_t previous_value = 0;

bool InitInductiveSensor(void){
  ADC_MultiInit(2);
  ADC_MultiRead(ADCResults);
  return true;
}

/* 0 is left inductive sensor
   1 is right inductive sensor
*/
uint32_t SampleInductiveSensor(uint8_t sensor) {
  ADC_MultiRead(ADCResults);
  if(sensor == 0) {
    return ADCResults[0];
  } else if (sensor == 1) {
    return ADCResults[1];
  } else{
    printf("Invalid sensor code\n\r");
    exit(0);
  }
  return 0xdeadbeef;
}

bool InductancesAreMatched(uint32_t left_val, uint32_t right_val) {
  return ((abs(left_val - right_val) < DIFFERENTIAL_TOLERANCE) ? true : false);
}

bool InductancesAreStrong(uint32_t left_val, uint32_t right_val) {
  return ((left_val >= INDUCTIVE_THRESHOLD && right_val >= INDUCTIVE_THRESHOLD) ? true : false);
}

uint32_t FindValueOfOffCenterInductor(void){
  return MY_MIN(ADCResults[0], ADCResults[1]);
}

uint32_t FindActualOffCenterInductor(void){
  uint32_t which_inductor = FindValueOfOffCenterInductor();
  if(which_inductor == ADCResults[0]) {
    return 0; // left
  } else if (which_inductor == ADCResults[1]) {
    return 1; // right
  } else {
    return 255; // error
  }
}

uint8_t InferDutyFromInductance(void) {
  uint32_t offcenter_inductance = FindValueOfOffCenterInductor();
  return (uint8_t)(((MAX_ADC_COUNTS - offcenter_inductance) \
                   * DIFFERENTIAL_BASELINE_RPM) / MAX_ADC_COUNTS \
                   + AFFINE_SHIFT); // possible +25 needed for affine function
}

uint8_t NonlinearDutyInference(void) {

    uint8_t current_value =(((MAX_ADC_COUNTS - abs(ADCResults[0] - ADCResults[1])) \
                          * DIFFERENTIAL_BASELINE_RPM) / MAX_ADC_COUNTS \
                          + AFFINE_SHIFT);
    return current_value;
}
