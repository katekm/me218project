/****************************************************************************

  Header file for SSI service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ServoLibrary_H
#define ServoLibrary_H

// Public Function Prototypes

void RejectServoOn(void);
void RejectServoOff(void);
void RejectServoBlock(void);

void DispenseServoOn(void);
void DispenseServoOff(void);

#endif /* ServSSI_H */

