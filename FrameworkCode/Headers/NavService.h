/****************************************************************************
 
  Header file for Test Harness Service0 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef NAV_SERVICE_H
#define NAV_SERVICE_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { NavInitPState, 
               NavAwaitingCommand,
               NavBallFinding,
               NavLineFinding,
               NavRecyclingFinding, 
               NavLineFollowing,
               NavShotPrepping
             } NavState_t ;

// Public Function Prototypes

bool InitNavService ( uint8_t Priority );
bool PostNavService ( ES_Event_t ThisEvent );
ES_Event_t RunNavService( ES_Event_t ThisEvent );
             
int16_t QueryLineFollowingRPM(void);

#endif 

