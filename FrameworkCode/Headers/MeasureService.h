/****************************************************************************

  Header file for Measure service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ServMeasure_H
#define ServMeasure_H

// Typedefs for the states 
typedef enum { InitPState, Measuring } MeasureState_t ;

// Public Function Prototypes

bool InitMeasureService(uint8_t Priority);
bool PostMeasureService(ES_Event_t ThisEvent);
ES_Event_t RunMeasureService(ES_Event_t ThisEvent);
void InitInputCapturePeriod(void);
void FindEdge(void);
void InitLEDs(void);
void UpdateLEDs(void);
void CalculateRPM();
void InputCaptureResponse(void);
uint32_t GetRPM(void);

#endif /* ServMeasure_H */

