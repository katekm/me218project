/****************************************************************************

  Header file for Test Harness Service0
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef CheckoffMotorService_H
#define CheckoffMotorService_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { CheckoffMotorInitPState, CheckoffMotorAwaitingCommand } CheckoffMotorState_t ;

// Public Function Prototypes

bool InitCheckoffMotorService ( uint8_t Priority );
bool PostCheckoffMotorService ( ES_Event_t ThisEvent );
ES_Event_t RunCheckoffMotorService( ES_Event_t ThisEvent );

// Public Function Prototypes

void InitPIDInterrupt(void);
void PID_ISR(void);
uint32_t QueryPIDCounter(void);

#endif
