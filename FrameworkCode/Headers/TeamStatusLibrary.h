#ifndef TeamStatusLibrary_H
#define TeamStatusLibrary_H

// Public Function Prototypes
uint8_t CheckforTeamLock(void);
uint8_t GetSelectState(void);
uint8_t ShowGameOn(void);
uint8_t ShowGameOff(void);

#endif /* TeamStatusLibrary_H */
