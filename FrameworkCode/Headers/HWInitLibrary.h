/****************************************************************************

  Header file for PWM service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef HWInitLibrary_H
#define HWInitLibrary_H

// Public Function Prototypes
bool InitAllTivaPins(void);
bool InitAllPWM(void);
bool InitAllInterruptTimers(void);

#endif
