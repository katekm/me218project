/****************************************************************************

  Header file for Test Harness Service0
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef Lab8Service_H
#define Lab8Service_H

typedef enum
{
  Lab8InitPState,
  AwaitingCommand,
  ExecutingCommand
}Lab8State_t;

// Public Function Prototypes

bool InitLab8Service(uint8_t Priority);
bool PostLab8Service(ES_Event_t ThisEvent);
ES_Event_t RunLab8Service(ES_Event_t ThisEvent);

void InitIRInputCapture(void);
void IRInputCaptureISR(void);

#endif /* Lab8Service_H */

