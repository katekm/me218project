/****************************************************************************
 
  Header file for Test Harness Service0 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef GAME_CLOCK_SERVICE_H
#define GAME_CLOCK_SERVICE_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { ClockInitPState, ClockTicking
             } GameClockState_t ;

// Public Function Prototypes

bool InitGameClockService ( uint8_t Priority );
bool PostGameClockService ( ES_Event_t ThisEvent );
ES_Event_t RunGameClockService( ES_Event_t ThisEvent );

#endif 

