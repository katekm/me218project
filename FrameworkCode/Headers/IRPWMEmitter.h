/****************************************************************************

  Header file for PWM service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ServPWM_H
#define ServPWM_H

#include "ES_Types.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitPSState, Driving } StepState_t ;

// Public Function Prototypes

bool InitPWMService(uint8_t Priority);
bool PostPWMService(ES_Event_t ThisEvent);
ES_Event_t RunPWMService(ES_Event_t ThisEvent);
bool InitPWM(void);
void Set100_DC(void);
void SetPWM(uint8_t duty); 

#endif /* ServPWM_H */

