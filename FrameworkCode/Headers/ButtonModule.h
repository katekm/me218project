/****************************************************************************
 
  Header file for Test Harness Service0 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef BUTTON_MODULE_H
#define BUTTON_MODULE_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { Debouncing, Ready2Sample } ButtonState_t ;

// Public Function Prototypes

bool InitButtonDebounce ( uint8_t Priority );
bool PostButtonDebounce ( ES_Event_t ThisEvent );
ES_Event_t RunButtonDebounceSM( ES_Event_t ThisEvent );
               



#endif 

