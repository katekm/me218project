/****************************************************************************

  Header file for Test Harness Service0
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ProjectService_H
#define ProjectService_H

typedef enum
{
  ProjectServiceInitPState,
  GameRunning,
  GameEnded
}ProjectServiceState_t;

// Public Function Prototypes

bool InitProjectService(uint8_t Priority);
bool PostProjectService(ES_Event_t ThisEvent);
ES_Event_t RunProjectService(ES_Event_t ThisEvent);

bool QueryRecyclingStation(void);
uint16_t QueryTeamFreq(void);
uint8_t QueryTeamColor(void);

#endif /* ProjectService_H */

