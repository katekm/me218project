/****************************************************************************

  Header file for Motor Library
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/

#ifndef MotorLib_H
#define MotorLib_H

bool InitMotorHW(void);
void SetDutyCycle(int8_t DutyCycle, uint8_t MotorNum);

#endif
