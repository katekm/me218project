/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_nvic.h"

#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"

#include "ES_Port.h"
#include "termio.h"

bool InitEncoderInterrupt(void);
void Encoder0_ISR(void);
void Encoder1_ISR(void);
uint32_t CalcRPM(uint8_t MotorNum);
int32_t QueryEncoderCount(uint8_t MotorNum);
