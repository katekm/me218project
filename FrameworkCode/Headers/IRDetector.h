/****************************************************************************

  Header file for Test Harness Service0
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef IRDetector_H
#define IRDetector_H


// Public Function Prototypes

bool InitIRInputCapture(void);
void IRInputCaptureISR(void);
uint32_t GetBeaconPeriod(void);

#endif /* ProjectService_H */

