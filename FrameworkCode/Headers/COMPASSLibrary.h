/****************************************************************************

  Header file for SSI service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef COMPASSLibrary_H
#define COMPASSLibrary_H

// Public Function Prototypes

bool InitSSIService(uint8_t Priority);
bool PostSSIService(ES_Event_t ThisEvent);
ES_Event_t RunSSIService(ES_Event_t ThisEvent);
bool InitSSI (void);
void TransmitSPI(uint8_t tx_data);
void QueryCG(void);
uint8_t ReceiveSPI(void);
void ExecuteCommandCycle (void);
void InterpretCommand(uint8_t new_command);
uint8_t IsNewCommand(uint8_t recent_command, uint8_t last_command);
uint8_t IsErrorCommand(uint8_t new_command);
void delay(void);
void message_delay(void);
uint8_t RegisterTeam(uint8_t team_polarity);
uint8_t QueryStatus(void);
uint8_t QueryUnmaskedStatus(void);
uint8_t QueryTeamInfo(void);
void QueryScore(void);
void QueryRecyclingValue(void);

#endif /* ServSSI_H */

