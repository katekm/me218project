/****************************************************************************

  Header file for AD service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ServAD_H
#define ServAD_H

// Public Function Prototypes

// typedefs for the states
// State definitions for use with the query function
typedef enum { PInitState, Sampling } ADState_t ;

// Public Function Prototypes

bool InitNewADService(uint8_t Priority);
bool PostNewADService(ES_Event_t ThisEvent);
ES_Event_t RunNewADService(ES_Event_t ThisEvent);
uint32_t getPotValue(void);

#endif /* ServAD_H */

