/****************************************************************************

  Header file for Test Harness Service0
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef InductiveLibrary_H
#define InductiveLibrary_H

#define LEFT_INDUCTOR 0
#define RIGHT_INDUCTOR 1 

// Public Function Prototypes

bool InitInductiveSensor(void);
uint32_t SampleInductiveSensor(uint8_t sensor);
bool InductancesAreMatched(uint32_t left_val, uint32_t right_val);
bool InductancesAreStrong(uint32_t left_val, uint32_t right_val);
uint32_t FindValueOfOffCenterInductor(void);
uint32_t FindActualOffCenterInductor(void);
uint8_t InferDutyFromInductance(void);
uint8_t NonlinearDutyInference(void);

#endif /* InductiveLibrary_H */

